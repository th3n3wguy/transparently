# Transparently
###### ...because insights are best when they are transparent and easi-ly found.

[![license](https://img.shields.io/badge/license-UNLICENSED-blue)](https://github.com/ftlos/transparently/blob/master/LICENSE)

The source code for the Transparently application.

## Developer Machine Setup
### Pre-Requisites
* Install [Docker](https://docs.docker.com/).
* Install [Node.js](https://nodejs.org/en/download/) (see the `package.json` file in the root directory to determine the version).

### Configuration Setup
1. Create the Docker Compose script.
    * In the root directory, create a copy of the `.env.default` and rename it to `.env`.
    * Fill out the environment variable values with the correct values.
2. Create an environment file for the Web App Server.
    * Navigate to the `web-app/server` directory.
    * Create a copy of the `.env.default` and rename it to `.env`.
    * Fill out the environment variable values with the correct values.
3. Create an environment file for the Web App UI.
    * Naviate to the `web-app/ui` directory.
    * Create a copy of the `.env.default` and rename it to `.env`.
    * Fill out the environment variable values with the correct values.
4Create an environment file for the Unleash app.
    * Naviate to the `third-party-apps/unleash` directory.
    * Create a copy of the `.env.default` and rename it to `.env`.
    * Fill out the environment variable values with the correct values.

## Starting Local Development
1. Ensure that Docker is started and running on your machine.
2. In the root directory, run the following command: `npm run local:containers:start`
3. In the root directory, run the following command: `npm run local:dev:tpa:unleash`
4. In the root directory, run the following command: `npm run local:dev:web:server`
5. In the root directory, run the following command: `npm run local:dev:web:ui`
