FROM node:12.16.3-buster-slim

# Set the environment variables that are common across ALL deployments of this image.
ENV APP_PORT=8080
ENV DB_HOST=""
ENV DB_NAME=""
ENV DB_PASSWORD=""
ENV DB_PORT=""
ENV DB_USER=""

USER root

# Set the working directory for the application.
WORKDIR /app

# Copy all of the server files to the expected locations.
COPY ./package.json /app/package.json
COPY ./package-lock.json /app/package-lock.json
COPY ./server.js /app/server.js

# Change the permissions of the folders and files that we just copied and created.
RUN chown -R node:node /app \
    && chmod -R 755 /app

# Switch to the user that was created in the Node.js image.
USER node

# Install all of the dependencies for the application.
RUN npm install --production

# Expose the port that the container will bind to on launch, assuming the value is not overridden.
EXPOSE 8081

# Run the default command to launch the server.
CMD ["npm", "start"]
