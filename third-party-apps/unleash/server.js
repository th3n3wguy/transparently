'use strict';

// Import 3rd-party libraries.
const unleash = require('unleash-server');
process.env.NODE_ENV !== 'production' ? require('dotenv').config() : () => {};

// Local variables.
const {
  APP_PORT,
  DB_HOST,
  DB_NAME,
  DB_PASSWORD,
  DB_PORT,
  DB_USER,
} = process.env;

unleash.start({ databaseUrl: `postgres://${DB_USER}:${DB_PASSWORD}@${DB_HOST}:${DB_PORT}/${DB_NAME}`, port: APP_PORT || 4242 })
  .then((unleash) => {
    console.log(`Unleash Server started on ${unleash.app.get('port')}`);
  });
