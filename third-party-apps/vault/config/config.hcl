
# https://www.vaultproject.io/docs/configuration/storage/filesystem
storage "file" {
  path = "/vault/file"
}

listener "tcp" {
  address = "0.0.0.0:8200"
  tls_disable = 1
}

cache_size = 32000

disable_cache = false

disable_mlock = true

log_level = "warn"

log_format = "standard"

default_lease_ttl = "768h"

max_lease_ttl = "768h"

ui = true
