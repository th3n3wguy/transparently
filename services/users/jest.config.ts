// Import 3rd-party libraries.
import { Config } from '@jest/types';

// Configuration definition.
const config: Config.InitialOptions = {
  collectCoverage: true,
  collectCoverageFrom: [
    '<rootDir>/{api,config,store}/**/*.ts',
    '!<rootDir>/api/index.ts',
    '!<rootDir>/api/v0/index.ts',
    '!<rootDir>/config/**',
    '!<rootDir>/**/*.{d,spec,old}.ts',
    '!<rootDir>/store/{migrations,seeds}/**',
    '!<rootDir>/store/{migrate-database,seed-database}.ts',
    '!<rootDir>/store/helpers/**/*.ts'
  ],
  coverageThreshold: {
    global: {
      branches: 95,
      functions: 95,
      lines: 95,
      statements: 95
    }
  },
  coverageReporters: ['json', 'lcov', 'text'],
  roots: [
    '<rootDir>/api',
    '<rootDir>/config',
    '<rootDir>/store'
  ],
  setupFiles: [
    'dotenv/config'
  ],
  setupFilesAfterEnv: [
    'jest-extended'
  ],
  transform: {
    '^.+\\.tsx?$': 'ts-jest'
  }
};

export default config;
