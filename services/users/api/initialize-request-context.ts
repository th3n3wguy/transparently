// Import 3rd party libraries.
import { v4 as uuidV4 } from 'uuid';

// Import types.
import {
  NextFunction,
  Request,
  Response
} from 'express';
import { Session } from 'express-session';

// Import local files.
import { appLogger } from '../config/logger';

export interface ModifiedRequest extends Request {
  requestId?: string|void;
  session: Session
}

export default function initializeRequestContext(req: ModifiedRequest, res: Response, next: NextFunction): void {
  try {
    // Set the request ID on the request object.
    req.requestId = uuidV4();

    // Add the logger's context value and set it as the ID for the user's session from express-session.
    appLogger.addContext('requestId', req.requestId);

    // Log some details so that we can understand the relationship between a Session ID (multiple requests) vs. a single request to the server.
    appLogger.debug(`Session ID: ${req.session.id}`);

    // Go to the next step in the application.
    return next();
  }
  catch (err) {
    return next(err);
  }
}
