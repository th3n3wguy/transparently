// Require 3rd party libraries.
import { isNil as _isNil } from 'lodash';

// Import types.
import { NextFunction, Response } from 'express';
import { ModifiedRequest } from '../initialize-request-context';

// Import local files.
import { appLogger } from '../../config/logger';
import RouteNotFoundError from '../v0/exceptions/RouteNotFoundError';

// TODO: Need to move this functionality to a private NPM repository so that other services do not have to duplicate it.

export default function routeNotFound(req: ModifiedRequest, res: Response, next: NextFunction): void {
  appLogger.debug(`Headers Sent: ${res.headersSent}`);

  // If the headers are not already set...
  _isNil(res.headersSent) || !res.headersSent
    ? next(new RouteNotFoundError(req.url, `The requested resource (${req.url}) was not found.`))
    : next();
}
