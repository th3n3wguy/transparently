// Import 3rd-party libraries.
import { validate as validateUUID } from 'uuid';
import SpyInstance = jest.SpyInstance;

// Import local files.
import { default as initializeRequestContext, ModifiedRequest } from './initialize-request-context';
import { Response } from 'express';
import { appLogger } from '../config/logger';
import request from '../tests/mocks/request';
import response from '../tests/mocks/response';

describe('/api/initialize-request-context', () => {
  test('should be a Function', () => {
    expect(initializeRequestContext).toEqual(expect.any(Function));
  });

  describe('when invoked', () => {
    let appLoggerDebug: SpyInstance;
    let appLoggerAddContext: SpyInstance;

    beforeEach(() => {
      appLoggerDebug = jest.spyOn(appLogger, 'debug');
      appLoggerAddContext = jest.spyOn(appLogger, 'addContext');
    });

    afterEach(() => {
      jest.clearAllMocks();
    });

    test('should invoke without throwing an error', () => {
      return new Promise((resolve) => {
        initializeRequestContext(<ModifiedRequest>request, <Response>response, () => {
          expect(true).toEqual(true);

          return resolve(null);
        });
      });
    });

    test('should add the .requestId property to the request object', () => {
      return new Promise((resolve) => {
        initializeRequestContext(<ModifiedRequest>request, <Response>response, () => {
          expect(validateUUID(<string>request.requestId)).toBeTruthy();

          return resolve(null);
        });
      });
    });

    test('should add the .requestId property to the context of the logger and log the Session as a "debug" entry', () => {
      return new Promise((resolve) => {
        initializeRequestContext(<ModifiedRequest>request, <Response>response, () => {
          expect(appLoggerDebug).toHaveBeenCalledTimes(1);
          expect(appLoggerAddContext).toHaveBeenCalledTimes(1);

          return resolve(null);
        });
      });
    });

    test('should return an error in the "next" function when an Error is caught', () => {
      return new Promise((resolve) => {
        // Need to mock the implementation so that we can force it to throw an Error when executed.
        appLoggerDebug.mockImplementation(() => {
          throw new Error('Sample error.');
        });

        initializeRequestContext(<ModifiedRequest>request, <Response>response, (err?: Error|string) => {
          expect(err).toEqual(expect.any(Error));

          return resolve(null);
        });
      });
    });
  });
});
