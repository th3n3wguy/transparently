// Import 3rd party libraries.
import { RequestHandler, Router as expressRouter } from 'express';

// Import route handlers.
import getUsers from './handlers/get-users';
import createUser from './handlers/create-user';
import livenessCheck from './handlers/liveness-check';
import readinessCheck from './handlers/readiness-check';

// Define the router that will be used to set up these sub-routes.
const router: expressRouter = expressRouter({ mergeParams: true });

router.route('/health/liveness')
  .get(<RequestHandler>livenessCheck);

router.route('/health/readiness')
  .get(<RequestHandler>readinessCheck);

router.route('/users')
  .get(<RequestHandler>getUsers)
  .post(<RequestHandler>createUser);

export default router;
