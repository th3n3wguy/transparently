// Import local files.
import { default as UnprocessableEntityError } from './';

describe('api/v0/exceptions/UnprocessableEntityError', () => {
  test('should be a Function', () => {
    expect(UnprocessableEntityError).toEqual(expect.any(Function));
  });

  describe('when instantiated', () => {
    let errorInstance: UnprocessableEntityError;

    test.each(
      [
        ['E_BAD_ERROR', ['test1', 'test2'], 'Some log message.'],
        ['E_UNKNOWN_ERROR', [123, 456], 'Another log message.']
      ]
    )('should set the values provided on the expected properties', (errorCode, context, logMessage) => {
      errorInstance = new UnprocessableEntityError(errorCode, context, logMessage);

      expect(errorInstance.code).toEqual(errorCode);
      expect(errorInstance.context).toEqual(context);
      expect(errorInstance.message).toEqual(logMessage);
    });
  });
});
