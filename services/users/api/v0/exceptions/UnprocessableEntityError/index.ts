// Import local files.
import ApiError from '../ApiError';

/**
 * @class UnprocessableEntityError
 * @extends ApiError
 */
export default class UnprocessableEntityError extends ApiError {
  constructor(code: string, context: (string|number|boolean)[], logMessage: string) {
    super(code, context, logMessage);
    this.name = this.constructor.name;
    Error.captureStackTrace(this, this.constructor);
  }
}
