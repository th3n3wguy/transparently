// Import 3rd-party libraries.
import { noop as _noop } from 'lodash';

// Import local files.
import {
  default as ApiError,
  defaultErrorCode
} from './';

describe('api/v0/exceptions/ApiError', () => {
  test('should be a Function', () => {
    expect(ApiError).toEqual(expect.any(Function));
  });

  describe('when instantiated', () => {
    let errorInstance: ApiError;

    test.each(
      [
        ['SOME_ERROR', 'SOME_ERROR', ['test1', 'test2'], ['test1', 'test2'], 'Here is a sample log message.', 'Here is a sample log message.'],
        ['SOME_ERROR', 'SOME_ERROR', ['test1', 'test2'], ['test1', 'test2'], _noop(), ''],
        ['SOME_ERROR', 'SOME_ERROR', _noop(), [], _noop(), ''],
        [_noop(), defaultErrorCode, _noop(), [], _noop(), '']
      ]
    )('should set the values provided on the expected properties', (providedErrorCode, expectedErrorCode, providedContext, expectedContext, providedLogMessage, expectedLogMessage) => {
      errorInstance = new ApiError(providedErrorCode, providedContext, providedLogMessage);

      expect(errorInstance.code).toEqual(expectedErrorCode);
      expect(errorInstance.context).toEqual(expectedContext);
      expect(errorInstance.message).toEqual(expectedLogMessage);
    });
  });
});
