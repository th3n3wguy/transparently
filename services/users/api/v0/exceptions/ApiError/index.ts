export const defaultErrorCode = 'E_GENERAL_SERVER_ERROR';

/**
 * @class ApiError
 * @extends Error
 */
export default class ApiError extends Error {
  code: string;
  context: (string|number|boolean|Record<string, any>)[];

  constructor(code?: string|void, context?: (string|number|boolean)[]|void, logMessage?: string|void) {
    super(logMessage || '');
    this.name = this.constructor.name;
    this.code = code || defaultErrorCode;
    this.context = context || [];
    Error.captureStackTrace(this, this.constructor);
  }
}
