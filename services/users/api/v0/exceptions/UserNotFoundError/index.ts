// Import local files.
import ApiError from '../ApiError';

export const userNotFoundErrorCode = 'E_USER_NOT_FOUND';

/**
 * @class UserNotFoundError
 * @extends ApiError
 */
export default class UserNotFoundError extends ApiError {
  constructor(userId: string, logMessage: string) {
    super(userNotFoundErrorCode, [userId], logMessage);
    this.name = this.constructor.name;
    Error.captureStackTrace(this, UserNotFoundError);
  }
}
