// Import types.
import { User as StoreUser } from '../../../store/types/users';

export namespace User {
  export interface Base {
    username: StoreUser.UserManagedProperties['username'];
    emailAddress: StoreUser.UserManagedProperties['email_address'];
    fullName: StoreUser.UserManagedProperties['full_name'];
  }

  export interface ResponseObject extends Base {
    userId: StoreUser.ServerManagedProperties['user_id'];
    createdOn: StoreUser.ServerManagedProperties['created_on'];
    lastUpdatedOn: StoreUser.ServerManagedProperties['last_updated_on'];
  }
}
