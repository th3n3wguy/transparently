/**
 * @apiDefine InternalServerError
 *
 * @apiError (500 - InternalServerError) {Object} ApiError
 * @apiError (500 - InternalServerError) {String} ApiError.code A code that precisely describes the error which occurred on the server.
 * @apiError (500 - InternalServerError) {Any[]} ApiError.context An array of values that contain additional context for why the error was returned.
 * @apiError (500 - InternalServerError) {String} ApiError.referenceId A UUID that allows for support to render assistance.
 */

/**
 * @apiDefine UnprocessableEntityError
 *
 * @apiError (422 - Unprocessable Entity) {Object} ApiError
 * @apiError (422 - Unprocessable Entity) {String} error.code A code that precisely describes the error which occurred on the server.
 * @apiError (422 - Unprocessable Entity) {Any[]} error.context An array of values that contain additional context for why the error was returned.
 * @apiError (422 - Unprocessable Entity) {String} error.referenceId A UUID that allows for support to render assistance.
 */
