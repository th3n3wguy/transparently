// Import local files.
import ApiError from '../exceptions/ApiError';

export interface ApiErrorBody {
  referenceId: string;
  code: string;
  context: (string|number|boolean|Record<string, any>)[];
}

/**
 * @typedef {Function} GetApiErrorBody
 * @param {ApiError} err - The Error object that is set for the request.
 * @param {string} [referenceId] - The unique ID that is set for the individual request.
 * @returns {ApiErrorBody} - The JSON representation of the response body that will be returned.
 */
export default function getApiErrorBodyFromApiError(err: ApiError, referenceId?: string|void): ApiErrorBody {
  return {
    referenceId: referenceId || '',
    code: err.code,
    context: err.context
  };
}

export function getApiErrorBody(code: string, context: (string|number|boolean|Record<string, any>)[], requestId: string|void): ApiErrorBody {
  return {
    referenceId: requestId || '',
    code,
    context
  };
}
