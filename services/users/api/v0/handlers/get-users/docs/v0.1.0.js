/**
 * @api {get} /api/v0/users Get List of Users
 * @apiGroup Users
 * @apiName GetListOfUsers
 * @apiVersion 0.1.0
 * @apiDescription Allows for the retrieval of a list of Users within the system.
 *
 * @apiParam {string{..255}} username The username that will be created for the user. This must be unique, as usernames are unique within the system and what is used to authenticate against the system.
 *
 * @apiSuccess (200 - OK) {Object} user
 * @apiSuccess (200 - OK) {Number} user.userId The unique identifier that is created by the system and associated to the User's account.
 * @apiSuccess (200 - OK) {String{..255}} user.username The unique username created by the User.
 * @apiSuccess (200 - OK) {String{..255}} user.emailAddress The email address associated to the User.
 * @apiSuccess (200 - OK) {String{..255}} user.full_name The full name value that will be displayed in certain places within the application that is provided by the User.
 */
