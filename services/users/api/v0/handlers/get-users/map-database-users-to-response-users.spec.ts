// Import 3rd-party libraries.
import {
  noop as _noop,
  size as _size
} from 'lodash';

// Import local files.
import { default as mapDatabaseUsersToResponseUsers } from './map-database-users-to-response-users';
import { User as StoreUser } from '../../../../store/types/users';
import ApiError from '../../exceptions/ApiError';
import { getValidDatabaseUser } from '../../../../tests/mocks/user';

describe('/api/v0/handlers/get-users/map-database-users-to-response-users', () => {
  test('should be a Function', () => {
    expect(mapDatabaseUsersToResponseUsers).toEqual(expect.any(Function));
  });

  describe('when invoked', () => {
    const dateConstant: Date = new Date();
    const databaseUsers: StoreUser.All[] = [
      getValidDatabaseUser({ created_on: dateConstant, last_updated_on: dateConstant }),
      getValidDatabaseUser({ created_on: dateConstant, last_updated_on: dateConstant })
    ];

    test('that the user objects are converted to the correct format when provided the expected data', () => {
      return new Promise((resolve, reject) => {
        mapDatabaseUsersToResponseUsers(databaseUsers)
          .then((users) => {
            // The arrays should be the exact same length.
            expect(users).toHaveLength(_size(databaseUsers));

            // Loop over each object in the array to make sure that each property matches values in the same index within the respective mapped array.
            for (let i = 0; i < _size(databaseUsers); i++) {
              expect(users[i].userId).toEqual(databaseUsers[i].user_id);
              expect(users[i].username).toEqual(databaseUsers[i].username);
              expect(users[i].emailAddress).toEqual(databaseUsers[i].email_address);
              expect(users[i].fullName).toEqual(databaseUsers[i].full_name);
              expect(users[i].createdOn).toEqual(databaseUsers[i].created_on);
              expect(users[i].lastUpdatedOn).toEqual(databaseUsers[i].last_updated_on);
            }

            return resolve(null);
          })
          .catch(reject);
      });
    });

    test.each(
      [
        [_noop()],
        [null],
        [1234],
        ['1234'],
        [true],
        [false]
      ]
    )('the function should return a rejected Promise with an ApiError exception if the value provided is not an array', (invalidValue) => {
      return new Promise((resolve, reject) => {
        mapDatabaseUsersToResponseUsers(<StoreUser.All[]><unknown>invalidValue)
          .then(() => reject(new Error(`This error should not trigger for the following value: ${invalidValue}`)))
          .catch((err) => {
            expect(err).toBeInstanceOf(ApiError);

            return resolve(null);
          });
      });
    });
  });
});
