// Set up mocks before any modules are imported.
import { invalidLimitErrorCode } from './validate-query-parameters';

jest.mock('./convert-query-params-to-database-params');

// Import 3rd-party libraries.
import SpyInstance = jest.SpyInstance;
import { Response } from 'express';
import { ParsedQs } from 'qs';
import {
  assign as _assign,
  get as _get,
  noop as _noop,
  size as _size
} from 'lodash';
import { StatusCodes } from 'http-status-codes';
import { v4 } from 'uuid';

// Import local files.
import { default as getUsers } from './';
import { appLogger } from '../../../../config/logger';
import store from '../../../../store';
import request from '../../../../tests/mocks/request';
import response from '../../../../tests/mocks/response';
import { ModifiedRequest } from '../../../initialize-request-context';
import convertQueryParamsToDatabaseParams from './convert-query-params-to-database-params';
import {
  default as ApiError,
  defaultErrorCode
} from '../../exceptions/ApiError';
import { getValidDatabaseUser } from '../../../../tests/mocks/user';

describe('/api/v0/handlers/get-users', () => {
  test('should be a Function', () => {
    expect(getUsers).toEqual(expect.any(Function));
  });

  describe('when invoked', () => {
    let appLoggerInfo: SpyInstance;
    let appLoggerDebug: SpyInstance;
    let storeGetUsers: SpyInstance;
    const validRequestParams: ParsedQs = {
      limit: '50',
      offset: '0',
      sortDirection: 'ASC',
      sortKey: 'username'
    };

    beforeEach(() => {
      appLoggerDebug = jest.spyOn(appLogger, 'debug').mockImplementation();
      appLoggerInfo = jest.spyOn(appLogger, 'info').mockImplementation();
      request.requestId = v4();
    });

    afterEach(() => {
      jest.clearAllMocks();
      jest.resetModules();
      request.requestId = _noop();
      request.query = {};
      response.locals = {};
    });

    test('a successful execution', () => {
      // Set up the spies / mocks that are needed in this test.
      request.query = validRequestParams;
      storeGetUsers = jest.spyOn(store.queries, 'getUsers').mockResolvedValue([]);

      return new Promise((resolve) => {
        // Execute the function to be tested.
        getUsers(<ModifiedRequest>request, <Response>response, (err?: Error|string) => {
          // Set the expectations.
          expect(err).toBeUndefined();
          expect(appLoggerInfo).toHaveBeenCalledTimes(1);
          expect(appLoggerDebug).toHaveBeenCalledTimes(1);
          expect(convertQueryParamsToDatabaseParams).toHaveBeenCalledTimes(1);
          expect(storeGetUsers).toHaveBeenCalledTimes(1);

          // Ensure that the Promise is resolved if all of the expectations are successful.
          return resolve(null);
        });
      });
    });

    test('the "response.locals" values are set properly on a successful execution', () => {
      // Set up the spies / mocks / values that are used specifically within this test.
      const usersFromDatabaseQuery = [
        getValidDatabaseUser({}),
        getValidDatabaseUser({})
      ];

      request.query = validRequestParams;
      storeGetUsers = jest.spyOn(store.queries, 'getUsers').mockResolvedValue(usersFromDatabaseQuery);

      return new Promise((resolve) => {
        // Execute the function to be tested.
        getUsers(<ModifiedRequest>request, <Response>response, (err?: Error|string) => {
          // Set the expectations.
          expect(err).toBeUndefined();
          expect(response.locals?.status).toEqual(StatusCodes.OK);
          expect(response.locals?.body).toHaveLength(_size(usersFromDatabaseQuery));

          // Ensure that the Promise is resolved if all of the expectations are successful.
          return resolve(null);
        });
      });
    });

    test('the "response.locals" values are set when an "UnprocessableEntityError" is thrown within the "validateQueryParameters()" function', () => {
      // Set up the spies / mocks / values that are used specifically within this test.
      const invalidLimit: string = '-50';

      request.query = _assign({}, validRequestParams, {
        limit: invalidLimit
      });

      storeGetUsers = jest.spyOn(store.queries, 'getUsers').mockResolvedValue([]);

      return new Promise((resolve) => {
        // Execute the function to be tested.
        getUsers(<ModifiedRequest>request, <Response>response, (err?: Error|string) => {
          // Set the expectations.
          expect(err).toBeUndefined();
          expect(response.locals?.status).toEqual(StatusCodes.UNPROCESSABLE_ENTITY);
          expect(response.locals?.body).toEqual({
            referenceId: request.requestId,
            code: invalidLimitErrorCode,
            context: [invalidLimit]
          });

          // Ensure that the Promise is resolved if all of the expectations are successful.
          return resolve(null);
        });
      });
    });

    test('the "response.locals" values are set when an "ApiError" is thrown within the "mapDatabaseUsersToResponseUsers()" function', () => {
      // Set up the spies / mocks / values that are used specifically within this test.
      request.query = validRequestParams;
      storeGetUsers = jest.spyOn(store.queries, 'getUsers')
        .mockRejectedValue(new ApiError(defaultErrorCode, [], 'Bad query.'));

      return new Promise((resolve) => {
        // Execute the function to be tested.
        getUsers(<ModifiedRequest>request, <Response>response, (err?: Error|string) => {
          // Set the expectations.
          expect(err).toBeUndefined();
          expect(response.locals?.status).toEqual(StatusCodes.INTERNAL_SERVER_ERROR);
          expect(response.locals?.body).toEqual({
            referenceId: request.requestId,
            code: defaultErrorCode,
            context: []
          });

          // Ensure that the Promise is resolved if all of the expectations are successful.
          return resolve(null);
        });
      });
    });

    test('an Error object should be returned as the callback function\'s parameter when a generic Error is caught', () => {
      // Set up the spies / mocks / values that are used specifically within this test.
      const errorMessage: string = 'Bad query.';

      request.query = validRequestParams;
      storeGetUsers = jest.spyOn(store.queries, 'getUsers').mockRejectedValue(new Error(errorMessage));

      return new Promise((resolve) => {
        // Invoke the function to be tested.
        getUsers(<ModifiedRequest>request, <Response>response, (err?: Error|string) => {
          // Set the expectations.
          expect(err).toBeInstanceOf(Error);
          expect(_get(err, 'message')).toEqual(errorMessage);

          // Ensure that the Promise is resolved if all of the expectations are successful.
          return resolve(null);
        });
      });
    });
  });
});
