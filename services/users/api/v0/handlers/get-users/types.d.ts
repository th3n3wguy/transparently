// Import types.
import { GetUsersQueryParams } from '../../../../store/queries/get-users';

export interface ValidatedQueryParameters {
  limit: string|number;
  offset: string|number;
  sortDirection: GetUsersQueryParams['sortDirection'];
  sortKey: 'username'|'createdOn'|'lastUpdatedOn';
}
