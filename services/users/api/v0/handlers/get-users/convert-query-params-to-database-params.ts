// Import 3rd-party libraries.
import { toFinite as _toFinite } from 'lodash';

// Import types.
import { ValidatedQueryParameters } from './types';
import { GetUsersQueryParams } from '../../../../store/queries/get-users';

const sortKeyToDatabaseTableMap = {
  username: 'username',
  createdOn: 'created_on',
  lastUpdatedOn: 'last_updated_on'
};

export default function convertQueryParamsToDatabaseParams({
  limit,
  offset,
  sortDirection,
  sortKey
}: ValidatedQueryParameters): Promise<GetUsersQueryParams> {
  return new Promise((resolve) => {
    return resolve({
      limit: _toFinite(limit),
      offset: _toFinite(offset),
      sortDirection,
      sortKey: <GetUsersQueryParams['sortKey']>sortKeyToDatabaseTableMap[sortKey]
    });
  });
}
