// Import 3rd-party libraries.
import {
  gte as _gte,
  lte as _lte,
  includes as _includes,
  isEqual as _isEqual,
  round as _round,
  toFinite as _toFinite,
  toString as _toString
} from 'lodash';
import { ParsedQs } from 'qs';

// Import local files.
import { ValidatedQueryParameters } from './types';
import UnprocessableEntityError from '../../exceptions/UnprocessableEntityError';

export const invalidLimitErrorCode = 'E_INVALID_LIMIT';
export const invalidOffsetErrorCode = 'E_INVALID_OFFSET';
export const invalidSortDirectionErrorCode = 'E_INVALID_SORT_DIRECTION';
export const invalidSortKeyErrorCode = 'E_INVALID_SORT_KEY';

export default function validateQueryParameters({
  limit = '50',
  offset = '0',
  sortKey = 'username',
  sortDirection = 'ASC'
}: ParsedQs): Promise<ValidatedQueryParameters> {
  return new Promise((resolve, reject) => {
    if (!isLimitValid(limit)) {
      return reject(new UnprocessableEntityError(invalidLimitErrorCode, [<string>limit], `The "limit" value provided (${limit}) was not valid.`));
    }
    else if (!isOffsetValid(offset)) {
      return reject(new UnprocessableEntityError(invalidOffsetErrorCode, [<string>offset], `The "offset" value provided (${offset}) was not valid.`));
    }
    else if (!isSortDirectionValid(sortDirection)) {
      return reject(new UnprocessableEntityError(invalidSortDirectionErrorCode, [<string>sortDirection], `The "sortDirection" provided (${sortDirection}) was not valid.`));
    }
    else if (!isSortKeyValid(sortKey)) {
      return reject(new UnprocessableEntityError(invalidSortKeyErrorCode, [<string>sortKey], `The "sortKey" provided (${sortKey}) was not valid.`));
    }
    else {
      return resolve({
        limit: _toString(limit),
        offset: _toString(offset),
        sortDirection: <ValidatedQueryParameters['sortDirection']>sortDirection,
        sortKey: <ValidatedQueryParameters['sortKey']>sortKey
      });
    }
  });
}

function isLimitValid(limit?: ParsedQs['string']): boolean {
  // We assume that there is no reason that a request would provide a value of 0 for the limit and that we do not allow for a limit of greater than 100.
  return _isEqual(limit, '0')
    || (!_isEqual(limit, '0') && isWholeNumber(<string>limit) && _lte(_toFinite(limit), 100) && _gte(_toFinite(limit), 0));
}

function isOffsetValid(offset: ParsedQs['string']): boolean {
  return _isEqual(offset, '0')
    || (!_isEqual(offset, '0') && isWholeNumber(<string>offset) && !isInfinity(<string>offset) && _gte(_toFinite(offset), 0));
}

function isSortDirectionValid(sortDirection: ParsedQs['string']): boolean {
  // Need to ignore the case.
  return _includes(['ASC', 'DESC', 'asc', 'desc'], sortDirection);
}

function isSortKeyValid(sortKey: ParsedQs['string']): boolean {
  return _includes(['username', 'createdOn', 'lastUpdatedOn'], sortKey);
}

function isWholeNumber(value: string) {
  return _isEqual(_round(_toFinite(value)), _toFinite(value));
}

function isInfinity(value: string|number) {
  return _includes([Infinity, -Infinity, 'Infinity', '-Infinity'], value);
}
