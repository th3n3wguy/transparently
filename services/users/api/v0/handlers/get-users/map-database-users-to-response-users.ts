// Import 3rd-party libraries.
import {
  isArray as _isArray,
  map as _map
} from 'lodash';

// Import local files.
import { User } from '../../types/user';
import { User as StoreUser } from '../../../../store/types/users';
import {
  default as ApiError,
  defaultErrorCode
} from '../../exceptions/ApiError';

export default function mapDatabaseUsersToResponseUsers(users: StoreUser.All[]): Promise<User.ResponseObject[]> {
  return new Promise((resolve, reject) => {
    return !_isArray(users)
      ? reject(new ApiError(defaultErrorCode, [], 'The list of users provided to the function is not an array.'))
      : resolve(_map(users, ({
        user_id,
        username,
        email_address,
        full_name,
        created_on,
        last_updated_on
      }) => ({
        userId: user_id,
        username,
        emailAddress: email_address,
        fullName: full_name,
        createdOn: created_on,
        lastUpdatedOn: last_updated_on
      })));
  });
}
