// Import 3rd-party libraries.
import { StatusCodes } from 'http-status-codes';
import { NextFunction, Response } from 'express';

// Import local files.
import { appLogger } from '../../../../config/logger';
import convertQueryParamsToDatabaseParams from './convert-query-params-to-database-params';
import mapDatabaseUsersToResponseUsers from './map-database-users-to-response-users';
import store from '../../../../store';
import validateQueryParameters from './validate-query-parameters';
import { User } from '../../types/user';
import { ModifiedRequest } from '../../../initialize-request-context';
import UnprocessableEntityError from '../../exceptions/UnprocessableEntityError';
import getApiErrorBodyFromApiError from '../../helpers/get-api-error-body-from-api-error';
import ApiError from '../../exceptions/ApiError';

export default function getUsers(req: ModifiedRequest, res: Response, next: NextFunction): void {
  appLogger.info('Request Handler: Get Users starting');
  appLogger.debug(`Request Body: ${JSON.stringify(req.query)}`);

  validateQueryParameters(req.query)
    .then(convertQueryParamsToDatabaseParams)
    .then(store.queries.getUsers)
    .then(mapDatabaseUsersToResponseUsers)
    .then((users: User.ResponseObject[]) => {
      res.locals.status = StatusCodes.OK;
      res.locals.body = users;

      return next();
    })
    .catch((err: Error) => {
      if (err instanceof UnprocessableEntityError) {
        res.locals.status = StatusCodes.UNPROCESSABLE_ENTITY;
        res.locals.body = getApiErrorBodyFromApiError(err, req.requestId);

        return next();
      }
      else if (err instanceof ApiError) {
        res.locals.status = StatusCodes.INTERNAL_SERVER_ERROR;
        res.locals.body = getApiErrorBodyFromApiError(err, req.requestId);

        return next();
      }
      else {
        return next(err);
      }
    });
}
