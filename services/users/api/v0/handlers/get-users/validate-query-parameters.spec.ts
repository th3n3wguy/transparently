// Import 3rd-party libraries.
import { ParsedQs } from 'qs';
import { assign as _assign } from 'lodash';

// Import local files.
import {
  default as validateQueryParameters,
  invalidLimitErrorCode,
  invalidSortDirectionErrorCode,
  invalidSortKeyErrorCode
} from './validate-query-parameters';
import UnprocessableEntityError from '../../exceptions/UnprocessableEntityError';

describe('/api/v0/handlers/get-users/validate-query-parameters', () => {
  test('should be a Function', () => {
    expect(validateQueryParameters).toEqual(expect.any(Function));
  });

  describe('when invoked', () => {
    const validParameters: ParsedQs = {
      limit: '50',
      offset: '0',
      sortKey: 'username',
      sortDirection: 'ASC'
    };

    test('should return back the default parameters in a resolved Promise when no parameters are provided', () => {
      return new Promise((resolve, reject) => {
        validateQueryParameters({})
          .then(({
            limit,
            offset,
            sortDirection,
            sortKey
          }) => {
            // Set the expectations.
            expect(limit).toEqual('50');
            expect(offset).toEqual('0');
            expect(sortDirection).toEqual('ASC');
            expect(sortKey).toEqual('username');

            return resolve(null);
          })
          .catch(reject);
      });
    });

    test('should return back the parameters in a resolved Promise when provided valid parameters', () => {
      return new Promise((resolve, reject) => {
        // Invoke the function to be tested.
        validateQueryParameters(validParameters)
          .then(({
            limit,
            offset,
            sortDirection,
            sortKey
          }) => {
            // Set the expectations.
            expect(limit).toEqual(expect.any(String));
            expect(limit).toEqual(validParameters?.limit);
            expect(offset).toEqual(expect.any(String));
            expect(offset).toEqual(validParameters?.offset);
            expect(sortKey).toEqual(validParameters?.sortKey);
            expect(sortDirection).toEqual(validParameters?.sortDirection);

            return resolve(null);
          })
          .catch(reject);
      });
    });

    test.each(
      [
        [101],
        ['101'],
        [-Infinity],
        [Infinity],
        ['Infinity'],
        ['-Infinity'],
        [-1],
        ['-1'],
        [123050234],
        [-423485],
        [34598.23489],
        ['987234.01348'],
        [98.234958],
        ['98.234958']
      ]
    )('should return a rejected Promise with an UnprocessableEntityError when the "limit" parameter is not valid', (invalidLimit) => {
      return new Promise((resolve, reject) => {
        validateQueryParameters(_assign({}, validParameters, { limit: invalidLimit }))
          .then(() => reject(new Error(`This should not have occurred for ${invalidLimit}.`)))
          .catch((err) => {
            expect(err).toEqual(new UnprocessableEntityError(invalidLimitErrorCode, [invalidLimit], `The "limit" value provided (${invalidLimit}) was not valid.`));

            return resolve(null);
          });
      });
    });

    test.each(
      [
        [-1],
        ['-1'],
        [Infinity],
        [-Infinity],
        ['Infinity'],
        ['-Infinity'],
        [-2398345.0034235],
        ['4897235.12305']
      ]
    )('should return a rejected Promise with an UnprocessableEntityError when the "offset" parameter is not valid', (invalidOffset) => {
      return new Promise((resolve, reject) => {
        validateQueryParameters(_assign({}, validParameters, { offset: invalidOffset }))
          .then(() => reject(new Error(`This should not have occurred for ${invalidOffset}.`)))
          .catch((err) => {
            expect(err).toEqual(new UnprocessableEntityError(invalidLimitErrorCode, [invalidOffset], `The "offset" value provided (${invalidOffset}) was not valid.`));

            return resolve(null);
          });
      });
    });

    test.each(
      [
        ['-1'],
        [-1],
        ['ascending'],
        ['Asc'],
        ['ASc'],
        ['AsC'],
        ['asC'],
        ['descending'],
        ['Desc'],
        ['DEsc'],
        ['DESc'],
        ['desC'],
        ['deSC'],
        ['dESC'],
        ['DeSc'],
        ['DeSC'],
        ['dEsC'],
        ['dEsc']
      ]
    )('should return a rejected Promise with an UnprocessableEntityError when the "sortDirection" parameter is not valid', (invalidSortDirection) => {
      return new Promise((resolve, reject) => {
        validateQueryParameters(_assign({}, validParameters, { sortDirection: invalidSortDirection }))
          .then(() => reject(new Error(`This should not have occurred for ${invalidSortDirection}.`)))
          .catch((err) => {
            expect(err).toEqual(new UnprocessableEntityError(invalidSortDirectionErrorCode, [invalidSortDirection], `The "sortDirection" provided (${invalidSortDirection}) was not valid.`));

            return resolve(null);
          });
      });
    });

    test.each(
      [
        ['user_name'],
        ['created_on'],
        ['last_updated_on'],
        ['createdon'],
        ['lastupdatedon'],
        ['lastupdated_on'],
        ['last_updatedon']
      ]
    )('should return a rejected Promise with an UnprocessableEntityError when the "sortKey" parameter is not valid', (invalidSortKey) => {
      return new Promise((resolve, reject) => {
        validateQueryParameters(_assign({}, validParameters, { sortKey: invalidSortKey }))
          .then(() => reject(new Error(`This should not have occurred for ${invalidSortKey}.`)))
          .catch((err) => {
            expect(err).toEqual(new UnprocessableEntityError(invalidSortKeyErrorCode, [invalidSortKey], `The "sortKey" provided (${invalidSortKey}) was not valid.`));

            return resolve(null);
          });
      });
    });
  });
});
