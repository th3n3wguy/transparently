// Import 3rd-party libraries.
import SpyInstance = jest.SpyInstance;
import { v4 } from 'uuid';
import { Response } from 'express';
import { StatusCodes } from 'http-status-codes';
import { noop as _noop } from 'lodash';

// Import local files.
import setResponse from './';
import request from '../../../../tests/mocks/request';
import { appLogger } from '../../../../config/logger';
import response from '../../../../tests/mocks/response';
import { ModifiedRequest } from '../../../initialize-request-context';

describe('/api/v0/handlers/set-response', () => {
  test('should be a Function', () => {
    expect(setResponse).toEqual(expect.any(Function));
  });

  describe('when invoked', () => {
    let appLoggerDebug: SpyInstance;
    let responseSetStatus: SpyInstance;
    let responseSetJson: SpyInstance;

    beforeEach(() => {
      request.requestId = v4();
      appLoggerDebug = jest.spyOn(appLogger, 'debug').mockImplementation();
      responseSetStatus = jest.spyOn(response, 'status');
      responseSetJson = jest.spyOn(response, 'json');
    });

    afterEach(() => {
      request.requestId = _noop();
      response.locals = {};
      jest.clearAllMocks();
    });

    test('should call the logger and the callback function when the "response.locals.status" is undefined', () => {
      // Set up the spies / mocks / variables used specifically within this test.
      response.locals = {
        status: _noop(),
        body: _noop()
      };

      return new Promise((resolve) => {
        // Execute the function to be tested.
        setResponse(<ModifiedRequest>request, <Response>response, (err?: Error|string) => {
          // Set the expectations.
          expect(err).toBeUndefined();
          expect(appLoggerDebug).toHaveBeenCalledTimes(2);
          expect(appLoggerDebug).toHaveBeenCalledWith(`Status: ${response.locals?.status}`);
          expect(appLoggerDebug).toHaveBeenCalledWith(`Body: ${JSON.stringify(response.locals?.body)}`);
          expect(responseSetStatus).toHaveBeenCalledTimes(0);
          expect(responseSetJson).toHaveBeenCalledTimes(0);

          // Ensure that the Promise is resolved if all of the expectations are successful.
          return resolve(null);
        });
      });
    });

    test('the callback should not be invoked if the "response.locals.status" is set', () => {
      // Set up the spies / mocks / variables used specifically within this test.
      const statusCode: number = StatusCodes.INTERNAL_SERVER_ERROR;
      const responseBody: Record<string, unknown> = {};

      response.locals = {
        status: statusCode,
        body: responseBody
      };

      return new Promise((resolve, reject) => {
        // Execute the function to be tested.
        setResponse(<ModifiedRequest>request, <Response>response, () => {
          // We don't expect that this is invoked.
          return reject(new Error('The callback was invoked when it should not have been.'));
        });

        // Set the expectations.
        expect(responseSetStatus).toHaveBeenCalledTimes(1);
        expect(responseSetStatus).toHaveBeenCalledWith(statusCode);
        expect(responseSetJson).toHaveBeenCalledTimes(1);
        expect(responseSetJson).toHaveBeenCalledWith(responseBody);

        // Ensure that the Promise is resolved if all of the expectations are successful.
        return resolve(null);
      });
    });
  });
});
