// Import 3rd party libraries.
import { isUndefined as _isUndefined } from 'lodash';
import { NextFunction, Response } from 'express';

// Import types.
import { ModifiedRequest } from '../../../initialize-request-context';

// Import local files.
import { appLogger } from '../../../../config/logger';

export default function setResponse(req: ModifiedRequest, res: Response, next: NextFunction): void {
  appLogger.debug(`Status: ${res.locals.status}`);
  appLogger.debug(`Body: ${JSON.stringify(res.locals.body)}`);

  // Set the response status.
  !_isUndefined(res.locals.status)
    ? res.status(res.locals.status).json(res.locals.body)
    : next();
}
