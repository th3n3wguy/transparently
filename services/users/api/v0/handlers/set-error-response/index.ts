// Import 3rd-party libraries.
import { StatusCodes } from 'http-status-codes';
import {
  assign as _assign,
  get as _get
} from 'lodash';
import {
  NextFunction,
  Response
} from 'express';

// Import local files.
import { appLogger } from '../../../../config/logger';
import { default as ApiError, defaultErrorCode } from '../../exceptions/ApiError';
import RouteNotFoundError from '../../exceptions/RouteNotFoundError';
import UnprocessableEntityError from '../../exceptions/UnprocessableEntityError';
import UserNotFoundError from '../../exceptions/UserNotFoundError';
import { ModifiedRequest } from '../../../initialize-request-context';

export default function setErrorResponse(err: Error | ApiError | UnprocessableEntityError | RouteNotFoundError | UserNotFoundError, req: ModifiedRequest, res: Response, next: NextFunction): void {
  // Set up a base request object from which all of the response bindings below will inherit.
  const baseResponseObject = { referenceId: req.requestId };

  // Log some details about the function.
  appLogger.debug(JSON.stringify(err));
  appLogger.error(err.stack);

  // Handle any errors that would not be handled on a single route or would exist for all routes.
  if (err instanceof UnprocessableEntityError) {
    res.status(StatusCodes.UNPROCESSABLE_ENTITY)
      .json(_assign({}, baseResponseObject, { code: err.code, context: err.context }));
  }
  else if (err instanceof RouteNotFoundError || err instanceof UserNotFoundError) {
    res.status(StatusCodes.NOT_FOUND)
      .json(_assign({}, baseResponseObject, { code: err.code, context: err.context }));
  }
  else {
    res.status(StatusCodes.INTERNAL_SERVER_ERROR)
      .json(_assign({}, baseResponseObject, { code: defaultErrorCode, context: _get(err, 'context', []) }));
  }

  return next();
}
