// Import 3rd-party libraries.
import SpyInstance = jest.SpyInstance;
import { StatusCodes } from 'http-status-codes';
import { v4 } from 'uuid';
import { Response } from 'express';
import { noop as _noop } from 'lodash';

// Import local files.
import setErrorResponse from './';
import { appLogger } from '../../../../config/logger';
import { ModifiedRequest } from '../../../initialize-request-context';
import request from '../../../../tests/mocks/request';
import response from '../../../../tests/mocks/response';
import UnprocessableEntityError from '../../exceptions/UnprocessableEntityError';
import { default as RouteNotFoundError, routeNotFoundErrorCode } from '../../exceptions/RouteNotFoundError';
import { default as UserNotFoundError, userNotFoundErrorCode } from '../../exceptions/UserNotFoundError';
import { defaultErrorCode } from '../../exceptions/ApiError';

describe('/api/v0/handlers/set-error-response', () => {
  test('should be a Function', () => {
    expect(setErrorResponse).toEqual(expect.any(Function));
  });

  describe('when invoked', () => {
    let appLoggerDebug: SpyInstance;
    let appLoggerError: SpyInstance;
    let responseSetStatus: SpyInstance;
    let responseSetJson: SpyInstance;

    beforeEach(() => {
      request.requestId = v4();
      appLoggerDebug = jest.spyOn(appLogger, 'debug').mockImplementation();
      appLoggerError = jest.spyOn(appLogger, 'error').mockImplementation();
      responseSetStatus = jest.spyOn(response, 'status');
      responseSetJson = jest.spyOn(response, 'json');
    });

    afterEach(() => {
      request.requestId = _noop();
      response.locals = {};
      jest.clearAllMocks();
    });

    test('should execute without any exceptions thrown', () => {
      // Set up the spies / mocks / variables used specifically within this test.
      const generalError: Error = new Error('An example error message.');

      return new Promise((resolve) => {
        // Execute the function to be tested.
        setErrorResponse(generalError, <ModifiedRequest>request, <Response>response, (err?: Error|string) => {
          // Set the expectations.
          expect(err).toBeUndefined();
          expect(appLoggerDebug).toHaveBeenCalledTimes(1);
          expect(appLoggerDebug).toHaveBeenCalledWith(JSON.stringify(generalError));
          expect(appLoggerError).toHaveBeenCalledTimes(1);
          expect(appLoggerError).toHaveBeenCalledWith(generalError.stack);
          expect(responseSetStatus).toHaveBeenCalledTimes(1);
          expect(responseSetJson).toHaveBeenCalledTimes(1);

          // Ensure that the Promise is resolved if all of the expectations are successful.
          return resolve(null);
        });
      });
    });

    test('should handle UnprocessableEntityError errors properly', () => {
      // Set up the spies / mocks / variables used specifically within this test.
      const generalError: UnprocessableEntityError = new UnprocessableEntityError('E_BAD_EXAMPLE_CODE', [], 'An example error message.');

      return new Promise((resolve) => {
        // Execute the function to be tested.
        setErrorResponse(generalError, <ModifiedRequest>request, <Response>response, (err?: Error|string) => {
          // Set the expectations.
          expect(err).toBeUndefined();
          expect(appLoggerDebug).toHaveBeenCalledTimes(1);
          expect(appLoggerDebug).toHaveBeenCalledWith(JSON.stringify(generalError));
          expect(appLoggerError).toHaveBeenCalledTimes(1);
          expect(appLoggerError).toHaveBeenCalledWith(generalError.stack);
          expect(responseSetStatus).toHaveBeenCalledTimes(1);
          expect(responseSetStatus).toHaveBeenCalledWith(StatusCodes.UNPROCESSABLE_ENTITY);
          expect(responseSetJson).toHaveBeenCalledTimes(1);
          expect(responseSetJson).toHaveBeenCalledWith({
            referenceId: request.requestId,
            code: generalError.code,
            context: generalError.context
          });

          // Ensure that the Promise is resolved if all of the expectations are successful.
          return resolve(null);
        });
      });
    });

    test('should handle RouteNotFoundError errors properly', () => {
      // Set up the spies / mocks / variables used specifically within this test.
      const badUrl: string = '/api/v0/bad-url';
      const generalError: RouteNotFoundError = new RouteNotFoundError(badUrl, 'An example error message.');

      return new Promise((resolve) => {
        // Execute the function to be tested.
        setErrorResponse(generalError, <ModifiedRequest>request, <Response>response, (err?: Error|string) => {
          // Set the expectations.
          expect(err).toBeUndefined();
          expect(appLoggerDebug).toHaveBeenCalledTimes(1);
          expect(appLoggerDebug).toHaveBeenCalledWith(JSON.stringify(generalError));
          expect(appLoggerError).toHaveBeenCalledTimes(1);
          expect(appLoggerError).toHaveBeenCalledWith(generalError.stack);
          expect(responseSetStatus).toHaveBeenCalledTimes(1);
          expect(responseSetStatus).toHaveBeenCalledWith(StatusCodes.NOT_FOUND);
          expect(responseSetJson).toHaveBeenCalledTimes(1);
          expect(responseSetJson).toHaveBeenCalledWith({
            referenceId: request.requestId,
            code: routeNotFoundErrorCode,
            context: [badUrl]
          });

          // Ensure that the Promise is resolved if all of the expectations are successful.
          return resolve(null);
        });
      });
    });

    test('should handle UserNotFoundError errors properly', () => {
      // Set up the spies / mocks / variables used specifically within this test.
      const badUserId: string = '23rd98rpas98yr';
      const generalError: UserNotFoundError = new UserNotFoundError(badUserId, 'An example error message.');

      return new Promise((resolve) => {
        // Execute the function to be tested.
        setErrorResponse(generalError, <ModifiedRequest>request, <Response>response, (err?: Error|string) => {
          // Set the expectations.
          expect(err).toBeUndefined();
          expect(appLoggerDebug).toHaveBeenCalledTimes(1);
          expect(appLoggerDebug).toHaveBeenCalledWith(JSON.stringify(generalError));
          expect(appLoggerError).toHaveBeenCalledTimes(1);
          expect(appLoggerError).toHaveBeenCalledWith(generalError.stack);
          expect(responseSetStatus).toHaveBeenCalledTimes(1);
          expect(responseSetStatus).toHaveBeenCalledWith(StatusCodes.NOT_FOUND);
          expect(responseSetJson).toHaveBeenCalledTimes(1);
          expect(responseSetJson).toHaveBeenCalledWith({
            referenceId: request.requestId,
            code: userNotFoundErrorCode,
            context: [badUserId]
          });

          // Ensure that the Promise is resolved if all of the expectations are successful.
          return resolve(null);
        });
      });
    });

    test('should handle Error errors properly', () => {
      // Set up the spies / mocks / variables used specifically within this test.
      const generalError: Error = new Error('An example error message.');

      return new Promise((resolve) => {
        // Execute the function to be tested.
        setErrorResponse(generalError, <ModifiedRequest>request, <Response>response, (err?: Error|string) => {
          // Set the expectations.
          expect(err).toBeUndefined();
          expect(appLoggerDebug).toHaveBeenCalledTimes(1);
          expect(appLoggerDebug).toHaveBeenCalledWith(JSON.stringify(generalError));
          expect(appLoggerError).toHaveBeenCalledTimes(1);
          expect(appLoggerError).toHaveBeenCalledWith(generalError.stack);
          expect(responseSetStatus).toHaveBeenCalledTimes(1);
          expect(responseSetStatus).toHaveBeenCalledWith(StatusCodes.INTERNAL_SERVER_ERROR);
          expect(responseSetJson).toHaveBeenCalledTimes(1);
          expect(responseSetJson).toHaveBeenCalledWith({
            referenceId: request.requestId,
            code: defaultErrorCode,
            context: []
          });

          // Ensure that the Promise is resolved if all of the expectations are successful.
          return resolve(null);
        });
      });
    });
  });
});
