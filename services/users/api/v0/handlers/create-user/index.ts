// Import 3rd-party libraries.
import { StatusCodes } from 'http-status-codes';
import { NextFunction, Response } from 'express';

// Import local files.
import { ModifiedRequest } from '../../../initialize-request-context';
import { appLogger } from '../../../../config/logger';
import store from '../../../../store';
import DuplicateUsernameError from '../../../../store/exceptions/DuplicateUsernameError';
import { PostgresError } from '../../../../store/types/database-errors';
import DatabaseQueryError from '../../../../store/exceptions/DatabaseQueryError';
import { defaultErrorCode } from '../../exceptions/ApiError';
import { duplicateUsername } from '../../helpers/error-codes';
import { getApiErrorBody } from '../../helpers/get-api-error-body-from-api-error';

export default function createUser(req: ModifiedRequest, res: Response, next: NextFunction): void {
  appLogger.info('Handling request to create a User.');
  appLogger.debug(`Request Body: ${JSON.stringify(req.body)}`);

  store.commands
    .createUser({
      username: req.body.username,
      email_address: req.body.emailAddress,
      full_name: req.body.fullName
    })
    .then(() => store.queries.getUserByUsername(req.body.username))
    .then(({
      user_id,
      username,
      email_address,
      full_name,
      created_on,
      last_updated_on
    }) => {
      res.locals.status = StatusCodes.CREATED;
      res.locals.body = {
        userId: user_id,
        username,
        emailAddress: email_address,
        fullName: full_name,
        createdOn: created_on,
        updatedOn: last_updated_on
      };

      return next();
    })
    .catch((err: Error | PostgresError | DatabaseQueryError | DuplicateUsernameError) => {
      appLogger.error(err);

      if (err instanceof DuplicateUsernameError) {
        res.locals.status = StatusCodes.UNPROCESSABLE_ENTITY;
        res.locals.body = getApiErrorBody(duplicateUsername, [req.body.username], req.requestId);
      }
      else {
        res.locals.status = StatusCodes.INTERNAL_SERVER_ERROR;
        res.locals.body = getApiErrorBody(defaultErrorCode, [], err.message);
      }

      return next();
    });
}
