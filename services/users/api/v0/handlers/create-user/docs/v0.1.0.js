/**
 * @api {post} /api/v0/users Create User
 * @apiGroup Users
 * @apiName CreateUser
 * @apiVersion 0.1.0
 * @apiDescription Allows for the creation of a new User within the system.
 *
 * @apiParam {string{..255}} username The username that will be created for the user. This must be unique, as usernames are unique within the system and what is used to authenticate against the system.
 * @apiParam {string{..5000}} emailAddress The email address associated to the User to be created.
 * @apiParam {string{..5000}} fullName The name associated to the User to be created. This is generally the full name of a person in other applications (not the username).
 *
 * @apiSuccess (201 - Created) {object} user
 * @apiSuccess (201 - Created) {number} user.userId The unique identifier that is created by the system and associated to the User's account.
 * @apiSuccess (201 - Created) {string} user.username The unique username value associated to and managed by the User.
 * @apiSuccess (201 - Created) {string{..5000}} user.emailAddress The email address associated to and managed by the User.
 * @apiSuccess (201 - Created) {string{..5000}} user.fullName The name associated to the User. This is what will be displayed in many places in the application.
 * @apiSuccess (201 - Created) {string} user.createdOn The timestamp (ISO8601-formatted string) associated to when the User was created.
 * @apiSuccess (201 - Created) {string} user.lastUpdatedOn The timestamp (ISO8601-formatted string) associated to when the User was last updated.
 *
 * @apiUse UnprocessableEntityError
 * @apiUse InternalServerError
 */
