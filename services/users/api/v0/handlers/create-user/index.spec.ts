// Import 3rd-party libraries.
import SpyInstance = jest.SpyInstance;
import { assign as _assign } from 'lodash';
import { Response } from 'express';
import { StatusCodes } from 'http-status-codes';

// Import local files.
import { default as createUser } from './';
import { appLogger } from '../../../../config/logger';
import store from '../../../../store';
import { User } from '../../../../store/types/users';
import request from '../../../../tests/mocks/request';
import response from '../../../../tests/mocks/response';
import { ModifiedRequest } from '../../../initialize-request-context';
import { default as ApiError, defaultErrorCode } from '../../exceptions/ApiError';
import DatabaseQueryError from '../../../../store/exceptions/DatabaseQueryError';
import DuplicateUsernameError from '../../../../store/exceptions/DuplicateUsernameError';
import UnprocessableEntityError from '../../exceptions/UnprocessableEntityError';
import { duplicateUsername } from '../../helpers/error-codes';
import { getValidDatabaseUser } from '../../../../tests/mocks/user';

describe('/api/v0/handlers/create-user', () => {
  test('should be a Function', () => {
    expect(createUser).toEqual(expect.any(Function));
  });

  describe('when invoked', () => {
    let appLoggerDebug: SpyInstance;
    let appLoggerInfo: SpyInstance;
    let appLoggerError: SpyInstance;
    let storeCreateUser: SpyInstance;
    let storeGetUserByUsername: SpyInstance;
    // Create the sample data used in this test.
    const fakeUserToBeCreated = {
      username: 'someUser',
      emailAddress: 'some.user@example.com',
      fullName: 'Some User'
    };
    const fakeUserFromDatabase: User.All = getValidDatabaseUser({});

    beforeEach(() => {
      appLoggerDebug = jest.spyOn(appLogger, 'debug').mockImplementation();
      appLoggerInfo = jest.spyOn(appLogger, 'info').mockImplementation();
      appLoggerError = jest.spyOn(appLogger, 'error').mockImplementation();
    });

    afterEach(() => {
      jest.clearAllMocks();
      response.locals = {};
    });

    test('should complete without any errors', () => {
      return new Promise((resolve) => {
        // Set up the spies used in this test.
        storeCreateUser = jest.spyOn(store.commands, 'createUser').mockResolvedValue();
        storeGetUserByUsername = jest.spyOn(store.queries, 'getUserByUsername').mockResolvedValue(fakeUserFromDatabase);

        // Execute the function being tested.
        createUser(<ModifiedRequest>_assign({}, request, { body: fakeUserToBeCreated }), <Response>response, (err?: Error|string) => {
          // Set the expectations.
          expect(err).toBeUndefined();
          expect(appLoggerInfo).toHaveBeenCalledTimes(1);
          expect(appLoggerInfo).toHaveBeenCalledWith('Handling request to create a User.');
          expect(appLoggerDebug).toHaveBeenCalledTimes(1);
          expect(appLoggerDebug).toHaveBeenCalledWith(`Request Body: ${JSON.stringify(fakeUserToBeCreated)}`);
          expect(storeCreateUser).toHaveBeenCalledTimes(1);
          expect(storeCreateUser).toHaveBeenCalledWith({
            username: fakeUserToBeCreated.username,
            email_address: fakeUserToBeCreated.emailAddress,
            full_name: fakeUserToBeCreated.fullName
          });
          expect(storeGetUserByUsername).toHaveBeenCalledTimes(1);
          expect(storeGetUserByUsername).toHaveBeenCalledWith(fakeUserToBeCreated.username);
          expect(response.locals?.status).toEqual(StatusCodes.CREATED);
          expect(response.locals?.body).toEqual({
            userId: fakeUserFromDatabase.user_id,
            username: fakeUserFromDatabase.username,
            emailAddress: fakeUserFromDatabase.email_address,
            fullName: fakeUserFromDatabase.full_name,
            createdOn: fakeUserFromDatabase.created_on,
            updatedOn: fakeUserFromDatabase.last_updated_on
          });

          // Ensure that the Promise is resolved if all of the expectations are successful.
          return resolve(null);
        });
      });
    });

    test.each(
      [
        [new Error('Some generic error.'), new ApiError(defaultErrorCode, [], 'Some generic error.')],
        [new DatabaseQueryError('An issue querying the database.'), new ApiError(defaultErrorCode, [], 'An issue querying the database.')],
        [new DuplicateUsernameError('The username was duplicated within the database.'), new UnprocessableEntityError(duplicateUsername, [fakeUserToBeCreated.username], 'Duplicate username.')]
      ]
    )('should handle the errors properly when they are thrown in the "store.commands.createUser()" function', (thrownError, expectedNextError) => {
      return new Promise((resolve) => {
        // Set up the spies used in this test.
        storeCreateUser = jest.spyOn(store.commands, 'createUser').mockRejectedValue(thrownError);
        storeGetUserByUsername = jest.spyOn(store.queries, 'getUserByUsername').mockResolvedValue(fakeUserFromDatabase);

        // Execute the function to be tested.
        createUser(<ModifiedRequest>_assign({}, request, { body: fakeUserToBeCreated }), <Response>response, (err?: Error|string) => {
          // Set the expectations.
          expect(appLoggerInfo).toHaveBeenCalledTimes(1);
          expect(appLoggerInfo).toHaveBeenCalledWith('Handling request to create a User.');
          expect(appLoggerDebug).toHaveBeenCalledTimes(1);
          expect(appLoggerDebug).toHaveBeenCalledWith(`Request Body: ${JSON.stringify(fakeUserToBeCreated)}`);
          expect(storeCreateUser).toHaveBeenCalledTimes(1);
          expect(storeCreateUser).toHaveBeenCalledWith({
            username: fakeUserToBeCreated.username,
            email_address: fakeUserToBeCreated.emailAddress,
            full_name: fakeUserToBeCreated.fullName
          });
          expect(storeGetUserByUsername).toHaveBeenCalledTimes(0);
          expect(response.locals?.status).toBeUndefined();
          expect(response.locals?.body).toBeUndefined();
          expect(appLoggerError).toHaveBeenCalledTimes(1);
          expect(appLoggerError).toHaveBeenCalledWith(thrownError);
          expect(err).toEqual(expectedNextError);

          // Ensure that the Promise is resolved if all of the expectations are successful.
          return resolve(null);
        });
      });
    });

    test.each(
      [
        [new Error('Some generic error.'), new ApiError(defaultErrorCode, [], 'Some generic error.')],
        [new DatabaseQueryError('An issue querying the database.'), new ApiError(defaultErrorCode, [], 'An issue querying the database.')],
        [new DuplicateUsernameError('The username was duplicated within the database.'), new UnprocessableEntityError(duplicateUsername, [fakeUserToBeCreated.username], 'Duplicate username.')]
      ]
    )('should handle the errors properly when they are thrown in the "store.commands.getUserByUsername()" function', (thrownError, expectedNextError) => {
      return new Promise((resolve) => {
        // Set up the spies used in this test.
        storeCreateUser = jest.spyOn(store.commands, 'createUser').mockResolvedValue();
        storeGetUserByUsername = jest.spyOn(store.queries, 'getUserByUsername').mockRejectedValue(thrownError);

        // Execute the function to be tested.
        createUser(<ModifiedRequest>_assign({}, request, { body: fakeUserToBeCreated }), <Response>response, (err?: Error|string) => {
          // Set the expectations.
          expect(appLoggerInfo).toHaveBeenCalledTimes(1);
          expect(appLoggerInfo).toHaveBeenCalledWith('Handling request to create a User.');
          expect(appLoggerDebug).toHaveBeenCalledTimes(1);
          expect(appLoggerDebug).toHaveBeenCalledWith(`Request Body: ${JSON.stringify(fakeUserToBeCreated)}`);
          expect(storeCreateUser).toHaveBeenCalledTimes(1);
          expect(storeCreateUser).toHaveBeenCalledWith({
            username: fakeUserToBeCreated.username,
            email_address: fakeUserToBeCreated.emailAddress,
            full_name: fakeUserToBeCreated.fullName
          });
          expect(storeGetUserByUsername).toHaveBeenCalledTimes(1);
          expect(storeGetUserByUsername).toHaveBeenCalledWith(fakeUserToBeCreated.username);
          expect(response.locals?.status).toBeUndefined();
          expect(response.locals?.body).toBeUndefined();
          expect(appLoggerError).toHaveBeenCalledTimes(1);
          expect(appLoggerError).toHaveBeenCalledWith(thrownError);
          expect(err).toEqual(expectedNextError);

          // Ensure that the Promise is resolved if all of the expectations are successful.
          return resolve(null);
        });
      });
    });
  });
});
