// Import 3rd-party libraries.
import { StatusCodes } from 'http-status-codes';
import SpyInstance = jest.SpyInstance;
import { Response } from 'express';
import { noop as _noop } from 'lodash';

// Import local files.
import { appLogger } from '../../../../config/logger';
import readinessCheck from './';
import { ModifiedRequest } from '../../../initialize-request-context';
import request from '../../../../tests/mocks/request';
import response from '../../../../tests/mocks/response';
import store from '../../../../store';
import { v4 } from 'uuid';
import { defaultErrorCode } from '../../exceptions/ApiError';

describe('/api/v0/handlers/readiness-check', () => {
  test('that it is a Function', () => {
    expect(readinessCheck).toEqual(expect.any(Function));
  });

  describe('when invoked', () => {
    let appLoggerDebug: SpyInstance;
    let appLoggerInfo: SpyInstance;
    let appLoggerError: SpyInstance;
    let storeIsDatabaseConnected: SpyInstance;

    beforeEach(() => {
      request.requestId = v4();
      appLoggerDebug = jest.spyOn(appLogger, 'debug').mockImplementation();
      appLoggerInfo = jest.spyOn(appLogger, 'info').mockImplementation();
      appLoggerError = jest.spyOn(appLogger, 'error').mockImplementation();
    });

    afterEach(() => {
      request.requestId = _noop();
      response.locals = {};
      jest.clearAllMocks();
    });

    test('when everything is successful', () => {
      // Set up any additional spies used for this test.
      storeIsDatabaseConnected = jest.spyOn(store.queries, 'isDatabaseConnected')
        .mockResolvedValue(_noop());

      return new Promise((resolve) => {
        // Execute the function to be tested.
        readinessCheck(<ModifiedRequest>request, <Response>response, (err?: Error|string) => {
          // Set the expectations.
          expect(storeIsDatabaseConnected).toHaveBeenCalledTimes(1);
          expect(err).toBeUndefined();
          expect(appLoggerInfo).toHaveBeenCalledTimes(2);
          expect(appLoggerInfo).toHaveBeenCalledWith('Readiness Check: Starting');
          expect(appLoggerInfo).toHaveBeenCalledWith('Readiness Check: Successful');
          expect(response.locals?.status).toEqual(StatusCodes.NO_CONTENT);
          expect(response.locals?.body).toBeNull();

          // Ensure that the Promise is resolved if all of the expectations are successful.
          return resolve(null);
        });
      });
    });

    test('when the database is not connected', () => {
      // Set up any additional spies used for this test.
      const storeQueryError: Error = new Error('Database not connected.');

      storeIsDatabaseConnected = jest.spyOn(store.queries, 'isDatabaseConnected')
        .mockRejectedValue(storeQueryError);

      return new Promise((resolve) => {
        // Execute the function to be tested.
        readinessCheck(<ModifiedRequest>request, <Response>response, (err?: Error|string) => {
          // Set the expectations.
          expect(storeIsDatabaseConnected).toHaveBeenCalledTimes(1);
          expect(err).toBeUndefined();
          expect(appLoggerInfo).toHaveBeenCalledTimes(2);
          expect(appLoggerInfo).toHaveBeenCalledWith('Readiness Check: Starting');
          expect(appLoggerInfo).toHaveBeenCalledWith('Readiness Check: Failure');
          expect(appLoggerDebug).toHaveBeenCalledTimes(1);
          expect(appLoggerDebug).toHaveBeenCalledWith(JSON.stringify(storeQueryError));
          expect(appLoggerError).toHaveBeenCalledTimes(1);
          expect(appLoggerError).toHaveBeenCalledWith(storeQueryError.stack);
          expect(response.locals?.status).toEqual(StatusCodes.INTERNAL_SERVER_ERROR);
          expect(response.locals?.body).toEqual({
            referenceId: request.requestId,
            code: defaultErrorCode,
            context: []
          });

          // Ensure that the Promise is resolved if all of the expectations are successful.
          return resolve(null);
        });
      });
    });
  });
});
