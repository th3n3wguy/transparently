// Import 3rd party libraries.
import { StatusCodes } from 'http-status-codes';

// Import types.
import { ModifiedRequest } from '../../../initialize-request-context';
import { NextFunction, Response } from 'express';

// Import local files.
import { appLogger } from '../../../../config/logger';
import { default as ApiError, defaultErrorCode } from '../../exceptions/ApiError';
import store from '../../../../store';
import getApiErrorBodyFromApiError from '../../helpers/get-api-error-body-from-api-error';

export default function readinessCheck(req: ModifiedRequest, res: Response, next: NextFunction): void {
  appLogger.info('Readiness Check: Starting');

  store.queries.isDatabaseConnected()
    .then(() => {
      // Log some details.
      appLogger.info('Readiness Check: Successful');

      // Set the response values.
      res.locals.status = StatusCodes.NO_CONTENT;
      res.locals.body = null;

      return next();
    })
    .catch((err) => {
      // Log some details.
      appLogger.info('Readiness Check: Failure');
      appLogger.debug(JSON.stringify(err));
      appLogger.error(err.stack);

      // Set the response values.
      res.locals.status = StatusCodes.INTERNAL_SERVER_ERROR;
      res.locals.body = getApiErrorBodyFromApiError(new ApiError(defaultErrorCode, [], err.message), req.requestId);

      return next();
    });
}
