// Import 3rd party libraries.
import { StatusCodes } from 'http-status-codes';

// Import types.
import { ModifiedRequest } from '../../../initialize-request-context';
import { NextFunction, Response } from 'express';

// Import local files.
import { appLogger } from '../../../../config/logger';

export default function livenessCheck(req: ModifiedRequest, res: Response, next: NextFunction): void {
  // Log a debug message so we know this is being handled for traceability purposes.
  appLogger.debug('Liveness Check');

  // Set the response values.
  res.locals.status = StatusCodes.NO_CONTENT;
  res.locals.body = null;

  return next();
}
