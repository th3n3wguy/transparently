// Import 3rd-party libraries.
import { StatusCodes } from 'http-status-codes';
import SpyInstance = jest.SpyInstance;
import { Response } from 'express';
import { noop as _noop } from 'lodash';

// Import local files.
import { appLogger } from '../../../../config/logger';
import livenessCheck from './';
import { ModifiedRequest } from '../../../initialize-request-context';
import request from '../../../../tests/mocks/request';
import response from '../../../../tests/mocks/response';
import { v4 } from 'uuid';

describe('api/v0/handlers/liveness-check', () => {
  test('should be a Function', () => {
    // Set the expectations.
    expect(livenessCheck).toEqual(expect.any(Function));
  });

  describe('when invoked', () => {
    let appLoggerDebug: SpyInstance;

    beforeEach(() => {
      request.requestId = v4();
      appLoggerDebug = jest.spyOn(appLogger, 'debug');
    });

    afterEach(() => {
      request.requestId = _noop();
      response.locals = {};
      jest.clearAllMocks();
    });

    test('should call the appLogger.debug() method 1 time', () => {
      return new Promise((resolve) => {
        // Execute the function that is being tested.
        livenessCheck(<ModifiedRequest>request, <Response>response, () => {
          // Set the expectations.
          expect(appLoggerDebug).toHaveBeenCalledTimes(1);
          expect(appLoggerDebug).toHaveBeenCalledWith('Liveness Check');

          // Ensure that the Promise is resolved if all of the expectations are successful.
          return resolve(null);
        });
      });
    });

    test('should set the expected response properties', () => {
      return new Promise((resolve) => {
        livenessCheck(<ModifiedRequest>request, <Response>response, () => {
          expect(response.locals?.status).toEqual(StatusCodes.NO_CONTENT);
          expect(response.locals?.body).toBeNull();

          return resolve(null);
        });
      });
    });
  });
});
