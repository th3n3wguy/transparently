// Import 3rd party libraries.
import { v4 } from 'uuid';
import { default as session, SessionOptions } from 'express-session';
import { default as sessionFileStore, FileStore } from 'session-file-store';

// Define constants used within this file.
const {
  SESSION_DIR,
  SESSION_NAME,
  SESSION_SECRET
} = process.env;
const SessionFileStore: FileStore = sessionFileStore(session);

export default <SessionOptions>{
  // https://github.com/expressjs/session#name
  name: SESSION_NAME,
  // https://github.com/expressjs/session#saveuninitialized
  saveUninitialized: false,
  // https://github.com/expressjs/session#resave
  resave: false,
  // https://github.com/expressjs/session#secret
  secret: <string>SESSION_SECRET,
  // https://github.com/expressjs/session#unset
  unset: 'destroy',
  // https://www.npmjs.com/package/express-session#cookie
  cookie: {
    path: '/',
    httpOnly: true,
    secure: false,
    maxAge: 365 * 24 * 60 * 60 * 1000
  },
  // https://www.npmjs.com/package/express-session#genid
  genid: () => v4(),
  store: new SessionFileStore({
    path: SESSION_DIR,
    secret: SESSION_SECRET
  })
};
