// Import 3rd party libraries.
import {
  includes as _includes,
  isEqual as _isEqual
} from 'lodash';
import { configure, Logger } from 'log4js';

// Set some constants needed.
const validLogLevels = ['TRACE', 'DEBUG', 'INFO', 'WARN', 'ERROR', 'FATAL', 'MARK'];
const defaultLogLevel = 'WARN';

// Import the environment variables needed.
const {
  APP_LOG_LEVEL = defaultLogLevel,
  APP_LOG_PATH = 'logs',
  NODE_ENV = 'development'
} = process.env;

const logFileName: string = _isEqual(NODE_ENV, 'test') ? 'app-test.log' : 'app.log';
const log4jsLib = configure({
  appenders: {
    localFile: {
      type: 'dateFile',
      filename: `${APP_LOG_PATH}/${logFileName}`,
      layout: {
        type: 'pattern',
        pattern: '[%d{ISO8601_WITH_TZ_OFFSET}] [%p] [%X{requestId}] (%f:%l:%o) - %m%n'
      },
      compress: true,
      daysToKeep: 7,
      keepFileExt: true
    },
    console: {
      type: 'console',
      layout: {
        type: 'pattern',
        pattern: '%[[%d{ISO8601_WITH_TZ_OFFSET}] [%p] (%f:%l:%o) - %m%n'
      }
    }
  },
  categories: {
    default: {
      appenders: _includes(['production', 'test'], NODE_ENV)
        ? ['localFile']
        : ['localFile', 'console'],
      level: _includes(validLogLevels, APP_LOG_LEVEL) ? <string>APP_LOG_LEVEL : defaultLogLevel,
      enableCallStack: true
    }
  }
});

export const log4js = log4jsLib;
export const appLogger: Logger = log4jsLib.getLogger();
