// Import 3rd party libraries.
import { get as _get } from 'lodash';
import knex from 'knex';

// Import local files.
import knexConfigs from './knexfile';

export default knex(_get(knexConfigs, <string>process.env.NODE_ENV, {}));
