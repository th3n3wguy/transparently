// Import 3rd party libraries.
import { createStream } from 'rotating-file-stream';
import { isNil as _isNil } from 'lodash';
import { format } from 'date-fns';

// Import types.
import { Options } from 'morgan';
import { Request, Response } from 'express';

// https://github.com/expressjs/morgan?_ga=1.1732017.1403991235.1454898701#predefined-formats
export const morganFormat: string = 'combined';
// https://github.com/expressjs/morgan?_ga=1.1732017.1403991235.1454898701#options
export const morganOptions: Options<Request, Response> = {
  // https://github.com/expressjs/morgan?_ga=1.1732017.1403991235.1454898701#immediate
  immediate: false,
  // https://github.com/iccicci/rotating-file-stream
  stream: createStream(
    (time?: number | Date) => _isNil(time) ? `transparently_http-requests_${format(new Date(), 'yyyy-MM-dd')}.log`
      : `transparently_http-requests_${format(time, 'yyyy-MM-dd')}.log`,
    {
      compress: true,
      interval: '1d',
      path: './logs'
    }
  )
};
