// Import 3rd-party libraries.
import { v4 } from 'uuid';

// Import types.
import { ModifiedRequest } from '../../api/initialize-request-context';
import { Session } from 'express-session';

export default <Partial<ModifiedRequest>>{
  requestId: v4(),
  body: {},
  session: <Session>{ id: v4() }
};
