export default {
  databaseTimezoneFormat: 'yyyy-MM-dd\'T\'HH:mm:ss.SSSSSSxxx'
};
