// Import local files.
import { CreateUser } from '../commands/create-user';
import { GetUserByUsername } from '../queries/get-user-by-username';
import { GetUsers } from '../queries/get-users';
import { IsDatabaseConnected } from '../queries/is-database-connected';

export namespace Store {
  /**
   * The "Commands" to allow for data to be inserted, updated, or deleted from the store.
   */
  export interface Commands {
    createUser: CreateUser;
  }

  /**
   * The "Queries" (see CQRS) that allow for data to be retrieved from the store.
   */
  export interface Queries {
    getUserByUsername: GetUserByUsername;
    getUsers: GetUsers;
    isDatabaseConnected: IsDatabaseConnected;
  }

  /**
   * The base object definition for the store that contains both the "commands" and "queries" functionality within a single object.
   */
  export interface All {
    commands: Commands;
    queries: Queries;
  }
}
