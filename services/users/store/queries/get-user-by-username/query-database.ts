// Import local files.
import { User } from '../../types/users';
import { appLogger } from '../../../config/logger';
import db from '../../../config/database';
import DatabaseQueryError from '../../exceptions/DatabaseQueryError';
import { PostgresError } from '../../types/database-errors';

export default function queryDatabase(username: User.UserManagedProperties['username']): Promise<User.All[]> {
  return new Promise((resolve, reject) => {
    db.from<User.All>('users')
      .select([
        'user_pk',
        'user_id',
        'username',
        'email_address',
        'full_name',
        'created_on',
        'last_updated_on'
      ])
      .where('username', username)
      .then((users: User.All[]) => {
        // Log the information to the application's log file for traceability.
        appLogger.info('Database Query: Success.');
        appLogger.debug(`Users: ${JSON.stringify(users)}`);

        // Return the list of Users.
        return resolve(users);
      })
      .catch((err: Error | PostgresError) => {
        appLogger.warn(err.stack);
        appLogger.debug(JSON.stringify(err));

        return reject(new DatabaseQueryError(err.message));
      });
  });
}
