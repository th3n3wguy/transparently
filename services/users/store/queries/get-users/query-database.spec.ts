// Import 3rd-party libraries.
import SpyInstance = jest.SpyInstance;

// Import local files.
import queryDatabase from './query-database';
import db from '../../../config/database';
import { User } from '../../types/users';
import { getValidDatabaseUser } from '../../../tests/mocks/user';
import { GetUsersQueryParams } from './index';
import { appLogger } from '../../../config/logger';
import DatabaseQueryError from '../../exceptions/DatabaseQueryError';

describe('/store/queries/get-users/query-database', () => {
  test('should be a Function', () => {
    expect(queryDatabase).toEqual(expect.any(Function));
  });

  describe('when invoked', () => {
    let appLoggerDebug: SpyInstance;
    let appLoggerInfo: SpyInstance;
    let appLoggerWarn: SpyInstance;
    let dbFromSpy: SpyInstance;
    let dbSelectSpy: SpyInstance;
    let dbLimitSpy: SpyInstance;
    let dbOffsetSpy: SpyInstance;

    beforeEach(() => {
      appLoggerDebug = jest.spyOn(appLogger, 'debug').mockImplementation();
      appLoggerInfo = jest.spyOn(appLogger, 'info').mockImplementation();
      appLoggerWarn = jest.spyOn(appLogger, 'warn').mockImplementation();
      dbFromSpy = jest.spyOn(db, 'from').mockReturnThis();
      dbSelectSpy = jest.spyOn(db, 'select').mockReturnThis();
      dbLimitSpy = jest.spyOn(db, 'limit').mockReturnThis();
      dbOffsetSpy = jest.spyOn(db, 'offset').mockReturnThis();
    });

    afterEach(() => {
      jest.clearAllMocks();
    });

    test('should return the same exact list of Users that is returned from the database', () => {
      // Set up the mocks, spies, and fake data used in this test.
      const dbUsers: User.All[] = [getValidDatabaseUser({}), getValidDatabaseUser({})];
      const dbOrderBySpy: SpyInstance = jest.spyOn(db, 'orderBy')
        .mockResolvedValue(dbUsers);
      const params: GetUsersQueryParams = {
        limit: 50,
        offset: 0,
        sortDirection: 'ASC',
        sortKey: 'username'
      };

      return new Promise((resolve, reject) => {
        // Execute the function to be tested.
        queryDatabase(params)
          .then((users) => {
            // Set the expectations.
            expect(dbFromSpy).toHaveBeenCalledWith('users');
            expect(dbSelectSpy).toHaveBeenCalledWith(['user_pk', 'user_id', 'username', 'email_address', 'full_name', 'created_on', 'last_updated_on']);
            expect(dbLimitSpy).toHaveBeenCalledWith(params.limit);
            expect(dbOffsetSpy).toHaveBeenCalledWith(params.offset);
            expect(dbOrderBySpy).toHaveBeenCalledWith(params.sortKey, params.sortDirection);
            expect(appLoggerInfo).toHaveBeenCalledWith('Database Query: Success.');
            expect(appLoggerDebug).toHaveBeenCalledWith(`Users: ${JSON.stringify(users)}`);
            expect(users).toEqual(dbUsers);

            // Ensure that the Promise is resolved if all of the expectations are successful.
            return resolve(null);
          })
          .catch(reject);
      });
    });

    test('should reject the Promise if there is an error', () => {
      // Set up the mocks, spies, and fake data used in this test.
      const databaseError: Error = new Error('Error when querying the database.');
      const dbOrderBySpy: SpyInstance = jest.spyOn(db, 'orderBy')
        .mockRejectedValue(databaseError);
      const params: GetUsersQueryParams = {
        limit: 50,
        offset: 0,
        sortDirection: 'ASC',
        sortKey: 'username'
      };

      return new Promise((resolve, reject) => {
        queryDatabase(params)
          .then(() => reject(new Error('This promise should not resolve.')))
          .catch((err) => {
            // Set the expectations.
            expect(dbFromSpy).toHaveBeenCalledWith('users');
            expect(dbSelectSpy).toHaveBeenCalledWith(['user_pk', 'user_id', 'username', 'email_address', 'full_name', 'created_on', 'last_updated_on']);
            expect(dbLimitSpy).toHaveBeenCalledWith(params.limit);
            expect(dbOffsetSpy).toHaveBeenCalledWith(params.offset);
            expect(dbOrderBySpy).toHaveBeenCalledWith(params.sortKey, params.sortDirection);
            expect(appLoggerDebug).toHaveBeenCalledWith(JSON.stringify(databaseError));
            expect(appLoggerWarn).toHaveBeenCalledWith(databaseError.stack);
            expect(err).toEqual(new DatabaseQueryError(databaseError.message));

            // Ensure that the Promise is resolved if all of the expectations are successful.
            return resolve(null);
          });
      });
    });
  });
});
