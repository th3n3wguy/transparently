// Import types.
import { User } from '../../types/users';
import { GetUsersQueryParams } from './';

// Import local files.
import { appLogger } from '../../../config/logger';
import db from '../../../config/database';
import DatabaseQueryError from '../../exceptions/DatabaseQueryError';

export default function queryDatabase({
  limit,
  offset,
  sortDirection,
  sortKey
}: GetUsersQueryParams): Promise<User.All[]> {
  return new Promise((resolve, reject) => {
    db.from<User.All>('users')
      .select([
        'user_pk',
        'user_id',
        'username',
        'email_address',
        'full_name',
        'created_on',
        'last_updated_on'
      ])
      .limit(limit)
      .offset(offset)
      .orderBy(sortKey, sortDirection)
      .then((users: User.All[]) => {
        // Log the information to the application's log file for traceability.
        appLogger.info('Database Query: Success.');
        appLogger.debug(`Users: ${JSON.stringify(users)}`);

        // Return the list of Users.
        return resolve(users);
      })
      .catch((err: Error) => {
        appLogger.debug(JSON.stringify(err));
        appLogger.warn(err.stack);

        return reject(new DatabaseQueryError(err.message));
      });
  });
}
