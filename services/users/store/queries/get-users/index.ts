'use strict';

// Import types.
import { User } from '../../types/users';

// Import local files.
import { appLogger } from '../../../config/logger';
import queryDatabase from './query-database';
import DatabaseQueryError from '../../exceptions/DatabaseQueryError';

export interface GetUsersQueryParams {
  limit: number;
  offset: number;
  sortDirection: 'ASC'|'asc'|'DESC'|'desc';
  sortKey: 'username'|'created_on'|'last_updated_on';
}

export interface GetUsers {
  (queryParams: GetUsersQueryParams): Promise<User.All[]>
}

export default function getUsers(queryParams: GetUsersQueryParams): Promise<User.All[]> {
  return new Promise((resolve, reject) => {
    appLogger.info('Attempting to retrieve the Users from the database.');
    appLogger.debug(JSON.stringify(queryParams));

    queryDatabase(queryParams)
      .then((users: User.All[]) => {
        appLogger.info('Successfully retrieved the Users from the database.');
        appLogger.debug(`Users: ${JSON.stringify(users)}`);

        return resolve(users);
      })
      .catch((err: Error|DatabaseQueryError) => {
        // Log the Error's stack to the application's log file.
        appLogger.warn(err.stack);

        // Return a new Error that can be handled properly at the API controller layer.
        return reject(err);
      });
  });
}
