// Import 3rd-party libraries.
import SpyInstance = jest.SpyInstance;
import { noop as _noop } from 'lodash';

// Import local files.
import isDatabaseConnected from './';
import db from '../../../config/database';
import { appLogger } from '../../../config/logger';
import DatabaseQueryError from '../../exceptions/DatabaseQueryError';

describe('/store/queries/is-database-connected', () => {
  test('should be a Function', () => {
    expect(isDatabaseConnected).toEqual(expect.any(Function));
  });

  describe('when invoked', () => {
    let appLoggerDebug: SpyInstance;
    let appLoggerInfo: SpyInstance;
    let appLoggerWarn: SpyInstance;

    beforeEach(() => {
      appLoggerDebug = jest.spyOn(appLogger, 'debug').mockImplementation();
      appLoggerInfo = jest.spyOn(appLogger, 'info').mockImplementation();
      appLoggerWarn = jest.spyOn(appLogger, 'warn').mockImplementation();
    });

    afterEach(() => {
      jest.clearAllMocks();
    });

    test('should resolve the Promise when no Error occurs when querying the database', () => {
      // Set up the spies and mocks used within this test.
      // Had to define the spy this way because the Knex.raw() method is read-only: https://github.com/facebook/jest/issues/2227
      Object.defineProperty(db, 'raw', { value: jest.fn().mockResolvedValue(_noop()) });

      return new Promise((resolve, reject) => {
        // Execute the function to be tested.
        isDatabaseConnected()
          .then(() => {
            // Set the expectations.
            expect(appLoggerInfo).toHaveBeenCalledWith('Checking if the database is connected.');
            expect(db.raw).toHaveBeenCalledWith('SELECT 1');

            // Ensure that the Promise is resolved if all of the expectations are successful.
            return resolve(null);
          })
          .catch(reject);
      });
    });

    test('should reject the Promise with a DatabaseQueryError when any errors occurs during the database query', () => {
      // Set up the spies and mocks used within this test.
      const dbError: Error = new Error('An error thrown within the database.');

      // Had to define the spy this way because the Knex.raw() method is read-only: https://github.com/facebook/jest/issues/2227
      Object.defineProperty(db, 'raw', { value: jest.fn().mockRejectedValue(dbError) });

      return new Promise((resolve, reject) => {
        // Execute the function to be tested.
        isDatabaseConnected()
          .then(() => reject(new Error('This Promise should not have resolved.')))
          .catch((err) => {
            // Set the expectations.
            expect(appLoggerInfo).toHaveBeenCalledWith('Checking if the database is connected.');
            expect(db.raw).toHaveBeenCalledWith('SELECT 1');
            expect(appLoggerWarn).toHaveBeenCalledWith(dbError.stack);
            expect(appLoggerDebug).toHaveBeenCalledWith(JSON.stringify(dbError));
            expect(err).toEqual(new DatabaseQueryError('The query to check whether the database is connected failed. The database might have disconnected.'));

            // Ensure that the Promise is resolved if all of the expectations are successful.
            return resolve(null);
          });
      });
    });
  });
});
