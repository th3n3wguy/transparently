// Import 3rd-party libraries.
import SpyInstance = jest.SpyInstance;
import {
  assign as _assign,
  pick as _pick
} from 'lodash';

// Import local files.
import createUser from './';
import { User } from '../../types/users';
import db from '../../../config/database';
import { appLogger } from '../../../config/logger';
import DuplicateUsernameError from '../../exceptions/DuplicateUsernameError';
import { constraints20201231 } from '../../migrations/20201231194537_init-database';
import { getValidDatabaseUser } from '../../../tests/mocks/user';

describe('/store/commands/create-user', () => {
  test('should be a Function', () => {
    expect(createUser).toEqual(expect.any(Function));
  });

  describe('when invoked', () => {
    const validDatabaseUser: User.All = getValidDatabaseUser({});
    const validDatabaseUserToBeCreated: User.UserManagedProperties = _pick(validDatabaseUser, ['username', 'email_address', 'full_name']);
    let appLoggerDebug: SpyInstance;
    let appLoggerInfo: SpyInstance;

    beforeEach(() => {
      appLoggerDebug = jest.spyOn(appLogger, 'debug').mockImplementation();
      appLoggerInfo = jest.spyOn(appLogger, 'info').mockImplementation();
      jest.spyOn(db, 'from').mockReturnThis();
      jest.spyOn(db, 'insert').mockReturnThis();
    });

    afterEach(() => {
      jest.clearAllMocks();
    });

    test('when it successfully creates the User in the database, it should log the User details, but NOT return them in the Promise', () => {
      // Set up the spies / mocks / variables used specifically within this test.
      jest.spyOn(db, 'returning')
        .mockResolvedValue([validDatabaseUser]);

      return new Promise((resolve, reject) => {
        // Invoke the function to be tested.
        createUser(validDatabaseUserToBeCreated)
          .then((val) => {
            // Set the expectations.
            expect(appLoggerInfo).toHaveBeenCalledWith('Attempting to create the User within the database.');
            expect(appLoggerInfo).toHaveBeenCalledWith('User created successfully within the database.');
            expect(appLoggerDebug).toHaveBeenCalledWith(`User: ${JSON.stringify(validDatabaseUser)}`);
            expect(val).toBeUndefined();

            // Ensure that the Promise is resolved if all of the expectations are successful.
            return resolve(null);
          })
          .catch(reject);
      });
    });

    test.each(
      [
        [new Error('Generic database error failure.')],
        [new DuplicateUsernameError('The username already exists within the database.')],
        [_assign(new Error('Postgres error.'), { code: '23505', constraint: constraints20201231.users.unique_username })]
      ]
    )('when an Error is thrown and the User is not created within the database, it should log the information and return the Error that was thrown', (thrownError) => {
      // Set up the spies / mocks / variables used specifically within this test.
      jest.spyOn(db, 'returning')
        .mockRejectedValue(thrownError);

      return new Promise((resolve, reject) => {
        createUser(validDatabaseUserToBeCreated)
          .then(() => reject(new Error('The promise was resolved when it should have been rejected.')))
          .catch((err) => {
            // Set the expectations.
            expect(appLoggerInfo).toHaveBeenCalledWith('Attempting to create the User within the database.');
            expect(appLoggerInfo).toHaveBeenCalledWith('Failed to create the User within the database.');
            expect(err).toEqual(thrownError);

            // Ensure that the Promise is resolved if all of the expectations are successful.
            return resolve(null);
          });
      });
    });
  });
});
