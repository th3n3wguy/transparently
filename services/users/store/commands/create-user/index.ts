'use strict';

// Import types.
import { User } from '../../types/users';
import { PostgresError } from '../../types/database-errors';

// Import local files.
import { appLogger } from '../../../config/logger';
import createUserInDatabase from './create-user-in-database';
import DuplicateUsernameError from '../../exceptions/DuplicateUsernameError';

/**
 * The type definition for the "createUser" command within the store.
 */
export interface CreateUser {
  (user: User.UserManagedProperties): Promise<void>
}

/**
 * Create a User within the store.
 *
 * @param {User.UserManagedProperties} user - The properties associated
 * @returns {Promise<User.All>} The User created within the store.
 */
export default function createUser(user: User.UserManagedProperties): Promise<void> {
  return new Promise((resolve, reject) => {
    appLogger.info('Attempting to create the User within the database.');
    appLogger.debug(`User: ${JSON.stringify(user)}`);

    createUserInDatabase(user)
      .then((user) => {
        appLogger.info('User created successfully within the database.');
        appLogger.debug(`User: ${JSON.stringify(user)}`);

        return resolve();
      })
      .catch((err: Error | PostgresError | DuplicateUsernameError) => {
        appLogger.info('Failed to create the User within the database.');

        return reject(err);
      });
  });
}
