// Import 3rd-party libraries.
import SpyInstance = jest.SpyInstance;
import {
  assign as _assign,
  pick as _pick
} from 'lodash';

// Import local files.
import createUserInDatabase from './create-user-in-database';
import db from '../../../config/database';
import { constraints20201231 } from '../../migrations/20201231194537_init-database';
import DuplicateUsernameError from '../../exceptions/DuplicateUsernameError';
import { User } from '../../types/users';
import { getValidDatabaseUser } from '../../../tests/mocks/user';

describe('/store/commands/create-user/create-user-in-database', () => {
  test('should be a Function', () => {
    expect(createUserInDatabase).toEqual(expect.any(Function));
  });

  describe('when invoked', () => {
    const columnsReturned = ['user_pk', 'user_id', 'username', 'email_address', 'full_name', 'created_on', 'last_updated_on'];
    let validDatabaseUser: User.All;
    let validDatabaseUserToBeCreated: User.UserManagedProperties;
    let expectedDatabaseUser: Pick<User.All, Exclude<keyof User.All, 'user_pk'>>;
    let dbFromSpy: SpyInstance;
    let dbInsertSpy: SpyInstance;
    let dbReturningSpy: SpyInstance;

    beforeEach(() => {
      validDatabaseUser = getValidDatabaseUser({});
      validDatabaseUserToBeCreated = _pick(validDatabaseUser, ['username', 'email_address', 'full_name']);
      expectedDatabaseUser = {
        user_id: expect.any(String),
        created_on: expect.any(Date),
        last_updated_on: expect.any(Date),
        username: validDatabaseUser.username,
        email_address: validDatabaseUser.email_address,
        full_name: validDatabaseUser.full_name
      };
      dbFromSpy = jest.spyOn(db, 'from').mockReturnThis();
      dbInsertSpy = jest.spyOn(db, 'insert').mockReturnThis();
    });

    afterEach(() => {
      jest.clearAllMocks();
    });

    test('should return a single User.Core object', () => {
      // Set up the spies / mocks / variables used specifically within this test.
      dbReturningSpy = jest.spyOn(db, 'returning')
        .mockResolvedValue([validDatabaseUser]);

      return new Promise((resolve, reject) => {
        // Invoke the function to be tested.
        createUserInDatabase(validDatabaseUserToBeCreated)
          .then((user) => {
            // Set the expectations.
            expect(dbFromSpy).toHaveBeenCalledWith('users');
            expect(dbInsertSpy).toHaveBeenCalledWith(expectedDatabaseUser);
            expect(dbReturningSpy).toHaveBeenCalledWith(columnsReturned);
            expect(user).toEqual(validDatabaseUser);

            // Ensure that the Promise is resolved if all of the expectations are successful.
            return resolve(null);
          })
          .catch(reject);
      });
    });

    test('should reject the Promise with a "DuplicateUsernameError" error when the specific PostgreSQL error is thrown', () => {
      // Set up the spies / mocks / variables used specifically within this test.
      const postgresDuplicateUsernameError: Error = _assign(new Error('Duplicate username.'), {
        code: '23505',
        constraint: constraints20201231.users.unique_username
      });

      dbReturningSpy = jest.spyOn(db, 'returning')
        .mockRejectedValue(postgresDuplicateUsernameError);

      // Invoke the function to be tested.
      return createUserInDatabase(validDatabaseUserToBeCreated)
        // This should not
        .then(() => expect(false).toBeTruthy())
        .catch((err) => {
          // Set the expectations.
          expect(dbFromSpy).toHaveBeenCalledWith('users');
          expect(dbInsertSpy).toHaveBeenCalledWith(expectedDatabaseUser);
          expect(dbReturningSpy).toHaveBeenCalledWith(columnsReturned);
          expect(err).toBeInstanceOf(DuplicateUsernameError);
          expect(err.message).toEqual(postgresDuplicateUsernameError.message);
        });
    });

    test('should reject the Promise with whatever generic Error is thrown within the function', () => {
      // Set up the spies / mocks / variables used specifically within this test.
      const genericError: Error = new Error('Generic error thrown in the function.');

      dbReturningSpy = jest.spyOn(db, 'returning')
        .mockRejectedValue(genericError);

      return new Promise((resolve, reject) => {
        createUserInDatabase(validDatabaseUserToBeCreated)
          .then(() => {
            // Make sure that we don't get this value, as we are expecting this test to throw the error.
            expect(true).toEqual(false);

            return reject(new Error('Expecting an error.'));
          })
          .catch((err) => {
            // Making sure that we are catching any errors thrown within the expectations so we can reject the Promise instead of the test timing out.
            try {
              // Set the expectations.
              expect(dbFromSpy).toHaveBeenCalledWith('users');
              expect(dbInsertSpy).toHaveBeenCalledWith(expectedDatabaseUser);
              expect(dbReturningSpy).toHaveBeenCalledWith(columnsReturned);
              expect(err).toBeInstanceOf(Error);
              expect(err.message).toEqual(genericError.message);

              // Ensure that the Promise is resolved if all of the expectations are successful.
              return resolve(null);
            }
            catch (ex) {
              // Reject the Promise in case an expectation fails.
              return reject(ex);
            }
          });
      });
    });
  });
});
