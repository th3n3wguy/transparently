// Import 3rd party libraries.
// import { v4 as uuidV4 } from 'uuid';
import { Ulid } from 'id128';
import {
  get as _get,
  isEqual as _isEqual
} from 'lodash';

// Import local files.
import { appLogger } from '../../../config/logger';
import db from '../../../config/database';
import { User } from '../../types/users';
import { PostgresError } from '../../types/database-errors';
import DuplicateUsernameError from '../../exceptions/DuplicateUsernameError';
import { constraints20201231 } from '../../migrations/20201231194537_init-database';

/**
 * Function to create a new User within the database.
 *
 * @param {User.UserManagedProperties} user - The base User object that contains the properties that will be added to the database.
 * @returns {Promise<User.All>} The full User details.
 */
export default function createUserInDatabase({
  username,
  email_address,
  full_name
}: User.UserManagedProperties): Promise<User.All> {
  return new Promise((resolve, reject) => {
    // Define the remaining constants to be used to create the User.
    const {
      created_on,
      last_updated_on,
      user_id
    } = getDefaultValues();

    appLogger.debug(`Generated User Values: "user_id": ${user_id}..."created_on": ${created_on}..."last_updated_on": ${last_updated_on}`);

    // Add the User to the database.
    db.from<User.All>('users')
      .insert({
        user_id,
        username,
        email_address,
        full_name,
        created_on,
        last_updated_on
      })
      .returning([
        'user_pk',
        'user_id',
        'username',
        'email_address',
        'full_name',
        'created_on',
        'last_updated_on'
      ])
      .then(([user]: User.All[]) => resolve(user))
      .catch((err: Error | PostgresError) => {
        appLogger.warn(err.stack);
        appLogger.debug(JSON.stringify(err));

        return isDuplicateUsernameError(err)
          ? reject(new DuplicateUsernameError(_get(err, 'detail', err.message)))
          : reject(err);
      });
  });
}

/**
 * Creates the constants that are used to create the proper data within the database when creating the User.
 *
 * @returns {{ created_on: Date, last_updated_on: Date, user_id: string }} The constants that can be deconstructed for easier access / reasoning in the above function.
 */
function getDefaultValues(): { created_on: User.All['created_on'], last_updated_on: User.All['last_updated_on'], user_id: User.All['user_id'] } {
  const now = new Date();

  return {
    created_on: now,
    last_updated_on: now,
    user_id: Ulid.generate({ time: now }).toRaw()
  };
}

/**
 * Check to determine whether the provided Error is the PostgreSQL error that is tied back to the constraint defined by name on the "users" table within the database. Should
 *   the name of the
 *
 * @param {Error | PostgresError} err The Error / PostgreSQL error that needs to be validated.
 * @returns {boolean} Returns "true" when we get the proper error code and constraint name match.
 */
function isDuplicateUsernameError(err: Error | PostgresError): boolean {
  return _isEqual(_get(err, 'code', null), '23505')
    && _isEqual(_get(err, 'constraint', null), constraints20201231.users.unique_username);
}
