/**
 * Error class that is specific for when a Database error occurs when retrieving data from the database.
 *
 * @class DatabaseQueryError
 * @extends Error
 */
export default class DatabaseQueryError extends Error {
  constructor(message: string) {
    super(message);
    Error.captureStackTrace(this, DatabaseQueryError);
  }
}
