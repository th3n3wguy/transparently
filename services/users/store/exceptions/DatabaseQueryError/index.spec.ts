// Import local files.
import DatabaseQueryError from './';

describe('/store/exceptions/DatabaseQueryError', () => {
  test('that an instance is an Error subclass', () => {
    const err: DatabaseQueryError = new DatabaseQueryError('Failure in the database.');

    expect(err).toBeInstanceOf(Error);
  });
});
