// Import local files.
import UserNotFoundError from './';

describe('/store/exceptions/DatabaseQueryError', () => {
  test('that an instance is an Error subclass', () => {
    const err: UserNotFoundError = new UserNotFoundError('Generic error message.');

    expect(err).toBeInstanceOf(Error);
  });
});
