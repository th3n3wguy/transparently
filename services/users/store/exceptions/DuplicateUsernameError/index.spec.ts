// Import local files.
import DuplicateUsernameError from './';

describe('/store/exceptions/DatabaseQueryError', () => {
  test('that an instance is an Error subclass', () => {
    const err: DuplicateUsernameError = new DuplicateUsernameError('Generic error message.');

    expect(err).toBeInstanceOf(Error);
  });
});
