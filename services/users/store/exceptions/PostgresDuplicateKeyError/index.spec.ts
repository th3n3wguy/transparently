// Import local files.
import PostgresDuplicateKeyError from './';

describe('/store/exceptions/DatabaseQueryError', () => {
  test('that an instance is an Error subclass', () => {
    const err: PostgresDuplicateKeyError = new PostgresDuplicateKeyError('Generic error message.');

    expect(err).toBeInstanceOf(Error);
  });
});
