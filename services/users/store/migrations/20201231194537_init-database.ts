// Import 3rd-party libraries.
import { Knex } from 'knex';

export const constraints20201231 = {
  users: {
    unique_username: 'users_username_unique',
    index_userId: 'idx___user_id',
    index_createdOn: 'idx___created_on',
    index_lastUpdatedOn: 'idx___last_updated_on'
  }
};

exports.up = function upgradeDatabase(knex: Knex) {
  return knex.schema
    .createTable('users', (tb) => {
      tb.uuid('user_id')
        .comment('The unique identifier for the User.');
      tb.string('username', 255)
        .unique(constraints20201231.users.unique_username)
        .notNullable()
        .comment('A unique nickname associated to the User.');
      tb.string('email_address', 255)
        .notNullable()
        .comment('The email address associated to the User.');
      tb.string('full_name', 5000)
        .nullable()
        .defaultTo('')
        .comment('The name field that is saved to the User\'s profile.');
      tb.timestamp('created_on', { useTz: true, precision: 6 })
        .notNullable()
        .comment('The date and time as to when the User was created.');
      tb.timestamp('last_updated_on', { useTz: true, precision: 6 })
        .notNullable()
        .comment('The date and time as to when the User was last updated.');
      tb.index('user_id', constraints20201231.users.index_userId);
      tb.index('created_on', constraints20201231.users.index_createdOn);
      tb.index('last_updated_on', constraints20201231.users.index_lastUpdatedOn);
    });
};

exports.down = function downgradeDatabase(knex: Knex) {
  return knex.schema.dropTable('users');
};
