// Import 3rd-party libraries.
import { Knex } from 'knex';

// Import local files.
import { constraints20201231 } from './20201231194537_init-database';

exports.up = function upgradeDatabase(knex: Knex) {
  return knex.schema.dropTableIfExists('users')
    .then(() => {
      return knex.schema.createTable('users', (tb) => {
        tb.bigIncrements('user_pk', { primaryKey: true })
          .comment('A unique identifier for the User that is used internally to do joins within this service\'s database.');
        tb.string('user_id', 32)
          .notNullable()
          .comment('The unique identifier for the User that is used for displaying an ID to other systems, specified using the ULID spec.');
        tb.string('username', 255)
          .unique(constraints20201231.users.unique_username)
          .notNullable()
          .comment('A unique nickname associated to the User.');
        tb.string('email_address', 255)
          .notNullable()
          .comment('The email address associated to the User.');
        tb.string('full_name', 5000)
          .nullable()
          .defaultTo('')
          .comment('The name field that is saved to the User\'s profile.');
        tb.timestamp('created_on', { useTz: true, precision: 6 })
          .notNullable()
          .comment('The date and time as to when the User was created.');
        tb.timestamp('last_updated_on', { useTz: true, precision: 6 })
          .notNullable()
          .comment('The date and time as to when the User was last updated.');
        tb.index('user_id', constraints20201231.users.index_userId);
        tb.index('created_on', constraints20201231.users.index_createdOn);
        tb.index('last_updated_on', constraints20201231.users.index_lastUpdatedOn);
      });
    });
};

exports.down = function downgradeDatabase(knex: Knex) {
  return knex.schema.dropTable('users')
    .then(() => {
      return knex.schema.createTable('users', (tb) => {
        tb.uuid('user_id')
          .comment('The unique identifier for the User.');
        tb.string('username', 255)
          .unique(constraints20201231.users.unique_username)
          .notNullable()
          .comment('A unique nickname associated to the User.');
        tb.string('email_address', 255)
          .notNullable()
          .comment('The email address associated to the User.');
        tb.string('full_name', 5000)
          .nullable()
          .defaultTo('')
          .comment('The name field that is saved to the User\'s profile.');
        tb.timestamp('created_on', { useTz: true, precision: 6 })
          .notNullable()
          .comment('The date and time as to when the User was created.');
        tb.timestamp('last_updated_on', { useTz: true, precision: 6 })
          .notNullable()
          .comment('The date and time as to when the User was last updated.');
        tb.index('user_id', constraints20201231.users.index_userId);
        tb.index('created_on', constraints20201231.users.index_createdOn);
        tb.index('last_updated_on', constraints20201231.users.index_lastUpdatedOn);
      });
    });
};
