// Import 3rd-party libraries.
import {
  get as _get,
  isEqual as _isEqual,
  noop as _noop
} from 'lodash';
import { config } from 'dotenv';

// Import local files.
import { appLogger } from '../config/logger';
import db from '../config/database';
import knexConfig from '../config/knexfile';

!_isEqual(process.env.NODE_ENV, 'production') ? config() : _noop();

// Log a message that the seeds have begun.
appLogger.debug('Database Seeds: Starting');

db.seed.run(_get(knexConfig, `${process.env.NODE_ENV}.seeds`, knexConfig.production))
  .then(() => appLogger.info('Database Seeds: Completed Successfully'))
  .catch((err: Error) => {
    // Log some information about the failure.
    appLogger.info('Database Seeds: Failed');
    appLogger.fatal(err.stack);
  });
