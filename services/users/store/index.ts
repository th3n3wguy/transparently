// Import types.
import { Store } from './types/store';

// Import local files.
import createUser from './commands/create-user';
import getUserByUsername from './queries/get-user-by-username';
import getUsers from './queries/get-users';
import isDatabaseConnected from './queries/is-database-connected';

export default <Store.All>{
  commands: {
    createUser
  },
  queries: {
    getUserByUsername,
    getUsers,
    isDatabaseConnected
  }
};
