// Import 3rd-party libraries.
import passport from 'passport';
import { Strategy as LocalStrategy } from 'passport-local';
import { isNil as _isNil } from 'lodash';
import * as bcrypt from 'bcrypt';

// Import local files.
import { appLogger } from './logger';
import { User } from '../store/types/store-interfaces/users';
import store from '../store';

export const authStrategyName = 'local-username';

passport.serializeUser<User.ServerManagedProperties['user_pk']>(function serializeUser(user, done) {
  process.nextTick(function() {
    appLogger.debug(`Passport - Serializing: user=${JSON.stringify(user)}`);

    // @ts-expect-error
    return done(null, user.id);
  });
});

passport.deserializeUser(function deserializeUser(userPk: User.Auth['user_pk'], done) {
  process.nextTick(function() {
    appLogger.debug(`Passport - Deserializing: user_pk=${userPk}`);

    store.queries.getUserByPk(userPk)
      .then((user) => {
        return done(null, user);
      })
      .catch((err) => {
        return done(err, null);
      });
  });
});

passport.use(authStrategyName, new LocalStrategy(
  {
    usernameField: 'username',
    passwordField: 'password',
    session: true,
    passReqToCallback: true
  },
  function authVerificationHandler(req, username, password, next) {
    appLogger.debug(`Passport - Auth Verification Handler: username=${username};password=${password};isAuthenticated()=${req.isAuthenticated()}`);

    store.queries.getUserForAuthCheck(username)
      .then((user) => {
        if (_isNil(user)) {
          appLogger.info('Someone attempted to log in with a username that does not exist within the system.');

          // TODO: Need to create an exception to handle this scenario.
          return Promise.reject(new Error('Invalid username or password.'));
        }
        else {
          return bcrypt.compare(password, user.password_hash)
            .then((arePasswordsEqual: boolean) => {
              if (arePasswordsEqual) {
                return next(null, { id: user.user_pk });
              }
              else {
                appLogger.warn('Someone attempted to log in with an invalid password.');

                // TODO: Need to create an exception to handle this scenario.
                return Promise.reject(new Error('Invalid username or password.'));
              }
            });
        }
      })
      .catch((err) => next(err, null));
  }
));

export default passport;
