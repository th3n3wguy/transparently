// Import 3rd-party libraries.
import axios from 'axios';

const {
  UNLEASH_URL: unleashUrl = ''
} = process.env;

export default axios.create({
  baseURL: `${unleashUrl}`,
  responseType: 'json',
  timeout: 5000
});
