// Import 3rd-party libraries.
import axios from 'axios';
import { isEqual as _isEqual } from 'lodash';

const {
  SERVICE_USERS_SSL_ON: isSslEnabled = 'false',
  SERVICE_USERS_DOMAIN: domain = '',
  SERVICE_USERS_PORT: port = ''
} = process.env;

export default axios.create({
  baseURL: `${_isEqual(isSslEnabled, 'true') ? 'https' : 'http'}://${domain}:${port}/api`,
  responseType: 'json',
  timeout: 5000
});
