// Import 3rd party libraries.
import {
  isEqual as _isEqual,
  toFinite as _toFinite
} from 'lodash';

// Import local files.
import { appLogger } from './logger';
import { Knex } from 'knex';

export default {
  development: generateConfigFromEnvironmentVariables(),
  test: generateConfigFromEnvironmentVariables(),
  production: generateConfigFromEnvironmentVariables()
};

function generateConfigFromEnvironmentVariables(): Knex.Config {
  // Get the environment variables we need.
  const {
    DB_DEBUG,
    DB_HOST,
    DB_MIGRATIONS_DIR,
    DB_NAME,
    DB_PASSWORD,
    DB_POOL_MIN,
    DB_POOL_MAX,
    DB_PORT,
    DB_SEEDS_DIR,
    DB_USER
  } = process.env;

  // Return the connection configuration from the environment variables.
  return {
    debug: _isEqual(DB_DEBUG, 'true'),
    log: {
      warn(message: string) {
        appLogger.warn(`Database Log: ${JSON.stringify(message)}`);
      },
      error(message: string) {
        appLogger.error(`Database Log: ${JSON.stringify(message)}`);
      },
      deprecate(message: string) {
        appLogger.warn(`Database Log: ${JSON.stringify(message)}`);
      },
      debug(message: string) {
        appLogger.debug(`Database Log: ${JSON.stringify(message)}`);
      }
    },
    useNullAsDefault: true,
    client: 'pg',
    connection: `postgres://${DB_USER}:${DB_PASSWORD}@${DB_HOST}:${DB_PORT}/${DB_NAME}`,
    pool: {
      min: _toFinite(DB_POOL_MIN),
      max: _toFinite(DB_POOL_MAX)
    },
    migrations: {
      directory: DB_MIGRATIONS_DIR,
      tableName: 'knex_migrations'
    },
    seeds: { directory: DB_SEEDS_DIR }
  };
}
