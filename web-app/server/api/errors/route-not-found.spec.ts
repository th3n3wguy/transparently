// Import 3rd-party libraries.
import SpyInstance = jest.SpyInstance;
import { v4 as uuidV4 } from 'uuid';
import {
  assign as _assign,
  noop as _noop
} from 'lodash';
import { Response } from 'express';

// Import local files.
import request from '../../tests/mocks/request';
import response from '../../tests/mocks/response';
import { appLogger } from '../../config/logger';
import { default as routeNotFoundHandler } from './route-not-found';
import { ModifiedRequest } from '../initialize-request-context';
import RouteNotFoundError from '../v0/exceptions/RouteNotFoundError';

describe('api/errors/route-not-found', () => {
  test('should be a Function', () => {
    expect(routeNotFoundHandler).toEqual(expect.any(Function));
  });

  describe('when invoked', () => {
    const requestUrl: string = '/api/v0/test';
    let appLoggerDebug: SpyInstance;

    beforeEach(() => {
      request.requestId = uuidV4();
      request.url = requestUrl;
      appLoggerDebug = jest.spyOn(appLogger, 'debug').mockImplementation();
    });

    afterEach(() => {
      jest.clearAllMocks();
    });

    test('should not throw an error', () => {
      return new Promise((resolve) => {
        routeNotFoundHandler(<ModifiedRequest>request, <Response>response, () => {
          expect(true).toBe(true);

          return resolve(null);
        });
      });
    });

    test('should always log a debug message', () => {
      return new Promise((resolve) => {
        // Set a custom "response" object that is used only within this function.
        const customResponse: Response = <Response>_assign({}, response, { headersSent: true });

        routeNotFoundHandler(<ModifiedRequest>request, <Response>customResponse, () => {
          expect(appLoggerDebug).toHaveBeenCalledTimes(1);
          expect(appLoggerDebug).toHaveBeenCalledWith(`Headers Sent: ${customResponse.headersSent}`);

          return resolve(null);
        });
      });
    });

    test.each(
      [
        [_noop(), new RouteNotFoundError(requestUrl, `The requested resource (${requestUrl}) was not found.`)],
        [null, new RouteNotFoundError(requestUrl, `The requested resource (${requestUrl}) was not found.`)],
        [false, new RouteNotFoundError(requestUrl, `The requested resource (${requestUrl}) was not found.`)],
        [true, _noop()]
      ]
    )('should only return an Error when the headers are not already sent', (headersSent, expectedError) => {
      return new Promise((resolve) => {
        routeNotFoundHandler(<ModifiedRequest>request, <Response>_assign({}, response, { headersSent }), (err?: Error|string) => {
          expect(err).toEqual(expectedError);

          return resolve(null);
        });
      });
    });
  });
});
