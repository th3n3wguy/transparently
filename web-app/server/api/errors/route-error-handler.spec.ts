// Import 3rd-party libraries.
import SpyInstance = jest.SpyInstance;
import { v4 as uuidV4 } from 'uuid';
import { StatusCodes } from 'http-status-codes';
import { Response } from 'express';

// Import local files.
import { default as routeErrorHandler } from './route-error-handler';
import { appLogger } from '../../config/logger';
import request from '../../tests/mocks/request';
import response from '../../tests/mocks/response';
import { ModifiedRequest } from '../initialize-request-context';
import { default as RouteNotFoundError, routeNotFoundErrorCode } from '../v0/exceptions/RouteNotFoundError';
import { default as ApiError } from '../v0/exceptions/ApiError';

describe('api/errors/route-error-handler', () => {
  test('should be a Function', () => {
    expect(routeErrorHandler).toEqual(expect.any(Function));
  });

  describe('when invoked', () => {
    let appLoggerDebug: SpyInstance;
    let appLoggerError: SpyInstance;
    let appLoggerInfo: SpyInstance;
    let responseJson: SpyInstance;
    let responseStatus: SpyInstance;

    beforeEach(() => {
      request.requestId = uuidV4();
      appLoggerDebug = jest.spyOn(appLogger, 'debug').mockImplementation();
      appLoggerInfo = jest.spyOn(appLogger, 'info').mockImplementation();
      appLoggerError = jest.spyOn(appLogger, 'error').mockImplementation();
      responseJson = jest.spyOn(response, 'json');
      responseStatus = jest.spyOn(response, 'status');
    });

    afterEach(() => {
      jest.clearAllMocks();
    });

    test('should invoke without throwing an error', () => {
      return new Promise((resolve) => {
        routeErrorHandler(new Error(''), <ModifiedRequest>request, <Response>response, () => {
          expect(true).toBe(true);

          return resolve(null);
        });
      });
    });

    test('should log the appropriate details', () => {
      return new Promise((resolve) => {
        routeErrorHandler(new Error(''), <ModifiedRequest>request, <Response>response, () => {
          expect(appLoggerDebug).toHaveBeenCalledTimes(1);
          expect(appLoggerError).toHaveBeenCalledTimes(1);
          expect(appLoggerInfo).toHaveBeenCalledTimes(1);

          return resolve(null);
        });
      });
    });

    test.each(
      [
        // Checking to make sure that the 404 errors are returned properly.
        [new RouteNotFoundError('/api/bad/url', 'The provided route was not found.'), StatusCodes.NOT_FOUND, routeNotFoundErrorCode, ['/api/bad/url']],
        // Checking to make sure that the ApiError exceptions will default to an Internal Server Error.
        [new ApiError('SOME_UNKNOWN_CODE', ['test', '123'], 'Something failed.'), StatusCodes.INTERNAL_SERVER_ERROR, 'E_GENERAL_SERVER_ERROR', ['test', '123']],
        // Checking to make sure that generic errors are handled as Internal Server Errors.
        [new Error('An unknown error'), StatusCodes.INTERNAL_SERVER_ERROR, 'E_GENERAL_SERVER_ERROR', []]
      ]
    )('should set the response status and json body accordingly', (err, statusCode, errCode, errContext) => {
      return new Promise((resolve) => {
        routeErrorHandler(err, <ModifiedRequest>request, <Response>response, () => {
          expect(responseStatus).toHaveBeenCalledWith(statusCode);
          expect(responseJson).toHaveBeenCalledWith(expect.objectContaining({
            code: errCode,
            context: errContext,
            referenceId: expect.any(String)
          }));

          return resolve(null);
        });
      });
    });
  });
});
