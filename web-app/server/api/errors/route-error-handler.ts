// Import 3rd party libraries.
import { StatusCodes } from 'http-status-codes';
import { get as _get } from 'lodash';
import { NextFunction, Response } from 'express';

// Import local files.
import { appLogger } from '../../config/logger';
import RouteNotFoundError from '../v0/exceptions/RouteNotFoundError';
import { ModifiedRequest } from '../initialize-request-context';

export default function routeErrorHandler(err: Error | RouteNotFoundError, req: ModifiedRequest, res: Response, next: NextFunction): void {
  // Log information about the error that is about to be handled.
  appLogger.info('Error Handler: Starting...');
  appLogger.debug(JSON.stringify(err));
  appLogger.error(err.stack);

  if (err instanceof RouteNotFoundError) {
    res.status(StatusCodes.NOT_FOUND)
      .json({
        code: err.code,
        context: err.context,
        referenceId: req.requestId
      });
  }
  else {
    res.status(StatusCodes.INTERNAL_SERVER_ERROR)
      .json({
        code: 'E_GENERAL_SERVER_ERROR',
        context: _get(err, 'context', []),
        referenceId: req.requestId
      });
  }

  return next();
}
