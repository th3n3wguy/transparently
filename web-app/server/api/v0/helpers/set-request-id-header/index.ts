// Import 3rd-party libraries.
import {
  NextFunction,
  Response
} from 'express';

// Import local files.
import { ModifiedRequest } from '../../../initialize-request-context';

export default function setRequestIdHeader(req: ModifiedRequest, res: Response, next: NextFunction): void {
  res.set('Transparently-Request-Id', req.requestId);

  return next();
}
