

describe('/api/v0/helpers/general-query-params-validators', () => {
  describe('isLimitValid()', () => {
    // This will need to be a .each() with a list of valid values.
    test.todo('the limit provided is valid');

    // This will need to be a .each() with a list of invalid values.
    test.todo('the limit provided is not valid');
  });

  describe('isOffsetValid()', () => {
    test.todo('the offset provided is valid');

    test.todo('the offset provided is not valid');
  });

  describe('isSortKeyValid()', () => {
    test.todo('the sortKey provided is valid');

    test.todo('the sortKey provided is not valid');
  });

  describe('isSortDirectionValid()', () => {
    test.todo('the sortDirection provided is valid');

    test.todo('the sortDirection provided is not valid');
  });
});
