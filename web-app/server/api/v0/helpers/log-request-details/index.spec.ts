describe('/api/v0/handlers/log-request-details', () => {
  test.todo('ensure the HTTP method and URI is logged');

  test.todo('ensure the request body is logged if and only if the HTTP method is POST, PUT, or PATCH');

  test.todo('ensure the query parameters are logged if and only if the HTTP method is GET');

  test.todo('make sure that no exceptions are thrown if non-JSON data is sent via the path parameters');

  test.todo('make sure that no exceptions are thrown if non-JSON data is sent via the request body');

  test.todo('make sure that no exceptions are thrown if non-JSON data is sent vai the query parameters');
});
