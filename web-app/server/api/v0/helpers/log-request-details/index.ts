// Import 3rd-party libraries.
import {
  NextFunction,
  Response
} from 'express';
import { includes as _includes } from 'lodash';

// Import local files.
import { ModifiedRequest } from '../../../initialize-request-context';
import { appLogger } from '../../../../config/logger';

export default function logRequestDetails(req: ModifiedRequest, res: Response, next: NextFunction): void {
  appLogger.info(`Method=${req.method},Path=${req.path}`);
  appLogger.info(`PathParams=${JSON.stringify(req.params)}`);

  if (_includes(['POST', 'PUT', 'PATCH'], req.method)) {
    appLogger.debug(`RequestBody=${JSON.stringify(req.body)}`);
  }
  else if (_includes(['GET'], req.method)) {
    appLogger.debug('QueryParams=', JSON.stringify(req.query));
  }

  return next();
}
