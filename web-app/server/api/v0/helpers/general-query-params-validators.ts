// Import 3rd-party libraries.
import { ParsedQs } from 'qs';
import {
  gte as _gte,
  includes as _includes,
  isEqual as _isEqual,
  lte as _lte,
  round as _round,
  toFinite as _toFinite
} from 'lodash';

/**
 * Determines whether the provided "limit" value is valid or not.
 *
 * @param {string} limit The value that is being verified by this function.
 * @param {number} definedLimit The maximum value allowed for the limit, as defined by the caller.
 * @returns {boolean} Returns true if the value provided is valid.
 */
export function isLimitValid(limit?: ParsedQs['string'], definedLimit: number = 100): boolean {
  // We assume that there is no reason that a request would provide a value of 0 for the limit and that we do not allow for a limit of greater than 100.
  return !_isEqual(limit, '0')
    && isWholeNumber(<string>limit)
    && _lte(_toFinite(limit), definedLimit)
    && _gte(_toFinite(limit), 0);
}

/**
 * Determines whether the provided "offset" value is valid or not.
 *
 * @param {string} offset The value that is being verified by this function.
 * @returns {boolean} Returns true if the value provided is valid.
 */
export function isOffsetValid(offset: ParsedQs['string']): boolean {
  return _isEqual(offset, '0')
    || (
      !_isEqual(offset, '0')
      && isWholeNumber(<string>offset)
      && !isInfinity(<string>offset)
      && _gte(_toFinite(offset), 0)
    );
}

/**
 * Determines whether the provided "sortKey" is valid based upon the provided "definedSortKeys" list.
 *
 * @param {string} sortKey The value that is being verified by this function.
 * @param {string[]} definedSortKeys The array of values that can be supplied in order to provide a set of valid values.
 * @returns {boolean} Returns true if the value provided is valid.
 */
export function isSortKeyValid(sortKey: ParsedQs['string'], definedSortKeys: string[] = []): boolean {
  return _includes(definedSortKeys, sortKey);
}

/**
 * Determines whether the provided "sortDirection" value is valid or not.
 *
 * @param {string} sortDirection The value that is being verified by this function.
 * @returns {boolean} Returns true if the value provided is valid.
 */
export function isSortDirectionValid(sortDirection: ParsedQs['string']): boolean {
  return _includes(['ASC', 'DESC', 'asc', 'desc'], sortDirection);
}

/**
 * Determines whether the provided value is a whole number (or can be cast/interpreted as a whole number).
 *
 * @param {string|number} value The value that is being tested as to whether it is a whole number or not.
 * @returns {boolean} Returns true if the value provided is an actual whole number.
 */
function isWholeNumber(value: string|number) {
  return _isEqual(_round(_toFinite(value)), _toFinite(value));
}

/**
 * Determines whether the provided value is "infinity" (in any form).
 *
 * @param {string|number} value The value that is being tested as to whether it is "infinity" or not.
 * @returns {boolean} Returns true if the value provided is an "infinite" value.
 */
function isInfinity(value: string|number) {
  return _includes([Infinity, -Infinity, 'Infinity', '-Infinity'], value);
}
