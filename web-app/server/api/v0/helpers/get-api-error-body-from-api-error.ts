// Import local files.
import ApiError from '../exceptions/ApiError';

export interface ApiErrorHttpResponseBody<T> {
  code: string;
  context: T[];
}

/**
 * @typedef {Function} GetApiErrorBody
 * @param {ApiError} err - The Error object that is set for the request.
 * @returns {ApiErrorHttpResponseBody} - The JSON representation of the response body that will be returned.
 */
export default function getApiErrorBodyFromApiError<T>(err: ApiError<T>): ApiErrorHttpResponseBody<T> {
  return {
    code: err.code,
    context: err.context
  };
}

export function getApiErrorBody<T>(code: string, context: T[]): ApiErrorHttpResponseBody<T> {
  return {
    code,
    context
  };
}
