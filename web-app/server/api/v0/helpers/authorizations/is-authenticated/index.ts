// Import 3rd-party libraries.
import {
  NextFunction,
  Response
} from 'express';

// Import local files.
import { ModifiedRequest } from '../../../../initialize-request-context';
import { E_NOT_AUTHENTICATED } from '../../error-codes';
import { appLogger } from '../../../../../config/logger';
import NotAuthenticatedError from '../../../exceptions/NotAuthenticatedError';

export default function createSession(req: ModifiedRequest, res: Response, next: NextFunction): void {
  appLogger.info('Attempting to create the session.');
  appLogger.debug(`req.isAuthenticated(): ${req.isAuthenticated()}`);

  return req.isAuthenticated()
    ? next()
    : next(new NotAuthenticatedError(E_NOT_AUTHENTICATED, [], 'The user\'s session is not authenticated.'));
}
