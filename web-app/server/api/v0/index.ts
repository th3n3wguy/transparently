// Import 3rd party libraries.
import { RequestHandler, Router as expressRouter } from 'express';

// Import route handlers.
import createSession from './handlers/create-session';
import createUser from './handlers/create-user';
import createWorkItem from './handlers/create-work-item';
import deleteUser from './handlers/delete-user';
import getUsers from './handlers/get-users';
import getWorkItems from './handlers/get-work-items';
import getWorkItemTypes from './handlers/get-work-item-types';
import livenessCheck from './handlers/liveness-check';
import logRequestDetails from './helpers/log-request-details';
import readinessCheck from './handlers/readiness-check';
import setRequestIdHeader from './helpers/set-request-id-header';

// Import helpers.
import { default as passport, authStrategyName } from '../../config/passport';
import { isAuthenticated } from './helpers/authorizations';

// Define the router that will be used to set up these sub-routes.
const router: expressRouter = expressRouter({ mergeParams: true });

router.use(<RequestHandler>setRequestIdHeader);
router.use(<RequestHandler>logRequestDetails);

router.route('/health/liveness')
  .get(<RequestHandler>livenessCheck);

router.route('/health/readiness')
  .get(<RequestHandler>readinessCheck);

// TODO: Implement these routes.
router.route('/auth/session')
//   .get() // Verify session.
  .post(passport.authenticate(authStrategyName), <RequestHandler>createSession) // Perform the authentication using username / password to establish a session (i.e. log in).
//   .delete() // Delete session (i.e. log out).

// router.route('/auth/new')
//   .post() // Allow for a new authentication to be created with username / password / etc (i.e. signup).

router.route('/users')
  .get(<RequestHandler>isAuthenticated, <RequestHandler>getUsers)
  .post(<RequestHandler>createUser);

router.route('/users/:userId')
  .delete(<RequestHandler>deleteUser);

router.route('/work-items')
  .get(<RequestHandler>getWorkItems)
  .post(<RequestHandler>createWorkItem);

router.route('/reference-data/work-item-types')
  .get(<RequestHandler>getWorkItemTypes);

// Export the router.
export default router;
