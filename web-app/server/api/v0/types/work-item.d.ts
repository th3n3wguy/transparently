// Import local files.
import { WorkItemType } from './work-item-type';
import { WorkItem as WorkItemFromStore } from '../../../store/types/store-interfaces/work-item';

/**
 * The properties that are associated to the Work Item on the store, but are managed by the server.
 */
export interface ServerManagedProperties {
  // This value is stored as a string in the database, but can be easily converted to a ULID using the `id128` library.
  workItemId: WorkItemFromStore['work_item_id'];
  // The timestamp (ISO8601-formatted string) associated to when the Work Item was created.
  createdOn: string;
  // The timestamp (ISO8601-formatted string) associated to when the Work Item was last updated.
  lastUpdatedOn: string;
}

/**
 * The properties that are associated to the Work Item on the store and are NOT managed by the server.
 */
export interface UserManagedProperties {
  name: WorkItemFromStore['name'];
  description: WorkItemFromStore['description'];
  typeSlug: WorkItemType['slug'];
}

export type WorkItem = ServerManagedProperties & UserManagedProperties;
