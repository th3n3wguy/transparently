import { ParsedQs } from 'qs';

export type QueryString = string | string[] | ParsedQs | ParsedQs[] | undefined;
