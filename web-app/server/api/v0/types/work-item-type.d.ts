// Import local files.
import { WorkItemType as StoreWorkItemType } from '../../../store/types/postgres/work-item-type';

export interface ServerManagedProperties {
  createdOn: StoreWorkItemType['created_on'];
  lastUpdatedOn: StoreWorkItemType['last_updated_on'];
}

export interface UserManagedProperties {
  slug: StoreWorkItemType['slug'];
  name: StoreWorkItemType['name'];
  description: StoreWorkItemType['description'];
  parentSlug: StoreWorkItemType['parent_slug'];
}

export type WorkItemType = ServerManagedProperties & UserManagedProperties;
