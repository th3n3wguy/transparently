// Import local files.
import ApiError from '../ApiError';

export const routeNotFoundErrorCode = 'E_ROUTE_NOT_FOUND';

/**
 * @class RouteNotFoundError
 * @extends ApiError
 */
export default class RouteNotFoundError extends ApiError<string> {
  constructor(url: string, logMessage?: string|void) {
    super(routeNotFoundErrorCode, [url], logMessage);
    this.name = this.constructor.name;
    Error.captureStackTrace(this, RouteNotFoundError);
  }
}
