// Import 3rd-party libraries.
import { noop as _noop } from 'lodash';

// Import local files.
import {
  default as RouteNotFoundError,
  routeNotFoundErrorCode
} from './';

describe('api/v0/exceptions/RouteNotFoundError', () => {
  test('should be a Function', () => {
    expect(RouteNotFoundError).toEqual(expect.any(Function));
  });

  describe('when instantiated', () => {
    let errorInstance: RouteNotFoundError;

    test.each(
      [
        ['/api/test', 'Some log message.', 'Some log message.'],
        ['/api/test', _noop(), '']
      ]
    )('should set the values provided on the expected properties', (url, providedLogMessage, expectedLogMessage) => {
      errorInstance = new RouteNotFoundError(url, providedLogMessage);

      expect(errorInstance.code).toEqual(routeNotFoundErrorCode);
      expect(errorInstance.context).toEqual([url]);
      expect(errorInstance.message).toEqual(expectedLogMessage);
    });
  });
});
