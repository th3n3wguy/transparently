// Import local files.
import { default as BadRequestError } from './';

describe('api/v0/exceptions/BadRequestError', () => {
  test('should be a Function', () => {
    expect(BadRequestError).toEqual(expect.any(Function));
  });

  describe('when instantiated', () => {
    test.each(
      [
        ['E_BAD_ERROR', ['test1', 'test2'], 'Some log message.'],
        ['E_UNKNOWN_ERROR', [123, 456], 'Another log message.']
      ]
    )('should set the values provided on the expected properties', (errorCode: string, context: (string|number)[], logMessage: string) => {
      const errorInstance = new BadRequestError(errorCode, context, logMessage);

      expect(errorInstance.code).toEqual(errorCode);
      expect(errorInstance.context).toEqual(context);
      expect(errorInstance.message).toEqual(logMessage);
    });
  });
});
