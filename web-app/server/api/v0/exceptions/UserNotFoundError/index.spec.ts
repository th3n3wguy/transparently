// Import local files.
import {
  default as UserNotFoundError,
  userNotFoundErrorCode
} from './';

describe('api/v0/exceptions/UserNotFoundError', () => {
  test('should be a Function', () => {
    expect(UserNotFoundError).toEqual(expect.any(Function));
  });

  describe('when instantiated', () => {
    let errorInstance: UserNotFoundError;

    test.each(
      [
        ['12345', 'Some log message.'],
        ['test-id', 'Another log message.']
      ]
    )('should set the values provided on the expected properties', (userId, logMessage) => {
      errorInstance = new UserNotFoundError(userId, logMessage);

      expect(errorInstance.code).toEqual(userNotFoundErrorCode);
      expect(errorInstance.context).toEqual([userId]);
      expect(errorInstance.message).toEqual(logMessage);
    });
  });
});
