// Import local files.
import ApiError from './';
import { E_GENERAL_SERVER_ERROR } from '../../helpers/error-codes';

describe('api/v0/exceptions/ApiError', () => {
  test('should be a Function', () => {
    expect(ApiError).toEqual(expect.any(Function));
  });

  describe('when instantiated', () => {
    test.each(
      [
        ['SOME_ERROR', 'SOME_ERROR', ['test1', 'test2'], ['test1', 'test2'], 'Here is a sample log message.', 'Here is a sample log message.'],
        ['SOME_ERROR', 'SOME_ERROR', ['test1', 'test2'], ['test1', 'test2'], undefined, ''],
        ['SOME_ERROR', 'SOME_ERROR', [1234, 123], [1234, 123], undefined, ''],
        ['SOME_ERROR', 'SOME_ERROR', [undefined, undefined], [undefined, undefined], undefined, ''],
        ['SOME_ERROR', 'SOME_ERROR', [null, 123], [null, 123], undefined, ''],
        ['SOME_ERROR', 'SOME_ERROR', [], [], undefined, ''],
        [undefined, E_GENERAL_SERVER_ERROR, [], [], undefined, '']
      ]
    )('should set the values provided on the expected properties', (providedErrorCode: string|undefined, expectedErrorCode: string, providedContext: (string|number|null|undefined)[], expectedContext: (string|number|null|undefined)[], providedLogMessage: string|undefined, expectedLogMessage: string|undefined) => {
      const errorInstance = new ApiError(providedErrorCode, providedContext, providedLogMessage);

      expect(errorInstance.code).toEqual(expectedErrorCode);
      expect(errorInstance.context).toEqual(expectedContext);
      expect(errorInstance.message).toEqual(expectedLogMessage);
    });
  });
});
