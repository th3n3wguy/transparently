// Import local files.
import { E_GENERAL_SERVER_ERROR } from '../../helpers/error-codes';

/**
 * @class ApiError
 * @extends Error
 */
export default class ApiError<T> extends Error {
  code: string;
  context: T[];

  constructor(code?: string|void, context?: T[]|void, logMessage?: string|void) {
    super(logMessage || '');
    this.name = this.constructor.name;
    this.code = code || E_GENERAL_SERVER_ERROR;
    this.context = context || [];
    Error.captureStackTrace(this, this.constructor);
  }
}
