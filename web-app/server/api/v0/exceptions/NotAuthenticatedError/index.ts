// Import local files.
import ApiError from '../ApiError';

/**
 * @class NotAuthenticatedError
 * @extends ApiError
 */
export default class NotAuthenticatedError<T> extends ApiError<T> {
  constructor(code: string, context: T[], logMessage: string) {
    super(code, context, logMessage);
    this.name = this.constructor.name;
    Error.captureStackTrace(this, this.constructor);
  }
}
