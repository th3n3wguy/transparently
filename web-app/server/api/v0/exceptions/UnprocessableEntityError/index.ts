// Import local files.
import ApiError from '../ApiError';

/**
 * @class UnprocessableEntityError
 * @extends ApiError
 */
export default class UnprocessableEntityError<T> extends ApiError<T> {
  constructor(code: string, context: T[], logMessage: string) {
    super(code, context, logMessage);
    this.name = this.constructor.name;
    Error.captureStackTrace(this, this.constructor);
  }
}
