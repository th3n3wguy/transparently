/**
 * @apiDefine BadRequest
 *
 * @apiError (400 - Bad Request) {Object} ApiError
 * @apiError (400 - Bad Request) {String} ApiError.code A code that precisely describes the error which occurred on the server.
 * @apiError (400 - Bad Request) {(String|Number|Object|null|undefined)[]} ApiError.context An array of values that contain additional context for why the error was returned.
 * @apiError (400 - Bad Request) {String} ApiError.referenceId A UUID that allows for support to render assistance.
 */

/**
 * @apiDefine InternalServerError
 *
 * @apiError (500 - Internal Server Error) {Object} ApiError
 * @apiError (500 - Internal Server Error) {String} ApiError.code A code that precisely describes the error which occurred on the server.
 * @apiError (500 - Internal Server Error) {(String|Number|Object|null|undefined)[]} ApiError.context An array of values that contain additional context for why the error was returned.
 * @apiError (500 - Internal Server Error) {String} ApiError.referenceId A UUID that allows for support to render assistance.
 */

/**
 * @apiDefine NotFound
 *
 * @apiError (404 - Not Found) {Object} ApiError
 * @apiError (404 - Not Found) {String} ApiError.code A code that precisely describes the error which occurred on the server.
 * @apiError (404 - Not Found) {(String|Number|Object|null|undefined)[]} ApiError.context An array of values that contain additional context for why the error was returned.
 */

/**
 * @apiDefine UnprocessableEntityError
 *
 * @apiError (422 - Unprocessable Entity) {Object} ApiError
 * @apiError (422 - Unprocessable Entity) {String} error.code A code that precisely describes the error which occurred on the server.
 * @apiError (422 - Unprocessable Entity) {(String|Number|Object|null|undefined)[]} error.context An array of values that contain additional context for why the error was returned.
 * @apiError (422 - Unprocessable Entity) {String} error.referenceId A UUID that allows for support to render assistance.
 */
