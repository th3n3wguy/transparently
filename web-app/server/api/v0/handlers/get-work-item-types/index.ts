import { ModifiedRequest } from '../../../initialize-request-context';
import {
  NextFunction,
  Response
} from 'express';
import { StatusCodes } from 'http-status-codes';

// Import local files.
import store from '../../../../store';
import validateRequestParams from './validate-request-params';
import { appLogger } from '../../../../config/logger';
import BadRequestError from '../../exceptions/BadRequestError';
import UnprocessableEntityError from '../../exceptions/UnprocessableEntityError';
import DatabaseQueryError from '../../../../store/exceptions/DatabaseQueryError';
import getApiErrorBodyFromApiError from '../../helpers/get-api-error-body-from-api-error';
import castQueryParams from './cast-query-params';
import mapStoreToResponse from './map-store-to-response';
import { QueryString } from '../../types/query-string';

export default function getWorkItemTypes(req: ModifiedRequest, res: Response, next: NextFunction): void {
  validateRequestParams(req.query)
    .then(castQueryParams)
    .then(store.queries.getWorkItemTypes)
    .then(mapStoreToResponse)
    .then((workItemTypes) => {
      appLogger.info('Successfully retrieved the Work Item Types.');
      appLogger.debug(workItemTypes);

      res.locals.status = StatusCodes.OK;
      res.locals.body = workItemTypes;

      return next();
    })
    .catch((err: Error|BadRequestError<QueryString>|UnprocessableEntityError<QueryString>|DatabaseQueryError) => {
      appLogger.info('Failure when attempting to retrieve the Work Item Types.');
      appLogger.error(err.stack);

      if (err instanceof BadRequestError) {
        res.locals.status = StatusCodes.BAD_REQUEST;
        res.locals.body = getApiErrorBodyFromApiError(err);

        return next();
      }
      else if (err instanceof UnprocessableEntityError) {
        res.locals.status = StatusCodes.UNPROCESSABLE_ENTITY;
        res.locals.body = getApiErrorBodyFromApiError(err);

        return next();
      }
      else {
        return next(err);
      }
    });
}
