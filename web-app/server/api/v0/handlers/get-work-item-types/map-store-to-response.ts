// Import 3rd-party libraries.
import {
  isArray as _isArray,
  map as _map
} from 'lodash';

// Import local files.
import { WorkItemType as WorkItemTypeFromStore } from '../../../../store/types/postgres/work-item-type';
import { WorkItemType } from '../../types/work-item-type';
import ApiError from '../../exceptions/ApiError';
import { E_GENERAL_SERVER_ERROR } from '../../helpers/error-codes';

export default function mapStoreToResponse(workItemTypes: WorkItemTypeFromStore[]): Promise<WorkItemType[]> {
  return new Promise((resolve, reject) => {
    return !_isArray(workItemTypes)
      ? reject(new ApiError(E_GENERAL_SERVER_ERROR, [], 'The "set" of Work Item Types returned from the store is not an array.'))
      : resolve(_map(workItemTypes, ({
        slug,
        created_on,
        last_updated_on,
        name,
        description,
        parent_slug
      }) => ({
        slug,
        createdOn: created_on,
        lastUpdatedOn: last_updated_on,
        name,
        description,
        parentSlug: parent_slug
      })));
  });
}
