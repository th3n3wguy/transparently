/**
 * @api {get} /api/v0/users Get List of Work Item Types
 * @apiGroup Work Items
 * @apiName GetListOfWorkItemTypes
 * @apiVersion 0.1.0
 * @apiDescription Allows for the retrieval of a list of Work Item Types within the system.
 *
 * @apiParam (Query Parameters) {Number} [limit=50] The number of records returned within the response body.
 * @apiParam (Query Parameters) {Number} [offset=0] The "offset" / "page" for the list of records to be returned within the response body.
 * @apiParam (Query Parameters) {String} [sortDirection="asc"] The direction for the sort to be applied using the provided "sortKey" value. The values allowed are: "asc" and "desc".
 * @apiParam (Query Parameters) {String} [sortKey="slug"] The "key" value used to sort the records returned within the response body. The values allowed are: slug, createdOn, and lastUpdatedOn.
 *
 * @apiSuccess (200 - OK) {Object[]} workItemTypes
 * @apiSuccess (200 - OK) {String} workItemTypes.slug A unique string reference to the Work Item Type that can be used safely within URLs (https://en.wikipedia.org/wiki/Clean_URL#Slug).
 * @apiSuccess (200 - OK) {String} workItemTypes.createdOn The ISO8601-formatted string (YYYY-MM-DDTHH:mm:ss.sssZ) for when the Work Item Type was created in the system.
 * @apiSuccess (200 - OK) {String} workItemTypes.lastUpdatedOn The ISO8601-formatted string (YYYY-MM-DDTHH:mm:ss.sssZ) for when the Work Item Type was last updated in the system.
 * @apiSuccess (200 - OK) {String} workItemTypes.name A short-name that is associated to the Work Item Type.
 * @apiSuccess (200 - OK) {String} workItemTypes.description A lengthy description to provide additional details for the specification of the Work Item Type.
 * @apiSuccess (200 - OK) {String} workItemTypes.parentSlug The unique string reference to the Work Item Type's "parent" Work Item Type in the user-defined hierarchy.
 *
 * @apiUse BadRequest
 * @apiUse UnprocessableEntityError
 * @apiUse InternalServerError
 */
