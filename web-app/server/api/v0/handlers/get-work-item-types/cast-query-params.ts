// Import 3rd-party libraries.
import { toFinite as _toFinite } from 'lodash';

// Import local files.
import { GetWorkItemsParams } from '../../../../store/queries/get-work-item-types';
import { ValidatedRequestParams } from './validate-request-params';

export default function castQueryParams({ limit, offset, sortKey, sortDirection }: ValidatedRequestParams): Promise<GetWorkItemsParams> {
  const sortKeyToDatabaseColumnMap = {
    slug: 'slug',
    name: 'name',
    createdOn: 'created_on',
    lastUpdatedOn: 'lastUpdatedOn'
  };

  return new Promise((resolve) => {
    return resolve({
      limit: _toFinite(limit),
      offset: _toFinite(offset) * _toFinite(limit),
      sortDirection: sortDirection,
      sortKey: <GetWorkItemsParams['sortKey']>sortKeyToDatabaseColumnMap[sortKey]
    });
  });
}
