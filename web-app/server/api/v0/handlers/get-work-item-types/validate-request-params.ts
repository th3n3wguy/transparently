// Import 3rd-party libraries.
import { ParsedQs } from 'qs';
import {
  isEqual as _isEqual,
  keys as _keys,
  size as _size
} from 'lodash';

// Import local files.
import {
  isLimitValid,
  isOffsetValid,
  isSortDirectionValid,
  isSortKeyValid
} from '../../helpers/general-query-params-validators';
import UnprocessableEntityError from '../../exceptions/UnprocessableEntityError';
import {
  E_INVALID_LIMIT,
  E_INVALID_OFFSET,
  E_INVALID_SORT_DIRECTION,
  E_INVALID_SORT_KEY,
  E_UNKNOWN_QUERY_PARAMETER
} from '../../helpers/error-codes';
import BadRequestError from '../../exceptions/BadRequestError';
import { GetWorkItemsParams } from '../../../../store/queries/get-work-item-types';

export interface ValidatedRequestParams {
  limit: string;
  offset: string;
  sortDirection: GetWorkItemsParams['sortDirection'];
  sortKey: 'slug'|'name'|'createdOn'|'lastUpdatedOn';
}

export default function validateRequestParams({
  limit = '50',
  offset = '0',
  sortDirection = 'ASC',
  sortKey = 'slug',
  ...additionalQueryParams
}: ParsedQs): Promise<ValidatedRequestParams> {
  return new Promise((resolve, reject) => {
    if (!isLimitValid(limit, 100)) {
      return reject(new UnprocessableEntityError(E_INVALID_LIMIT, [limit], `The "limit" value provided (${limit}) was not valid.`));
    }
    else if (!isOffsetValid(offset)) {
      return reject(new UnprocessableEntityError(E_INVALID_OFFSET, [offset], `The "offset" value provided (${offset}) was not valid.`));
    }
    else if (!isSortDirectionValid(sortDirection)) {
      return reject(new UnprocessableEntityError(E_INVALID_SORT_DIRECTION, [sortDirection], `The "sortDirection" value provided (${sortDirection}) was not valid.`));
    }
    else if (!isSortKeyValid(sortKey, ['slug', 'name', 'createdOn', 'lastUpdatedOn'])) {
      return reject(new UnprocessableEntityError(E_INVALID_SORT_KEY, [sortKey], `The "sortKey" value provided (${sortKey}) was not valid.`));
    }
    else if (!_isEqual(_size(_keys(additionalQueryParams)), 0)) {
      return reject(new BadRequestError(E_UNKNOWN_QUERY_PARAMETER, _keys(additionalQueryParams), `There were additional query parameters (${JSON.stringify(_keys(additionalQueryParams))}) that were not valid for this request.`));
    }
    else {
      return resolve({
        limit: <string>limit,
        offset: <string>offset,
        sortDirection: <ValidatedRequestParams['sortDirection']>sortDirection,
        sortKey: <ValidatedRequestParams['sortKey']>sortKey
      });
    }
  });
}
