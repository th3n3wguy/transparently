/**
 * @api {get} /api/v0/health/readiness Check for Readiness
 * @apiGroup Health
 * @apiName CheckForReadiness
 * @apiVersion 0.1.0
 * @apiDescription Allows for an external system to verify whether the API is ready to take traffic (i.e. all dependent systems are functioning properly).
 *
 * @apiSuccess (204 - No Content) {Empty} noContent
 *
 * @apiUse InternalServerError
 */
