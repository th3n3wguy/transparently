// Import 3rd party libraries.
import { StatusCodes } from 'http-status-codes';
import { NextFunction, Response } from 'express';

// Import local files.
import { appLogger } from '../../../../config/logger';
import { E_GENERAL_SERVER_ERROR } from '../../helpers/error-codes';
import store from '../../../../store';
import { ModifiedRequest } from '../../../initialize-request-context';
import unleashService from '../../../../config/service-unleash';
import { getApiErrorBody } from '../../helpers/get-api-error-body-from-api-error';

export default function readinessCheck(req: ModifiedRequest, res: Response, next: NextFunction): void {
  store.queries.isDatabaseConnected()
    .then(() => unleashService.get('/health'))
    .then(() => {
      appLogger.info('Readiness Check: Successful');

      res.locals.status = StatusCodes.NO_CONTENT;
      res.locals.body = null;

      return next();
    })
    .catch((err: Error) => {
      appLogger.info('Readiness Check: Failure');
      appLogger.error(err.stack);

      res.locals.status = StatusCodes.INTERNAL_SERVER_ERROR;
      res.locals.body = getApiErrorBody(E_GENERAL_SERVER_ERROR, []);

      return next();
    });
}
