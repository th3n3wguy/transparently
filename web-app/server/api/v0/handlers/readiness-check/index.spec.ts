// Set up the mocks to be imported.
import { getApiErrorBody } from '../../helpers/get-api-error-body-from-api-error';

jest.mock('../../../../store');

// Import 3rd-party libraries.
import { StatusCodes } from 'http-status-codes';
import SpyInstance = jest.SpyInstance;
import { Response } from 'express';

// Import local files.
import { appLogger } from '../../../../config/logger';
import readinessCheck from './';
import { ModifiedRequest } from '../../../initialize-request-context';
import request from '../../../../tests/mocks/request';
import response from '../../../../tests/mocks/response';
import store from '../../../../store';
import ApiError from '../../exceptions/ApiError';
import { E_GENERAL_SERVER_ERROR } from '../../helpers/error-codes';
import unleashService from '../../../../config/service-unleash';

describe('api/v0/handlers/readiness-check', () => {
  test('should be a Function', () => {
    expect(readinessCheck).toEqual(expect.any(Function));
  });

  describe('when invoked', () => {
    let appLoggerInfo: SpyInstance;
    let appLoggerError: SpyInstance;
    let storeIsDatabaseConnectedMock: SpyInstance;
    let unleashServiceHealthCheckMock: SpyInstance;

    beforeEach(() => {
      appLoggerInfo = jest.spyOn(appLogger, 'info')
        .mockImplementation();
      appLoggerError = jest.spyOn(appLogger, 'error')
        .mockImplementation();
    });

    afterEach(() => {
      jest.clearAllMocks();
    });

    test('everything works as expected when no errors occur', () => {
      // Set up the mock data / spies / mocks used in this test.
      storeIsDatabaseConnectedMock = jest.spyOn(store.queries, 'isDatabaseConnected')
        .mockResolvedValue();
      unleashServiceHealthCheckMock = jest.spyOn(unleashService, 'get')
        .mockResolvedValue(null);

      return new Promise((resolve, reject) => {
        // Invoke the function being tested.
        readinessCheck(<ModifiedRequest>request, <Response>response, (err?: Error|string) => {
          try {
            // Set the expectations.
            expect(storeIsDatabaseConnectedMock).toHaveBeenCalledWith();
            expect(unleashServiceHealthCheckMock).toHaveBeenCalledWith('/health');
            expect(appLoggerInfo).toHaveBeenCalledWith('Readiness Check: Successful');
            expect(response?.locals?.status).toEqual(StatusCodes.NO_CONTENT);
            expect(response?.locals?.body).toBeNull();
            expect(err).toBeUndefined();

            // Ensure that the Promise is resolved if all of the expectations are successful.
            return resolve(null);
          }
          catch (e) {
            return reject(e);
          }
        });
      });
    });

    test('what happens when an error occurs while ensuring that the database is connected', () => {
      // Set up the mock data / spies / mocks used in this test.
      const databaseQueryError: Error = new Error('Error when verifying the database is connected.');

      storeIsDatabaseConnectedMock = jest.spyOn(store.queries, 'isDatabaseConnected')
        .mockRejectedValue(databaseQueryError);
      unleashServiceHealthCheckMock = jest.spyOn(unleashService, 'get')
        .mockResolvedValue(null);

      return new Promise((resolve, reject) => {
        // Invoke the function to be tested.
        readinessCheck(<ModifiedRequest>request, <Response>response, (err?: ApiError<unknown>|Error|string) => {
          try {
            // Set the expectations.
            expect(storeIsDatabaseConnectedMock).toHaveBeenCalledWith();
            expect(unleashServiceHealthCheckMock).toHaveBeenCalledTimes(0);
            expect(appLoggerInfo).toHaveBeenCalledWith('Readiness Check: Failure');
            expect(appLoggerError).toHaveBeenCalledWith(databaseQueryError.stack);
            expect(response?.locals?.status).toEqual(StatusCodes.INTERNAL_SERVER_ERROR);
            expect(response?.locals?.body).toEqual(getApiErrorBody(E_GENERAL_SERVER_ERROR, []));
            expect(err).toBeUndefined();

            // Ensure that the Promise is resolved if all the expectations are successful.
            return resolve(null);
          }
          catch (e) {
            return reject(e);
          }
        });
      });
    });

    test('what happens when an error occurs while ensuring that the Unleash server is "healthy"', () => {
      // Set up the mock data / spies / mocks used in this test.
      const unleashServiceApiFailure: Error = new Error('Error when verifying the Unleash Service is not ready.');

      storeIsDatabaseConnectedMock = jest.spyOn(store.queries, 'isDatabaseConnected')
        .mockResolvedValue();
      unleashServiceHealthCheckMock = jest.spyOn(unleashService, 'get')
        .mockRejectedValue(unleashServiceApiFailure);

      return new Promise((resolve, reject) => {
        // Invoke the function to be tested.
        readinessCheck(<ModifiedRequest>request, <Response>response, (err?: ApiError<unknown>|Error|string) => {
          try {
            // Set the expectations.
            expect(storeIsDatabaseConnectedMock).toHaveBeenCalledWith();
            expect(unleashServiceHealthCheckMock).toHaveBeenCalledWith('/health');
            expect(appLoggerInfo).toHaveBeenCalledWith('Readiness Check: Failure');
            expect(appLoggerError).toHaveBeenCalledWith(unleashServiceApiFailure.stack);
            expect(response?.locals?.status).toEqual(StatusCodes.INTERNAL_SERVER_ERROR);
            expect(response?.locals?.body).toEqual(getApiErrorBody(E_GENERAL_SERVER_ERROR, []));
            expect(err).toBeUndefined();

            // Ensure that the Promise is resolved if all the expectations are successful.
            return resolve(null);
          }
          catch (e) {
            return reject(e);
          }
        });
      });
    });
  });
});
