// Import 3rd-party libraries.
import { toFinite as _toFinite } from 'lodash';

// Import local files.
import { ValidatedQueryParameters } from './validate-query-parameters';
import { GetWorkItemsParams } from '../../../../store/queries/get-work-items';

const sortKeyToDatabaseTableMap: { [K: string]: GetWorkItemsParams['sortKey'] } = {
  type: 'type_slug',
  createdOn: 'created_on',
  lastUpdatedOn: 'last_updated_on'
};

export default function mapQueryParamsToStoreParams({
  limit,
  offset,
  sortKey,
  sortDirection
}: ValidatedQueryParameters): Promise<GetWorkItemsParams> {
  return new Promise((resolve) => {
    return resolve({
      limit: _toFinite(limit),
      offset: _toFinite(offset) * _toFinite(limit),
      sortKey: sortKeyToDatabaseTableMap[sortKey],
      sortDirection
    });
  });
}
