// Import 3rd-party libraries.
import { map as _map } from 'lodash';
import { format as dfFormat } from 'date-fns';

// Import local files.
import { WorkItem as WorkItemFromStore } from '../../../../store/types/store-interfaces/work-item';
import { WorkItem } from '../../types/work-item';
import { DATE_FNS_TIMEZONE_FORMAT } from '../../helpers/constants';

export default function mapStoreToResponse(workItems: WorkItemFromStore[]): Promise<WorkItem[]> {
  return new Promise((resolve) => {
    return resolve(_map(workItems, (workItem) => ({
      workItemId: workItem.work_item_id,
      createdOn: dfFormat(workItem.created_on, DATE_FNS_TIMEZONE_FORMAT),
      lastUpdatedOn: dfFormat(workItem.last_updated_on, DATE_FNS_TIMEZONE_FORMAT),
      name: workItem.name,
      description: workItem.description,
      typeSlug: workItem.type_slug
    })));
  });
}
