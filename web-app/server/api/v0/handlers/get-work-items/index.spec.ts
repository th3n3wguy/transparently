describe('/api/v0/handlers/get-work-items', () => {
  describe('happy path tests', () => {
    test.todo('the query parameters provided are all valid');

    test.todo('no query parameters are provided');

    test.todo('only part of the query parameters are provided');
  });

  describe('negative tests', () => {
    test.todo('one of the query parameters provided is invalid');

    test.todo('additional query parameters are provided');

    test.todo('the store returns an error');
  });
});
