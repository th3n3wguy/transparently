/**
 * @api {get} /api/v0/users Get List of Work Items
 * @apiGroup Work Items
 * @apiName GetListOfWorkItems
 * @apiVersion 0.1.0
 * @apiDescription Allows for the retrieval of a list of Work Items within the system.
 *
 * @apiParam (Query Parameters) {Number} [limit=50] The number of records returned within the response body.
 * @apiParam (Query Parameters) {Number} [offset=0] The "offset" / "page" for the list of records to be returned within the response body.
 * @apiParam (Query Parameters) {String} [sortDirection="desc"] The direction for the sort to be applied using the provided "sortKey" value. The values allowed are: "asc" and "desc".
 * @apiParam (Query Parameters) {String} [sortKey="createdOn"] The "key" value used to sort the records returned within the response body. The values allowed are: typeSlug, createdOn, lastUpdatedOn
 *
 * @apiSuccess (200 - OK) {Object[]} workItems
 * @apiSuccess (200 - OK) {String} workItems.workItemId The unique identifier (ULID) that is created by the system and associated to the Work Item.
 * @apiSuccess (200 - OK) {String} workItems.createdOn The ISO8601-formatted string (YYYY-MM-DDTHH:mm:ss.sssZ) for when the Work Item was created in the system.
 * @apiSuccess (200 - OK) {String} workItems.lastUpdatedOn The ISO8601-formatted string (YYYY-MM-DDTHH:mm:ss.sssZ) for when the Work Item was last updated in the system.
 * @apiSuccess (200 - OK) {String} workItems.name The short-name value that was provided when the Work Item was created.
 * @apiSuccess (200 - OK) {String} workItems.description A Markdown-formatted string that provides a full list of details which was provided when the Work Item was created.
 * @apiSuccess (200 - OK) {String} workItems.typeSlug The "slug" that references a specific Work Item Type value. This is a unique string and is HTML/URL-safe.
 *
 * @apiUse UnprocessableEntityError
 * @apiUse InternalServerError
 */
