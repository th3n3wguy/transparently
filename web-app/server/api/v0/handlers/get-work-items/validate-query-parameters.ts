// Import 3rd-party libraries.
import { ParsedQs } from 'qs';
import { toString as _toString } from 'lodash';

// Import local files.
import { GetWorkItemsParams } from '../../../../store/queries/get-work-items';
import {
  isLimitValid,
  isOffsetValid,
  isSortDirectionValid,
  isSortKeyValid
} from '../../helpers/general-query-params-validators';
import UnprocessableEntityError from '../../exceptions/UnprocessableEntityError';
import {
  E_INVALID_LIMIT,
  E_INVALID_OFFSET,
  E_INVALID_SORT_DIRECTION,
  E_INVALID_SORT_KEY
} from '../../helpers/error-codes';

export enum ValidSortKeys {
  type,
  createdOn,
  lastUpdatedOn
}
export interface ValidatedQueryParameters {
  limit: string|number;
  offset: string|number;
  sortDirection: GetWorkItemsParams['sortDirection'];
  sortKey: 'type'|'createdOn'|'lastUpdatedOn';
}

export default function validateQueryParams({
  limit = '50',
  offset = '0',
  sortDirection = 'DESC',
  sortKey = 'createdOn'
}: ParsedQs): Promise<ValidatedQueryParameters> {
  return new Promise((resolve, reject) => {
    if (!isLimitValid(limit, 100)) {
      return reject(new UnprocessableEntityError(E_INVALID_LIMIT, [limit], `The provided limit value (${limit}) was not valid.`));
    }
    else if (!isOffsetValid(offset)) {
      return reject(new UnprocessableEntityError(E_INVALID_OFFSET, [offset], `The provided offset value (${offset}) was not valid.`));
    }
    else if (!isSortKeyValid(sortKey, ['type', 'createdOn', 'lastUpdatedOn'])) {
      return reject(new UnprocessableEntityError(E_INVALID_SORT_KEY, [sortKey], `The provided sortKey value (${sortKey}) was not valid.`));
    }
    else if (!isSortDirectionValid(sortDirection)) {
      return reject(new UnprocessableEntityError(E_INVALID_SORT_DIRECTION, [sortDirection], `The provided sortDirection value (${sortDirection}) was not valid.`));
    }
    else {
      return resolve({
        limit: _toString(limit),
        offset: _toString(offset),
        sortKey: <ValidatedQueryParameters['sortKey']>sortKey,
        sortDirection: <ValidatedQueryParameters['sortDirection']>sortDirection
      });
    }
  });
}
