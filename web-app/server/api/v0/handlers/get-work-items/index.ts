// Import 3rd-party libraries.
import {
  NextFunction,
  Response
} from 'express';
import { StatusCodes } from 'http-status-codes';

// Import local files.
import { ModifiedRequest } from '../../../initialize-request-context';
import store from '../../../../store';
import validateQueryParams from './validate-query-parameters';
import mapQueryParamsToStoreParams from './map-query-params-to-store-params';
import mapStoreToResponse from './map-store-to-response';
import getApiErrorBodyFromApiError from '../../helpers/get-api-error-body-from-api-error';
import { appLogger } from '../../../../config/logger';
import DatabaseQueryError from '../../../../store/exceptions/DatabaseQueryError';
import UnprocessableEntityError from '../../exceptions/UnprocessableEntityError';
import { QueryString } from '../../types/query-string';

export default function getWorkItems(req: ModifiedRequest, res: Response, next: NextFunction): void {
  // TODO: Probably should set the default parameters here or in another function instead of the validateQueryParams function.
  validateQueryParams(req.query)
    .then(mapQueryParamsToStoreParams)
    .then(store.queries.getWorkItems)
    .then(mapStoreToResponse)
    .then((workItems) => {
      res.locals.status = StatusCodes.OK;
      res.locals.body = workItems;

      return next();
    })
    .catch((err: Error|UnprocessableEntityError<QueryString>|DatabaseQueryError) => {
      appLogger.info('Failure when attempting to retrieve the Work Items.');
      appLogger.error(err.stack);

      if (err instanceof UnprocessableEntityError) {
        res.locals.status = StatusCodes.UNPROCESSABLE_ENTITY;
        res.locals.body = getApiErrorBodyFromApiError(err);

        return next();
      }
      else {
        return next(err);
      }
    });
}
