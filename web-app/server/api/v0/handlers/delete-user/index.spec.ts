describe('/api/v0/handlers/delete-user', () => {
  describe('when provided a valid "userId" value', () => {
    test.todo('should return a 204 (No Content) with an empty response body when the "userId" exists within the store');

    test.todo('should return a 404 (Not Found) with the ApiError response body and the E_USER_NOT_FOUND error code when the "userId" does NOT exist within the store');

    test.todo('should return a 500 (Internal Server Error) with the ApiError response body and the E_GENERAL_SERVER_ERROR error code when there is an unknown error when attempting to delete the user from the store');
  });

  describe('when provided an invalid "userId" value', () => {
    // This should be a .each with a bunch of different invalid types of "userId" values.
    test.todo('should return a 400 (Bad Request) when the ApiError response body and the E_INVALID_USER_ID error code');
  });
});
