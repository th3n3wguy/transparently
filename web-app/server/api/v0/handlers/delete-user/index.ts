// Import 3rd-party libraries.
import {
  NextFunction,
  Response
} from 'express';
import { StatusCodes } from 'http-status-codes';

// Import local files.
import { ModifiedRequest } from '../../../initialize-request-context';
import { appLogger } from '../../../../config/logger';
import store from '../../../../store';
import { getApiErrorBody } from '../../helpers/get-api-error-body-from-api-error';
import { E_GENERAL_SERVER_ERROR } from '../../helpers/error-codes';
import DatabaseQueryError from '../../../../store/exceptions/DatabaseQueryError';
import UserNotFoundError from '../../../../store/exceptions/UserNotFoundError';

export default function deleteUser(req: ModifiedRequest, res: Response, next: NextFunction): void {
  store.commands.deleteUser(req.params.userId)
    .then(() => {
      res.locals.status = StatusCodes.NO_CONTENT;
      res.locals.body = null;

      return next();
    })
    .catch((err: DatabaseQueryError|UserNotFoundError) => {
      appLogger.error('There was an error when attempting to delete the User using the provided userId value.');
      appLogger.debug(err);

      res.locals.status = StatusCodes.INTERNAL_SERVER_ERROR;
      res.locals.body = getApiErrorBody(E_GENERAL_SERVER_ERROR, []);

      return next();
    });
}
