/**
 * @api {post} /api/v0/users/:userId Delete a User
 * @apiGroup Users
 * @apiName DeleteUser
 * @apiVersion 0.1.0
 * @apiDescription Delete a User within the system using the User's ID.
 *
 * @apiParam {String{..255}} userId The unique identifier that is created by the system and associated to the User's account. While this is technically the string-representation of a ULID, this might not be the case in the future, so do not rely upon this assumption to be true.
 *
 * @apiSuccess (204 - No Content) {Empty} noContent
 *
 * @apiUse BadRequest
 * @apiUse InternalServerError
 * @apiUse NotFound
 * @apiUse UnprocessableEntityError
 */
