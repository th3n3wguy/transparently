// Import 3rd-party libraries.
import { StatusCodes } from 'http-status-codes';
import { Response } from 'express';

// Import local files.
import livenessCheck from './';
import { ModifiedRequest } from '../../../initialize-request-context';
import request from '../../../../tests/mocks/request';
import response from '../../../../tests/mocks/response';

describe('api/v0/handlers/liveness-check', () => {
  test('should be a Function', () => {
    expect(livenessCheck).toEqual(expect.any(Function));
  });

  describe('when invoked', () => {
    afterEach(() => {
      jest.clearAllMocks();
    });

    test('should set the expected response properties', () => {
      return new Promise((resolve) => {
        livenessCheck(<ModifiedRequest>request, <Response>response, () => {
          expect(response?.locals?.status).toEqual(StatusCodes.NO_CONTENT);
          expect(response?.locals?.body).toBeNull();

          return resolve(null);
        });
      });
    });
  });
});
