// Import 3rd party libraries.
import { StatusCodes } from 'http-status-codes';

// Import types.
import { ModifiedRequest } from '../../../initialize-request-context';
import { NextFunction, Response } from 'express';

export default function livenessCheck(req: ModifiedRequest, res: Response, next: NextFunction): void {
  // Set the response values.
  res.locals.status = StatusCodes.NO_CONTENT;
  res.locals.body = null;

  return next();
}
