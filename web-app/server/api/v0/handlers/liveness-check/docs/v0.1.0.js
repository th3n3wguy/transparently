/**
 * @api {get} /api/v0/health/liveness Check for Liveness
 * @apiGroup Health
 * @apiName CheckForLiveness
 * @apiVersion 0.1.0
 * @apiDescription Allows for an external system to verify whether the API is running or not.
 *
 * @apiSuccess (204 - No Content) {Empty} noContent
 *
 * @apiUse InternalServerError
 */
