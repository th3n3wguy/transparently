/**
 * @api {get} /api/v0/users Get List of Users
 * @apiGroup Users
 * @apiName GetListOfUsers
 * @apiVersion 0.1.0
 * @apiDescription Allows for the retrieval of a list of Users within the system.
 *
 * @apiParam (Query Parameters) {Number} [limit=50] The number of records returned within the response body.
 * @apiParam (Query Parameters) {Number} [offset=0] The "offset" / "page" for the list of records to be returned within the response body.
 * @apiParam (Query Parameters) {String} [sortDirection="asc"] The direction for the sort to be applied using the provided "sortKey" value. The values allowed are: "asc" and "desc".
 * @apiParam (Query Parameters) {String} [sortKey="username"] The "key" value used to sort the records returned within the response body. The values allowed are: "username", "createdOn", and "lastUpdatedOn".
 *
 * @apiSuccess (200 - OK) {Object[]} users
 * @apiSuccess (200 - OK) {String} users.userId The unique identifier (UUID) that is created by the system and associated to the User's account.
 * @apiSuccess (200 - OK) {String{..255}} users.username The unique username created by the User.
 * @apiSuccess (200 - OK) {String{..255}} users.emailAddress The email address associated to the User.
 * @apiSuccess (200 - OK) {String{..5000}} users.fullName The full name value that will be displayed in certain places within the application that is provided by the User.
 * @apiSuccess (200 - OK) {String} users.createdOn The ISO8601-formatted string (YYYY-MM-DDTHH:mm:ss.sssZ) for when the User was created in the system.
 * @apiSuccess (200 - OK) {String} users.lastUpdatedOn The ISO8601-formatted string (YYYY-MM-DDTHH:mm:ss.sssZ) for when the User was last updated in the system.
 *
 * @apiUse UnprocessableEntityError
 * @apiUse InternalServerError
 */
