// Import 3rd-party libraries.
import { toString as _toString } from 'lodash';
import { ParsedQs } from 'qs';

// Import local files.
import UnprocessableEntityError from '../../exceptions/UnprocessableEntityError';
import {
  E_INVALID_LIMIT,
  E_INVALID_OFFSET,
  E_INVALID_SORT_DIRECTION,
  E_INVALID_SORT_KEY
} from '../../helpers/error-codes';
import { ValidatedQueryParameters } from './types';
import {
  isLimitValid,
  isOffsetValid,
  isSortDirectionValid,
  isSortKeyValid
} from '../../helpers/general-query-params-validators';

export default function validateQueryParameters({
  limit = '50',
  offset = '0',
  sortDirection = 'ASC',
  sortKey = 'username'
}: ParsedQs): Promise<ValidatedQueryParameters> {
  return new Promise((resolve, reject) => {
    if (!isLimitValid(limit, 100)) {
      return reject(new UnprocessableEntityError(E_INVALID_LIMIT, [limit], `The "limit" value provided (${limit}) was not valid.`));
    }
    else if (!isOffsetValid(offset)) {
      return reject(new UnprocessableEntityError(E_INVALID_OFFSET, [offset], `The "offset" value provided (${offset}) was not valid.`));
    }
    else if (!isSortDirectionValid(sortDirection)) {
      return reject(new UnprocessableEntityError(E_INVALID_SORT_DIRECTION, [sortDirection], `The "sortDirection" provided (${sortDirection}) was not valid.`));
    }
    else if (!isSortKeyValid(sortKey, ['username', 'createdOn', 'lastUpdatedOn'])) {
      return reject(new UnprocessableEntityError(E_INVALID_SORT_KEY, [sortKey], `The "sortKey" provided (${sortKey}) was not valid.`));
    }
    else {
      return resolve({
        limit: _toString(limit),
        offset: _toString(offset),
        sortDirection: <ValidatedQueryParameters['sortDirection']>sortDirection,
        sortKey: <ValidatedQueryParameters['sortKey']>sortKey
      });
    }
  });
}
