// Import 3rd-party libraries.
import { noop as _noop } from 'lodash';

// Import local files.
import { default as convertQueryParamsToDatabaseParams } from './convert-query-params-to-database-params';
import { ValidatedQueryParameters } from './types';

describe('/api/v0/handlers/get-users/convert-query-params-to-database-params', () => {
  test('should be a Function', () => {
    expect(convertQueryParamsToDatabaseParams).toEqual(expect.any(Function));
  });

  describe('when invoked', () => {
    test.each(
      [
        ['50', 50, '0', 0, 'asc', 'asc', 'username', 'username'],
        [50, 50, '0', 0, 'asc', 'asc', 'username', 'username'],
        ['50', 50, 0, 0, 'asc', 'asc', 'username', 'username'],
        ['50', 50, '0', 0, 'ASC', 'ASC', 'username', 'username'],
        ['50', 50, '0', 0, 'desc', 'desc', 'username', 'username'],
        ['50', 50, '0', 0, 'DESC', 'DESC', 'username', 'username'],
        ['50', 50, '0', 0, 'asc', 'asc', 'createdOn', 'created_on'],
        ['50', 50, '0', 0, 'asc', 'asc', 'lastUpdatedOn', 'last_updated_on'],
        ['50', 50, '0', 0, 'asc', 'asc', 'someDisallowedValue', _noop()]
      ]
    )('should convert the provided parameters to the proper type and format', (
      testLimit,
      expectedLimit,
      testOffset,
      expectedOffset,
      testSortDirection,
      expectedSortDirection,
      testSortKey,
      expectedSortKey
    ) => {
      return new Promise((resolve, reject) => {
        // Invoke the function to be tested.
        convertQueryParamsToDatabaseParams({
          limit: testLimit,
          offset: testOffset,
          sortDirection: <ValidatedQueryParameters['sortDirection']>testSortDirection,
          sortKey: <ValidatedQueryParameters['sortKey']>testSortKey
        })
          .then(({
            limit,
            offset,
            sortDirection,
            sortKey
          }) => {
            // Set the expectations.
            expect(limit).toEqual(expectedLimit);
            expect(offset).toEqual(expectedOffset);
            expect(sortDirection).toEqual(expectedSortDirection);
            expect(sortKey).toEqual(expectedSortKey);

            return resolve(null);
          })
          .catch(reject);
      });
    });
  });
});
