// Import 3rd-party libraries.
import { ParsedQs } from 'qs';
import { assign as _assign } from 'lodash';

// Import local files.
import validateQueryParameters from './validate-query-parameters';
import UnprocessableEntityError from '../../exceptions/UnprocessableEntityError';
import {
  E_INVALID_LIMIT,
  E_INVALID_OFFSET,
  E_INVALID_SORT_DIRECTION,
  E_INVALID_SORT_KEY
} from '../../helpers/error-codes';

describe('/api/v0/handlers/get-users/validate-query-parameters', () => {
  test('should be a Function', () => {
    expect(validateQueryParameters).toEqual(expect.any(Function));
  });

  describe('when invoked', () => {
    const validQueryParameters: ParsedQs = {
      limit: '50',
      offset: '0',
      sortDirection: 'ASC',
      sortKey: 'username'
    };

    test('the default parameters are returned if no parameters are provided to the function', () => {
      return new Promise((resolve, reject) => {
        // Execute the function.
        validateQueryParameters({})
          .then((params) => {
            // Set the expectations.
            expect(params.limit).toEqual('50');
            expect(params.offset).toEqual('0');
            expect(params.sortDirection).toEqual('ASC');
            expect(params.sortKey).toEqual('username');

            // Ensure that the Promise is resolved if all of the expectations are successful.
            return resolve(null);
          })
          .catch(reject);
      });
    });

    test('when the parameters provided are all valid', () => {
      return new Promise((resolve, reject) => {
        // Execute the function to be tested.
        validateQueryParameters(validQueryParameters)
          .then((params) => {
            // Set the expectations.
            expect(params).toEqual(validQueryParameters);

            // Ensure that the Promise is resolved if all of the expectations are successful.
            return resolve(null);
          })
          .catch(reject);
      });
    });

    test.each(
      [
        [_assign({}, validQueryParameters, { limit: '-50' }), new UnprocessableEntityError(E_INVALID_LIMIT, ['-50'], 'The "limit" value provided (-50) was not valid.')],
        [_assign({}, validQueryParameters, { limit: '200' }), new UnprocessableEntityError(E_INVALID_LIMIT, ['200'], 'The "limit" value provided (200) was not valid.')],
        [_assign({}, validQueryParameters, { limit: '0' }), new UnprocessableEntityError(E_INVALID_LIMIT, ['0'], 'The "limit" value provided (0) was not valid.')],
        [_assign({}, validQueryParameters, { limit: '30.53' }), new UnprocessableEntityError(E_INVALID_LIMIT, ['30.53'], 'The "limit" value provided (30.53) was not valid.')]
      ]
    )('when the limit provided is invalid', (providedParams, expectedError) => {
      return new Promise((resolve, reject) => {
        // Execute the function to be tested.
        validateQueryParameters(providedParams)
          .then(() => reject(new Error(`This Promise should not have resolved for (${JSON.stringify(providedParams)}).`)))
          .catch((err) => {
            // Set the expectations.
            expect(err).toEqual(expectedError);

            // Ensure that the Promise is resolved if all of the expectations are successful.
            return resolve(null);
          });
      });
    });

    test.each(
      [
        [_assign({}, validQueryParameters, { offset: '-1' }), new UnprocessableEntityError(E_INVALID_OFFSET, ['-1'], 'The "offset" value provided (-1) was not valid.')],
        [_assign({}, validQueryParameters, { offset: '50.432' }), new UnprocessableEntityError(E_INVALID_OFFSET, ['50.432'], 'The "offset" value provided (50.432) was not valid.')],
        [_assign({}, validQueryParameters, { offset: 'Infinity' }), new UnprocessableEntityError(E_INVALID_OFFSET, ['Infinity'], 'The "offset" value provided (Infinity) was not valid.')]
      ]
    )('when the offset provided is invalid', (providedParams, expectedError) => {
      return new Promise((resolve, reject) => {
        // Execute the function to be tested.
        validateQueryParameters(providedParams)
          .then(() => reject(new Error(`The offset provided as a test value (${providedParams.offset}) should have thrown an error, but didn't.`)))
          .catch((err) => {
            // Set the expectations.
            expect(err).toEqual(expectedError);

            // Ensure that the Promise is resolved if all of the expectations are successful.
            return resolve(null);
          });
      });
    });

    test.each(
      [
        [_assign({}, validQueryParameters, { sortDirection: 'Asc' }), new UnprocessableEntityError(E_INVALID_SORT_DIRECTION, ['Asc'], 'The "sortDirection" provided (Asc) was not valid.')],
        [_assign({}, validQueryParameters, { sortDirection: 'AsC' }), new UnprocessableEntityError(E_INVALID_SORT_DIRECTION, ['AsC'], 'The "sortDirection" provided (AsC) was not valid.')],
        [_assign({}, validQueryParameters, { sortDirection: 'ASc' }), new UnprocessableEntityError(E_INVALID_SORT_DIRECTION, ['ASc'], 'The "sortDirection" provided (ASc) was not valid.')],
        [_assign({}, validQueryParameters, { sortDirection: 'asC' }), new UnprocessableEntityError(E_INVALID_SORT_DIRECTION, ['asC'], 'The "sortDirection" provided (asC) was not valid.')],
        [_assign({}, validQueryParameters, { sortDirection: 'aSc' }), new UnprocessableEntityError(E_INVALID_SORT_DIRECTION, ['aSc'], 'The "sortDirection" provided (aSc) was not valid.')],
        [_assign({}, validQueryParameters, { sortDirection: 'aSC' }), new UnprocessableEntityError(E_INVALID_SORT_DIRECTION, ['aSC'], 'The "sortDirection" provided (aSC) was not valid.')]
      ]
    )('when the sortDirection provided is invalid', (providedParams, expectedError) => {
      return new Promise((resolve, reject) => {
        // Execute the function to be tested.
        validateQueryParameters(providedParams)
          .then(() => reject(new Error(`The sortDirection provided as a test value (${providedParams.sortDirection}) should have thrown an error, but didn't.`)))
          .catch((err) => {
            // Set the expectations.
            expect(err).toEqual(expectedError);

            // Ensure that the Promise is resolved if all of the expectations are successful.
            return resolve(null);
          });
      });
    });

    // THe list of sortKeys provided this test suite is not comprehensive of all of the "failure" possibilities.
    test.each(
      [
        [_assign({}, validQueryParameters, { sortKey: 'userName' }), new UnprocessableEntityError(E_INVALID_SORT_KEY, ['userName'], 'The "sortKey" provided (userName) was not valid.')],
        [_assign({}, validQueryParameters, { sortKey: 'userNAME' }), new UnprocessableEntityError(E_INVALID_SORT_KEY, ['userNAME'], 'The "sortKey" provided (userNAME) was not valid.')],
        [_assign({}, validQueryParameters, { sortKey: 'UserName' }), new UnprocessableEntityError(E_INVALID_SORT_KEY, ['UserName'], 'The "sortKey" provided (UserName) was not valid.')],
        [_assign({}, validQueryParameters, { sortKey: 'Username' }), new UnprocessableEntityError(E_INVALID_SORT_KEY, ['Username'], 'The "sortKey" provided (Username) was not valid.')],
        [_assign({}, validQueryParameters, { sortKey: 'User_Name' }), new UnprocessableEntityError(E_INVALID_SORT_KEY, ['User_Name'], 'The "sortKey" provided (User_Name) was not valid.')],
        [_assign({}, validQueryParameters, { sortKey: 'user_name' }), new UnprocessableEntityError(E_INVALID_SORT_KEY, ['user_name'], 'The "sortKey" provided (user_name) was not valid.')],
        [_assign({}, validQueryParameters, { sortKey: 'createdon' }), new UnprocessableEntityError(E_INVALID_SORT_KEY, ['createdon'], 'The "sortKey" provided (createdon) was not valid.')],
        [_assign({}, validQueryParameters, { sortKey: 'createdON' }), new UnprocessableEntityError(E_INVALID_SORT_KEY, ['createdON'], 'The "sortKey" provided (createdON) was not valid.')],
        [_assign({}, validQueryParameters, { sortKey: 'CreatedOn' }), new UnprocessableEntityError(E_INVALID_SORT_KEY, ['CreatedOn'], 'The "sortKey" provided (CreatedOn) was not valid.')],
        [_assign({}, validQueryParameters, { sortKey: 'Createdon' }), new UnprocessableEntityError(E_INVALID_SORT_KEY, ['Createdon'], 'The "sortKey" provided (Createdon) was not valid.')],
        [_assign({}, validQueryParameters, { sortKey: 'created_on' }), new UnprocessableEntityError(E_INVALID_SORT_KEY, ['created_on'], 'The "sortKey" provided (created_on) was not valid.')],
        [_assign({}, validQueryParameters, { sortKey: 'lastupdatedon' }), new UnprocessableEntityError(E_INVALID_SORT_KEY, ['lastupdatedon'], 'The "sortKey" provided (lastupdatedon) was not valid.')],
        [_assign({}, validQueryParameters, { sortKey: 'Lastupdatedon' }), new UnprocessableEntityError(E_INVALID_SORT_KEY, ['Lastupdatedon'], 'The "sortKey" provided (Lastupdatedon) was not valid.')],
        [_assign({}, validQueryParameters, { sortKey: 'LastUpdatedon' }), new UnprocessableEntityError(E_INVALID_SORT_KEY, ['LastUpdatedon'], 'The "sortKey" provided (LastUpdatedon) was not valid.')],
        [_assign({}, validQueryParameters, { sortKey: 'LastUpdatedOn' }), new UnprocessableEntityError(E_INVALID_SORT_KEY, ['LastUpdatedOn'], 'The "sortKey" provided (LastUpdatedOn) was not valid.')],
        [_assign({}, validQueryParameters, { sortKey: 'last_updated_on' }), new UnprocessableEntityError(E_INVALID_SORT_KEY, ['last_updated_on'], 'The "sortKey" provided (last_updated_on) was not valid.')]
      ]
    )('when the sortKey provided is invalid', (providedParams, expectedError) => {
      return new Promise((resolve, reject) => {
        // Execute the function to be tested.
        validateQueryParameters(providedParams)
          .then(() => reject(new Error(`The sortKey provided as a test value (${providedParams.sortKey}) should have thrown an error, but didn't.`)))
          .catch((err) => {
            // Set the expectations.
            expect(err).toEqual(expectedError);

            // Ensure that the Promise is resolved if all of the expectations are successful.
            return resolve(null);
          })
          .catch(reject);
      });
    });
  });
});
