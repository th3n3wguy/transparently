// Import 3rd-party libraries.
import { StatusCodes } from 'http-status-codes';
import { NextFunction, Response } from 'express';

// Import local files.
import { appLogger } from '../../../../config/logger';
import store from '../../../../store';
import { ModifiedRequest } from '../../../initialize-request-context';
import validateQueryParameters from './validate-query-parameters';
import getApiErrorBodyFromApiError from '../../helpers/get-api-error-body-from-api-error';
import UnprocessableEntityError from '../../exceptions/UnprocessableEntityError';
import { User } from '../../types/user';
import convertQueryParamsToDatabaseParams from './convert-query-params-to-database-params';
import mapDatabaseUsersToResponseUsers from './map-database-users-to-response-users';
import { QueryString } from '../../types/query-string';

export default function getUsers(req: ModifiedRequest, res: Response, next: NextFunction): void {
  validateQueryParameters(req.query)
    .then(convertQueryParamsToDatabaseParams)
    .then(store.queries.getUsers)
    .then(mapDatabaseUsersToResponseUsers)
    .then((users: User.ResponseObject[]) => {
      appLogger.info('Successfully retrieved the list of Users.');
      appLogger.debug(users);

      res.locals.status = StatusCodes.OK;
      res.locals.body = users;

      return next();
    })
    .catch((err: Error|UnprocessableEntityError<QueryString>) => {
      if (err instanceof UnprocessableEntityError) {
        res.locals.status = StatusCodes.UNPROCESSABLE_ENTITY;
        res.locals.body = getApiErrorBodyFromApiError(err);

        return next();
      }
      else {
        return next(err);
      }
    });
}
