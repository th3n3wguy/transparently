// Set up the mocks to be imported.
import { GetUsersQueryParams } from '../../../../store/queries/get-users';

jest.mock('./validate-query-parameters');
jest.mock('./convert-query-params-to-database-params');
jest.mock('../../../../store');

// Import 3rd-party libraries.
import Mock = jest.Mock;
import SpyInstance = jest.SpyInstance;
import { Response } from 'express';
import { StatusCodes } from 'http-status-codes';

// Import local files.
import { appLogger } from '../../../../config/logger';
import getUsers from './';
import validateQueryParameters from './validate-query-parameters';
import store from '../../../../store';
import { ModifiedRequest } from '../../../initialize-request-context';
import request from '../../../../tests/mocks/request';
import response from '../../../../tests/mocks/response';
import UnprocessableEntityError from '../../exceptions/UnprocessableEntityError';
import ApiError from '../../exceptions/ApiError';
import {
  E_GENERAL_SERVER_ERROR,
  E_INVALID_LIMIT
} from '../../helpers/error-codes';
import getApiErrorBodyFromApiError from '../../helpers/get-api-error-body-from-api-error';
import { getValidUserFromStore } from '../../../../tests/mocks/user';
import { User } from '../../../../store/types/store-interfaces/users';
import convertQueryParamsToDatabaseParams from './convert-query-params-to-database-params';

describe('/api/v0/handlers/get-users', () => {
  test('should be a Function', () => {
    expect(getUsers).toEqual(expect.any(Function));
  });

  describe('when invoked', () => {
    const validQueryParameters: GetUsersQueryParams = {
      limit: 50,
      offset: 0,
      sortDirection: 'ASC',
      sortKey: 'username'
    };
    const validateQueryParametersMock: Mock = <Mock>validateQueryParameters;
    const convertQueryParamsToDatabaseParamsMock: Mock = <Mock>convertQueryParamsToDatabaseParams;
    let appLoggerInfo: SpyInstance;
    let storeQueryGetUsers: SpyInstance;

    beforeEach(() => {
      appLoggerInfo = jest.spyOn(appLogger, 'info').mockImplementation();
    });

    afterEach(() => {
      jest.clearAllMocks();
      validateQueryParametersMock.mockClear();
      convertQueryParamsToDatabaseParamsMock.mockClear();
    });

    test('the response local values are set when no errors occur', () => {
      // Define the mocked data used in this test.
      const mockedUsers: User.Core[] = [getValidUserFromStore({})];

      // Set up the spies & mocks used in this test.
      validateQueryParametersMock.mockResolvedValue(validQueryParameters);
      convertQueryParamsToDatabaseParamsMock.mockResolvedValue(validQueryParameters);
      storeQueryGetUsers = jest.spyOn(store.queries, 'getUsers')
        .mockResolvedValue(mockedUsers);

      return new Promise((resolve, reject) => {
        getUsers(<ModifiedRequest>request, <Response>response, (err: Error|string) => {
          // Set the expectations.
          try {
            expect(validateQueryParametersMock).toHaveBeenCalledWith(request.query);
            expect(storeQueryGetUsers).toHaveBeenCalledWith(validQueryParameters);
            expect(appLoggerInfo).toHaveBeenCalledWith('Successfully retrieved the list of Users.');
            expect(response?.locals?.status).toEqual(StatusCodes.OK);
            expect(response?.locals?.body[0]).toEqual({
              userId: mockedUsers[0].user_id,
              createdOn: mockedUsers[0].created_on,
              lastUpdatedOn: mockedUsers[0].last_updated_on,
              emailAddress: mockedUsers[0].email_address,
              fullName: mockedUsers[0].full_name,
              username: mockedUsers[0].username
            });
            expect(err).toBeUndefined();

            // Ensure that the Promise is resolved if all of the expectations are successful.
            return resolve(null);
          }
          catch (e) {
            return reject(e);
          }
        });
      });
    });

    test('when the "validateQueryParameters()" throws an UnprocessableEntityError', () => {
      // Define the mocked data used in this test.
      const expectedError: UnprocessableEntityError<undefined> = new UnprocessableEntityError(E_INVALID_LIMIT, [], 'Error when validating the query parameters.');
      const mockedUsers: User.Core[] = [getValidUserFromStore({})];

      // Set up the spies & mocks used in this test.
      validateQueryParametersMock.mockRejectedValue(expectedError);
      convertQueryParamsToDatabaseParamsMock.mockResolvedValue(validQueryParameters);
      storeQueryGetUsers = jest.spyOn(store.queries, 'getUsers')
        .mockResolvedValue(mockedUsers);

      return new Promise((resolve, reject) => {
        // Invoke the function to be tested.
        getUsers(<ModifiedRequest>request, <Response>response, (err: Error|string) => {
          try {
            // Set the expectations.
            expect(validateQueryParametersMock).toHaveBeenCalledWith(request.query);
            expect(convertQueryParamsToDatabaseParamsMock).not.toHaveBeenCalled();
            expect(storeQueryGetUsers).not.toHaveBeenCalled();
            expect(response?.locals?.status).toEqual(StatusCodes.UNPROCESSABLE_ENTITY);
            expect(response?.locals?.body).toEqual(getApiErrorBodyFromApiError(expectedError));
            expect(err).toBeUndefined();

            // Ensure that the Promise is resolved if all the expectations are successful.
            return resolve(null);
          }
          catch (e) {
            return reject(e);
          }
        });
      });
    });

    test.skip('when the "store.queries.createUser()" throws an Error', () => {
      // Define the mocked data used in this test.
      const expectedError: Error = new Error('Error when attempting to retrieve the users from the store.');

      // Set up the spies & mocks used in this test.
      validateQueryParametersMock.mockResolvedValue(validQueryParameters);
      convertQueryParamsToDatabaseParamsMock.mockResolvedValue(validQueryParameters);
      storeQueryGetUsers = jest.spyOn(store.queries, 'getUsers')
        .mockRejectedValue(expectedError);

      return new Promise((resolve, reject) => {
        // Invoke the function to be tested.
        getUsers(<ModifiedRequest>request, <Response>response, (err: Error|string) => {
          try {
            // Set the expectations.
            expect(validateQueryParametersMock).toHaveBeenCalledWith(request.query);
            expect(convertQueryParamsToDatabaseParamsMock).toHaveBeenCalledWith(validQueryParameters);
            expect(storeQueryGetUsers).toHaveBeenCalledWith(validQueryParameters);
            expect(response?.locals?.status).toEqual(StatusCodes.INTERNAL_SERVER_ERROR);
            expect(response?.locals?.body).toEqual(getApiErrorBodyFromApiError(new ApiError(E_GENERAL_SERVER_ERROR, [], expectedError.message)));
            expect(err).toBeUndefined();

            // Ensure that the Promise is resolved if all the expectations are successful.
            return resolve(null);
          }
          catch (e) {
            return reject(e);
          }
        });
      });
    });
  });
});
