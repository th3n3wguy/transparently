// Import 3rd-party libraries.
import { StatusCodes } from 'http-status-codes';
import { NextFunction, Response } from 'express';

// Import local files.
import { appLogger } from '../../../../config/logger';
import { ModifiedRequest } from '../../../initialize-request-context';
import store from '../../../../store';
import DuplicateUsernameError from '../../../../store/exceptions/DuplicateUsernameError';
import { getApiErrorBody } from '../../helpers/get-api-error-body-from-api-error';
import {
  E_BLANK_USERNAME,
  E_GENERAL_SERVER_ERROR,
  E_DUPLICATE_USERNAME
} from '../../helpers/error-codes';
import UsernameIsBlankError from '../../../../store/exceptions/UsernameIsBlankError';
import validateUserParams from './validate-user-params';

export default function createUser(req: ModifiedRequest, res: Response, next: NextFunction): void {
  // TODO: Figure out the bug that allows a user to be created with a blank username, emailAddress, and fullName.

  validateUserParams(req.body)
    .then(() => store.commands.createUser({
      username: req.body.username,
      email_address: req.body.emailAddress,
      full_name: req.body.fullName
    }))
    .then(() => store.queries.getUserByUsername(req.body.username))
    .then(({
      user_id,
      created_on,
      last_updated_on,
      username,
      email_address,
      full_name
    }) => {
      res.locals.status = StatusCodes.CREATED;
      res.locals.body = {
        userId: user_id,
        createdOn: created_on,
        lastUpdatedOn: last_updated_on,
        username,
        emailAddress: email_address,
        fullName: full_name
      };

      return next();
    })
    .catch((err: Error|DuplicateUsernameError|UsernameIsBlankError) => {
      appLogger.error('There was an error when attempting to create the User.');
      appLogger.debug(err);

      if (err instanceof DuplicateUsernameError) {
        res.locals.status = StatusCodes.UNPROCESSABLE_ENTITY;
        res.locals.body = getApiErrorBody(E_DUPLICATE_USERNAME, [req.body.username]);
      }
      else if (err instanceof UsernameIsBlankError) {
        res.locals.status = StatusCodes.UNPROCESSABLE_ENTITY;
        res.locals.body = getApiErrorBody(E_BLANK_USERNAME, [req.body.username]);
      }
      else {
        res.locals.status = StatusCodes.INTERNAL_SERVER_ERROR;
        res.locals.body = getApiErrorBody(E_GENERAL_SERVER_ERROR, []);
      }

      return next();
    });
}
