// Import 3rd-party libraries.
import { noop as _noop } from 'lodash';

// Import local files.
import validateUserParams from './validate-user-params';

describe('/api/v0/handlers/create-user/validate-user-params', () => {
  test('should be a Function that returns a Promise', () => {
    expect(validateUserParams).toEqual(expect.any(Function));
    expect(validateUserParams({}).catch(_noop)).toEqual(expect.any(Promise));
  });

  // Need to make sure "" is not valid.
  test.todo('when the username is valid');

  test.todo('when the username is invalid');

  test.todo('when the emailAddress is valid');

  test.todo('when the emailAddress is invalid');

  test.todo('when the fullName is valid');

  test.todo('when the fullName is invalid');
});
