// Import 3rd-party libraries.
import {
  isEmpty as _isEmpty
} from 'lodash';

// Import local files.
import { User } from '../../types/user';
import UnprocessableEntityError from '../../exceptions/UnprocessableEntityError';
import {
  E_BLANK_EMAIL_ADDRESS,
  E_BLANK_FULL_NAME,
  E_BLANK_USERNAME
} from '../../helpers/error-codes';

export default function validateUserParams({ username, emailAddress, fullName }: Partial<User.ResponseObject>): Promise<void> {
  return new Promise((resolve, reject) => {
    if (_isEmpty(username)) {
      return reject(new UnprocessableEntityError(E_BLANK_USERNAME, [username], `The "username" parameter provided (${username}) was an empty string, null, or undefined.`));
    }
    else if (_isEmpty(emailAddress)) {
      return reject(new UnprocessableEntityError(E_BLANK_EMAIL_ADDRESS, [emailAddress], `The "emailAddress" parameter provided (${emailAddress}) was an empty string, null, or undefined.`));
    }
    else if (_isEmpty(fullName)) {
      return reject(new UnprocessableEntityError(E_BLANK_FULL_NAME, [fullName], `The "fullName" parameter provided (${fullName}) was an empty string, null, or undefined.`));
    }
    else {
      return resolve();
    }
  });
}
