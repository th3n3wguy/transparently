// Set up the mocks to be imported.
// jest.mock('../../../../store');

// Import 3rd-party libraries.

// Import local files.
import createUser from './';

describe('/api/v0/handlers/create-user', () => {
  test('should be a Function', () => {
    expect(createUser).toEqual(expect.any(Function));
  });
});
