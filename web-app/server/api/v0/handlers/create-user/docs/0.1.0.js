/**
 * @api {post} /api/v0/users Create a User
 * @apiGroup Users
 * @apiName CreateUser
 * @apiVersion 0.1.0
 * @apiDescription Create a User within the system.
 *
 * @apiParam {String{..255}} username The username that will be created for the user. This must be unique, as usernames are unique within the system and what is used to authenticate against the system.
 * @apiParam {String{..5000}} emailAddress The email address associated to the User to be created.
 * @apiParam {String{..5000}} fullName The name associated to the User to be created. This is generally the full name of a person in other applications (not the username).
 *
 * @apiSuccess (201 - Created) {object} user
 * @apiSuccess (201 - Created) {String{..32}} user.userId The unique identifier that is created by the system and associated to the User's account. While this is technically the string-representation of a ULID, this might not be the case in the future, so do not rely upon this assumption to be true.
 * @apiSuccess (201 - Created) {String} user.username The unique username value associated to and managed by the User.
 * @apiSuccess (201 - Created) {String{..5000}} user.emailAddress The email address associated to and managed by the User.
 * @apiSuccess (201 - Created) {String{..5000}} user.fullName The name associated to the User. This is what will be displayed in many places in the application.
 * @apiSuccess (201 - Created) {String} user.createdOn The timestamp (ISO8601-formatted string) associated to when the User was created.
 * @apiSuccess (201 - Created) {String} user.lastUpdatedOn The timestamp (ISO8601-formatted string) associated to when the User was last updated.
 *
 * @apiUse UnprocessableEntityError
 * @apiUse InternalServerError
 */
