

describe('/api/v0/handlers/create-work-item', () => {
  test.todo('should be a "void" return value');

  describe('happy paths', () => {
    test.todo('when the work item is created successfully');
  });

  describe('negative testing paths', () => {
    test.todo('when the "store" returns an InvalidWorkItemTypeError');

    test.todo('when the "store" returns a NameTooLongError');

    test.todo('when the "store" returns an InvalidNameError');

    test.todo('when the "store" returns a general Error');
  });
});
