/**
 * @api {post} /api/v0/users Create Work Item
 * @apiGroup Work Items
 * @apiName CreateWorkItem
 * @apiVersion 0.1.0
 * @apiDescription Create a Work Item within the system.
 *
 * TODO: Need to add the request body below.
 * @apiParam {String} name The short-name value that was provided when the Work Item was created.
 * @apiParam {String} A Markdown-formatted string that provides a full list of details which was provided when the Work Item was created.
 * @apiParam {String} typeSlug The "slug" that references a specific Work Item Type value. This is a unique string and is HTML/URL-safe.
 *
 * TODO: Need to add the response object below.
 * @apiSuccess (200 - OK) {Object} workItem
 * @apiSuccess (200 - OK) {String} workItem.workItemId The unique identifier (ULID) that is created by the system and associated to the Work Item.
 *
 * @apiUse UnprocessableEntityError
 * @apiUse InternalServerError
 */
