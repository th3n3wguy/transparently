// Import 3rd-party libraries.
import {
  NextFunction,
  Response
} from 'express';
import { StatusCodes } from 'http-status-codes';

// Import local files.
import { ModifiedRequest } from '../../../initialize-request-context';
import store from '../../../../store';

export default function createWorkItem(req: ModifiedRequest, res: Response, next: NextFunction): void {
  // TODO: Run a validation where it returns a 400 if the request body contains other properties other than the ones expected.
  store.commands.createWorkItem({ name: req.body.name, description: req.body.description }, req.body.typeSlug)
    .then((workItemId) => {
      res.locals.status = StatusCodes.CREATED;
      res.locals.body = {
        workItemId
      };

      return next();
    })
    .catch(next);
}
