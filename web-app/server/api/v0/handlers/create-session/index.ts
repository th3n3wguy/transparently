// Import 3rd-party libraries.
import {
  NextFunction,
  Response
} from 'express';

// Import local files.
import { ModifiedRequest } from '../../../initialize-request-context';
import store from '../../../../store';
import { StatusCodes } from 'http-status-codes';

export default function createUser(req: ModifiedRequest, res: Response, next: NextFunction): void {
  const { username } = req.body;

  store.queries.getUserByUsername(username)
    .then((user) => {
      req.login({ id: user.user_pk }, function (err) {
        if (err) {
          return next(err);
        }
        else {
          res.locals.status = StatusCodes.CREATED;
          res.locals.body = null;

          return next();
        }
      });
    });
}
