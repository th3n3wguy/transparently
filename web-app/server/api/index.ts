// Import 3rd party libraries.
import {
  ErrorRequestHandler,
  RequestHandler,
  Router as expressRouter
} from 'express';

// Import route handlers.
import v0Routes from './v0';
import v0SetResponse from './v0/helpers/set-response';
import v0SetErrorResponse from './v0/helpers/set-error-response';

// Define the router that will be used to set up these sub-routes.
const router: expressRouter = expressRouter({ mergeParams: true });

// Define the sub-route paths.
router.use('/v0', v0Routes, <RequestHandler>v0SetResponse, <ErrorRequestHandler>v0SetErrorResponse);

export default router;
