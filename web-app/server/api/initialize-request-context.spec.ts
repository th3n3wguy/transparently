// Import 3rd-party libraries.
import { validate as validateUUID } from 'uuid';
import SpyInstance = jest.SpyInstance;
import { Response } from 'express';

// Import local files.
import { default as initializeRequestContext, ModifiedRequest } from './initialize-request-context';
import { appLogger } from '../config/logger';
import request from '../tests/mocks/request';
import response from '../tests/mocks/response';

describe('API - Initialize Request Context', () => {
  test('should be a Function', () => {
    expect(initializeRequestContext).toEqual(expect.any(Function));
  });

  describe('when invoked', () => {
    let appLoggerDebug: SpyInstance;
    let appLoggerAddContext: SpyInstance;

    beforeEach(() => {
      appLoggerDebug = jest.spyOn(appLogger, 'debug');
      appLoggerAddContext = jest.spyOn(appLogger, 'addContext');
    });

    afterEach(() => {
      jest.clearAllMocks();
    });

    test('should invoke without throwing an error', () => {
      return new Promise((resolve) => {
        initializeRequestContext(<ModifiedRequest>request, <Response>response, () => {
          expect(true).toEqual(true);

          return resolve(null);
        });
      });
    });

    test('should add the .requestId property to the request object', () => {
      return new Promise((resolve) => {
        initializeRequestContext(<ModifiedRequest>request, <Response>response, () => {
          expect(validateUUID(<string>request.requestId)).toBeTruthy();

          return resolve(null);
        });
      });
    });

    test('should add the .requestId property to the context of the logger and log the Session as a "debug" entry', () => {
      return new Promise((resolve) => {
        initializeRequestContext(<ModifiedRequest>request, <Response>response, () => {
          // Set the expectations used within this test.
          expect(appLoggerAddContext).toHaveBeenCalledWith('requestId', request.requestId);
          expect(appLoggerDebug).toHaveBeenCalledWith(expect.stringContaining('Session ID:'));

          return resolve(null);
        });
      });
    });

    test('should return an Error when one is triggered in the "try...catch" block', () => {
      // Set up the constants / spies / mocks used within this test.
      const expectedErr: Error = new Error('Some failure occurred.');

      appLoggerAddContext.mockImplementation(() => {
        throw expectedErr;
      });

      return new Promise((resolve) => {
        initializeRequestContext(<ModifiedRequest>request, <Response>response, (err: Error|string) => {
          // Set the expectations used within this test.
          expect(appLoggerAddContext).toHaveBeenCalledTimes(1);
          expect(err).toEqual(expectedErr);

          return resolve(null);
        });
      });
    });
  });
});
