// Require 3rd party libraries.
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import compression from 'compression';
import {
  default as express,
  ErrorRequestHandler,
  RequestHandler
} from 'express';
import { Express } from 'express-serve-static-core';
import expressSession from 'express-session';
import helmet from 'helmet';
import http from 'http';
import {
  keys as _keys,
  invoke as _invoke,
  isEqual as _isEqual,
  noop as _noop,
  size as _size
} from 'lodash';
import morgan from 'morgan';
import * as sourcemaps from 'source-map-support';
import { config } from 'dotenv';
import {
  destroy as destroyUnleashConnection,
  initialize as initializeUnleash
} from 'unleash-client';

// Require the .env file for various configuration pieces and whether to generate source maps.
!_isEqual(process.env.NODE_ENV, 'production') ? config() : _noop();
!_isEqual(process.env.NODE_ENV, 'production') ? sourcemaps.install() : _noop();

// Require the functions necessary for the configurations.
import sessionConfig from './config/session';
import { morganFormat, morganOptions } from './config/morgan';
import initializeRequestContext from './api/initialize-request-context';
import { log4js, appLogger } from './config/logger';
import passport from './config/passport';

// Import the Express route handlers.
import apiRoutes from './api';
import routeNotFound from './api/errors/route-not-found';
import routeErrorHandler from './api/errors/route-error-handler';

// Create the Express application that will host the REST API and the WebSocket Server.
const app: Express = express();
const {
  PORT = 8081,
  UNLEASH_URL = '',
  UNLEASH_APP_NAME = '',
  UNLEASH_INSTANCE_ID = ''
} = process.env;

// Use the helmet library to ensure some basic security protocols.
app.use(helmet());

// Use the body-parser library for POST and GET request body values.
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Use the cookie-parser library.
app.use(cookieParser(sessionConfig.secret));

// Set up the session library.
app.use(expressSession(sessionConfig));
app.use(passport.initialize());
app.use(passport.session());

// Set up the route logging library.
app.use(morgan(morganFormat, morganOptions));

// Use the compression library for gzip compression of all HTTP assets.
app.use(compression());

// Custom middleware function that allows for us to bind the database connection and other functionality needed as necessary.
app.use(<RequestHandler>initializeRequestContext);

// Bind the basic route handlers for the REST API and static files.
app.use('/api', apiRoutes);
app.use('/docs/api', express.static('api_docs'));
app.use(<RequestHandler>routeNotFound);
app.use(<ErrorRequestHandler>routeErrorHandler);

// Create the servers to handle the incoming HTTP and WebSocket connections.
const httpServer = http.createServer(app);

// Run the latest DB migrations (if needed) and then start the server.
connectFeatureFlagStore()
  .then(startHttpServer)
  .catch(handleServerError);

function connectFeatureFlagStore() {
  return new Promise((resolve) => {
    const instance = initializeUnleash({
      url: `${UNLEASH_URL}/api/`,
      appName: UNLEASH_APP_NAME,
      instanceId: UNLEASH_INSTANCE_ID
    });

    instance.on('ready', () => resolve(null));
    instance.on('warn', appLogger.warn);
    instance.on('error', appLogger.error);
    instance.on('registered', (data) => appLogger.info(`Unleash: Registered Event - ${JSON.stringify(data)}`));
    instance.on('sent', (payload) => appLogger.debug(`Unleash: Metrics Bucket/Payload Sent Event - ${JSON.stringify(payload)}`));
    instance.on('count', (name, enabled) => appLogger.debug(`Unleash: Count Event - isEnabled(${name}) returned ${enabled}`));
    instance.on('changed', (data) => {
      const toggleNames = _keys(data);

      appLogger.debug(`Unleash: Changed Event - ${JSON.stringify(data)}`);

      for (let i = 0; i < _size(toggleNames); i++) {
        if (data[toggleNames[i]].stale) {
          appLogger.warn(`Unleash: Stale Feature Flag - ${JSON.stringify(data[toggleNames[i]])}`);
        }
      }
    });
  });
}

/**
 * @typedef {Function} WebApp.Server.Startup.StartHttpServer
 * @returns {Promise<void>} Returns a void Promise when resolved and an error within the rejected Promise when an error does occur.
 */
function startHttpServer() {
  return new Promise((resolve, reject) => {
    // Tell the HTTP server to start and listen on the configured (or default) port.
    appLogger.info('HTTP Server: Starting');

    // Start the HTTP server.
    httpServer.listen(PORT, () => {
      appLogger.info('HTTP Server: Started Successfully');
      appLogger.debug(`HTTP Server: Listening on ${PORT}.`);
    });

    httpServer.on('close', reject);
  });
}

/**
 * @typedef {Function} WebApp.Server.Startup.HandleServerError
 * @param {Error} err - The error that was thrown and caught to be handled.
 * @returns {Promise<void>} Returns a void Promise when resolved and an error within the rejected Promise when an error does occur.
 */
function handleServerError(err: Error) {
  // Log the error stack trace.
  appLogger.fatal(err.stack);

  // Shut down the logger (gracefully).
  appLogger.info('Shut Down: Attempting to gracefully stop the logger');
  _invoke(log4js, 'shutdown');

  // Shut down the feature flag (Unleash) system.
  destroyUnleashConnection();

  // Shut down the HTTP server (gracefully).
  appLogger.info('Shut Down: Attempting to gracefully stop the HTTP server');
  _invoke(httpServer, 'close');
}
