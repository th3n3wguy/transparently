// Import 3rd-party libraries.
import * as faker from 'faker';

// Import local files.
import { User } from '../../store/types/store-interfaces/users';
import { Ulid } from 'id128';

/**
 * Generate a new valid user that is returned from the store.
 *
 * @param {Partial<User.Core>} partialUser - The partial database User object that can be provided for some values.
 * @returns {User.Core} newUserFromStore
 */
export function getValidUserFromStore({
  username,
  created_on = faker.date.soon(),
  last_updated_on = faker.date.soon()
}: Partial<User.Core>): User.Core {
  return {
    user_pk: faker.datatype.number(),
    user_id: Ulid.generate().toRaw(),
    created_on: created_on,
    last_updated_on: last_updated_on,
    username: username || faker.internet.userName(),
    email_address: faker.internet.email(),
    full_name: faker.fake('{{name.firstName}} {{name.lastName}}')
  };
}
