// Import types.
import { Response } from 'express';

export default <Partial<Response>>{
  headersSent: false,
  json() {
    return this;
  },
  locals: {},
  status() {
    return this;
  }
};
