// Import local files.
import { GetWorkItemsParams } from './';
import { appLogger } from '../../../config/logger';
import db from '../../../config/database';
import DatabaseQueryError from '../../exceptions/DatabaseQueryError';
import { WorkItem as WorkItemFromStore } from '../../types/store-interfaces/work-item';
import { WorkItem as WorkItemFromDatabase } from '../../types/postgres/work-item';

export default function queryDatabase({
  limit,
  offset,
  sortDirection,
  sortKey
}: GetWorkItemsParams): Promise<WorkItemFromStore[]> {
  return new Promise((resolve, reject) => {
    db.from<WorkItemFromDatabase, WorkItemFromStore[]>('work_items')
      .select([
        'work_items.work_item_id',
        'work_items.created_on',
        'work_items.last_updated_on',
        'work_items.name',
        'work_items.description',
        'work_item_types.slug as type_slug'
      ])
      .leftOuterJoin('work_item_types', 'work_item_types.work_item_type_pk', 'work_items.work_item_type_fk')
      .limit(limit)
      .offset(offset)
      .orderBy(sortKey, sortDirection)
      .then((workItems: WorkItemFromStore[]) => {
        // Log the information to the application's log file for traceability.
        appLogger.info('Database Query: Success.');
        appLogger.debug(`Work Items: ${JSON.stringify(workItems)}`);

        // Return the list of Users.
        return resolve(workItems);
      })
      .catch((err: Error) => {
        appLogger.debug(JSON.stringify(err));
        appLogger.warn(err.stack);

        return reject(new DatabaseQueryError(err.message));
      });
  });
}
