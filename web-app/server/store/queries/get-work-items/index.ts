// Import local files.
import { appLogger } from '../../../config/logger';
import queryDatabase from './query-database';
import DatabaseQueryError from '../../exceptions/DatabaseQueryError';
import { WorkItem } from '../../types/store-interfaces/work-item';

export interface GetWorkItemsParams {
  limit: number;
  offset: number;
  sortDirection: 'ASC'|'asc'|'DESC'|'desc';
  sortKey: 'type_slug'|'created_on'|'last_updated_on';
}

export type GetWorkItems = (params: GetWorkItemsParams) => Promise<WorkItem[]>;

export default function getWorkItems(params: GetWorkItemsParams): Promise<WorkItem[]> {
  return new Promise((resolve, reject) => {
    appLogger.info('Attempting to retrieve the Work Items from the database.');
    appLogger.debug(JSON.stringify(params));

    queryDatabase(params)
      .then((workItems) => {
        appLogger.info('Successfully retrieved the Work Items from the database.');
        appLogger.debug(`Work Items: ${JSON.stringify(workItems)}`);

        return resolve(workItems);
      })
      .catch((err: Error|DatabaseQueryError) => {
        appLogger.warn(err.stack);

        return reject(err);
      });
  });
}
