// Import local files.
import { appLogger } from '../../../config/logger';
import queryDatabase from './query-database';
import { User } from '../../types/store-interfaces/users';
import UserNotFoundError from '../../exceptions/UserNotFoundError';
import DatabaseQueryError from '../../exceptions/DatabaseQueryError';
import getUserFromUsersList from './get-user-from-users-list';

export interface GetUserByUsername {
  (username: User.UserManagedProperties['username']): Promise<User.Core>
}

/**
 * Returns the specified User using the provided username, or rejects the Promise if there was an error when attempting to do so.
 *
 * @param {User.UserManagedProperties.username} username - The username by which to find the User within the store.
 * @returns {User.Core|undefined} The requested User, if found.
 */
export default function getUserByUsername(username: User.UserManagedProperties['username']): Promise<User.Core|undefined> {
  return new Promise((resolve, reject) => {
    appLogger.info('Attempting to get the User from the store using the provided "username" value.');
    appLogger.debug(`Username: ${username}`);

    queryDatabase(username)
      .then(getUserFromUsersList)
      .then((user) => {
        appLogger.info('Successfully retrieved the correct User from the database.');
        appLogger.debug(`User: ${JSON.stringify(user)}`);

        return resolve(user);
      })
      .catch((err: Error|DatabaseQueryError|UserNotFoundError) => {
        // Log the Error's stack to the application's log file.
        appLogger.warn(err.stack);

        // Return a new Error that can be handled properly at the API controller layer.
        return reject(err);
      });
  });
}
