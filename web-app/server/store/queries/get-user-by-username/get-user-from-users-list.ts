// Import 3rd-party libraries.
import {
  first as _first,
  isEmpty as _isEmpty,
  isEqual as _isEqual,
  size as _size
} from 'lodash';

// Import local files.
import { User } from '../../types/store-interfaces/users';
import UserNotFoundError from '../../exceptions/UserNotFoundError';
import { appLogger } from '../../../config/logger';

/**
 *
 * @param {User.Core[]} users - An array (possibly empty) of Users that are returned from the database.
 * @returns {User.Core} The User that should be returned back. This does reject the Promise in certain cases. See the code for the business logic.
 */
export default function getUserFromUsersList(users: User.Core[]): Promise<User.Core|undefined> {
  return new Promise((resolve, reject) => {
    // If the list of Users that comes back from the database is empty...
    if (_isEmpty(users)) {
      return reject(new UserNotFoundError('A User could not be found within the database using the provided "username" value.'));
    }
    // If the list of Users that comes back from the database includes more than a single User...
    else if (!_isEqual(_size(users), 1)) {
      appLogger.warn('Somehow, there are multiple users with the same username. This should not be allowed.');

      return reject(new Error('There is (somehow) more than 1 user with the same username. This should not be allowed, as there are consequences to this issue.'));
    }
    else {
      return resolve(_first(users));
    }
  });
}
