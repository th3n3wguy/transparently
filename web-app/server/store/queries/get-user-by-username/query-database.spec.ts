// Import 3rd-party libraries.
import SpyInstance = jest.SpyInstance;
import { assign as _assign } from 'lodash';

// Import local files.
import queryDatabase from './query-database';
import db from '../../../config/database';
import { User } from '../../types/store-interfaces/users';
import { getValidUserFromStore } from '../../../tests/mocks/user';
import { appLogger } from '../../../config/logger';
import { constraints20201231 } from '../../migrations/20201231194537_init-database';
import { PostgresError } from '../../types/postgres/database-errors';
import DatabaseQueryError from '../../exceptions/DatabaseQueryError';

describe('/store/queries/get-user-by-username/query-database', () => {
  test('should be a Function', () => {
    expect(queryDatabase).toEqual(expect.any(Function));
  });

  describe('when invoked', () => {
    const queryColumns: string[] = ['user_pk', 'user_id', 'username', 'email_address', 'full_name', 'created_on', 'last_updated_on'];
    let dbFromSpy: SpyInstance;
    let dbSelectSpy: SpyInstance;
    let dbWhereSpy: SpyInstance;
    let appLoggerDebug: SpyInstance;
    let appLoggerInfo: SpyInstance;
    let appLoggerWarn: SpyInstance;

    beforeEach(() => {
      dbFromSpy = jest.spyOn(db, 'from').mockReturnThis();
      dbSelectSpy = jest.spyOn(db, 'select').mockReturnThis();
      appLoggerDebug = jest.spyOn(appLogger, 'debug').mockImplementation();
      appLoggerInfo = jest.spyOn(appLogger, 'info').mockImplementation();
      appLoggerWarn = jest.spyOn(appLogger, 'warn').mockImplementation();
    });

    afterEach(() => {
      jest.clearAllMocks();
    });

    test('should return the list of User details back exactly as it is received by the database call', () => {
      // Set up the spies / mocks / variables used specifically within this test.
      const validDatabaseUsers: User.Core[] = [getValidUserFromStore({})];

      dbWhereSpy = jest.spyOn(db, 'where')
        .mockResolvedValue(validDatabaseUsers);

      return new Promise((resolve, reject) => {
        // Invoke the function to be tested.
        queryDatabase(validDatabaseUsers[0].username)
          .then((users: User.Core[]) => {
            // Set the expectations.
            expect(dbFromSpy).toHaveBeenCalledWith('users');
            expect(dbSelectSpy).toHaveBeenCalledWith(queryColumns);
            expect(dbWhereSpy).toHaveBeenCalledWith('username', validDatabaseUsers[0].username);
            expect(users).toEqual(validDatabaseUsers);
            expect(appLoggerInfo).toHaveBeenCalledWith('Database Query: Success.');
            expect(appLoggerDebug).toHaveBeenCalledWith(`Users: ${JSON.stringify(users)}`);
            expect(appLoggerWarn).toHaveBeenCalledTimes(0);

            // Ensure that the Promise is resolved if all of the expectations are successful.
            return resolve(null);
          })
          .catch(reject);
      });
    });

    test('should return a DatabaseQueryError with the original error message if there is a PostgresError in the rejected Promise', () => {
      // Set up the spies / mocks / variables used specifically within this test.
      const validDatabaseUsers: User.Core[] = [getValidUserFromStore({})];
      const postgresError: PostgresError = _assign(new Error('General PostgreSQL error.'), {
        code: '23505',
        constraint: constraints20201231.users.unique_username
      });

      dbWhereSpy = jest.spyOn(db, 'where')
        .mockRejectedValue(postgresError);

      return new Promise((resolve, reject) => {
        queryDatabase(validDatabaseUsers[0].username)
          .then(() => reject(new Error('This Promise function should not be returning successfully.')))
          .catch((err) => {
            // Set the expectations.
            expect(appLoggerWarn).toHaveBeenCalledWith(postgresError.stack);
            expect(appLoggerDebug).toHaveBeenCalledWith(JSON.stringify(postgresError));
            expect(err).toBeInstanceOf(DatabaseQueryError);
            expect(err.message).toEqual(postgresError.message);

            // Ensure that the Promise is resolved if all of the expectations are successful.
            return resolve(null);
          });
      });
    });

    test('should return a DatabaseQueryError with the original error message if there is an Error in the rejected Promise', () => {
      // Set up the spies / mocks / variables used specifically within this test.
      const validDatabaseUsers: User.Core[] = [getValidUserFromStore({})];
      const genericError: Error = new Error('Generic error message.');

      dbWhereSpy = jest.spyOn(db, 'where')
        .mockRejectedValue(genericError);

      return new Promise((resolve, reject) => {
        queryDatabase(validDatabaseUsers[0].username)
          .then(() => reject(new Error('This Promise function should not be returning successfully.')))
          .catch((err) => {
            // Set the expectations.
            expect(appLoggerWarn).toHaveBeenCalledWith(genericError.stack);
            expect(appLoggerDebug).toHaveBeenCalledWith(JSON.stringify(genericError));
            expect(err).toBeInstanceOf(DatabaseQueryError);
            expect(err.message).toEqual(genericError.message);

            // Ensure that the Promise is resolved if all of the expectations are successful.
            return resolve(null);
          });
      });
    });
  });
});
