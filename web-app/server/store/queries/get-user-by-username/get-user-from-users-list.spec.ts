// Import 3rd-party libraries.
import SpyInstance = jest.SpyInstance;

// Import local files.
import getUserFromUsersList from './get-user-from-users-list';
import { appLogger } from '../../../config/logger';
import UserNotFoundError from '../../exceptions/UserNotFoundError';
import { getValidUserFromStore } from '../../../tests/mocks/user';
import { User } from '../../types/store-interfaces/users';

describe('/store/queries/get-user-by-username/get-user-from-users-list', () => {
  test('should be a Function', () => {
    expect(getUserFromUsersList).toEqual(expect.any(Function));
  });

  describe('when invoked', () => {
    let appLoggerWarn: SpyInstance;

    beforeEach(() => {
      appLoggerWarn = jest.spyOn(appLogger, 'warn').mockImplementation();
    });

    afterEach(() => {
      jest.clearAllMocks();
    });

    test('when the list of Users provided to the function is empty', () => {
      // Create the list of Users to be used in this test.
      const emptyUserList: User.Core[] = [];

      return new Promise((resolve, reject) => {
        // Execute the function to be tested.
        getUserFromUsersList(emptyUserList)
          .then(() => reject(new Error('This promise should not have resolved successfully.')))
          .catch((err) => {
            // Set the expectations.
            expect(err).toEqual(new UserNotFoundError('A User could not be found within the database using the provided "username" value.'));
            expect(appLoggerWarn).toHaveBeenCalledTimes(0);

            // Ensure that the Promise is resolved if all of the expectations are successful.
            return resolve(null);
          });
      });
    });

    test('when the list of Users provided to the function has more than 1 user', () => {
      // Create the list of Users to be used in this test.
      const users: User.Core[] = [getValidUserFromStore({}), getValidUserFromStore({})];

      return new Promise((resolve, reject) => {
        // Execute the function to be tested.
        getUserFromUsersList(users)
          .then(() => reject(new Error('This promise should not have resolved successfully.')))
          .catch((err) => {
            // Set the expectations.
            expect(err).toEqual(new Error('There is (somehow) more than 1 user with the same username. This should not be allowed, as there are consequences to this issue.'));
            expect(appLoggerWarn).toHaveBeenCalledWith('Somehow, there are multiple users with the same username. This should not be allowed.');

            // Ensure that the Promise is resolved if all of the expectations are successful.
            return resolve(null);
          });
      });
    });

    test('when the list of Users provided to the function has only 1 user', () => {
      // Create the list of Users to be used in this test.
      const users: User.Core[] = [getValidUserFromStore({})];

      return new Promise((resolve, reject) => {
        // Execute the function to be tested.
        getUserFromUsersList(users)
          .then((user) => {
            // Set the expectations.
            expect(user).toEqual(users[0]);

            // Ensure that the Promise is resolved if all of the expectations are successful.
            return resolve(null);
          })
          .catch(reject);
      });
    });
  });
});
