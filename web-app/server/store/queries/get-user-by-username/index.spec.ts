jest.mock('./query-database');
jest.mock('./get-user-from-users-list');

// Import 3rd-party libraries.
import SpyInstance = jest.SpyInstance;
import Mock = jest.Mock;

// Import local files.
import getUserByUsername from './';
import queryDatabase from './query-database';
import getUserFromUsersList from './get-user-from-users-list';
import { User } from '../../types/store-interfaces/users';
import { getValidUserFromStore } from '../../../tests/mocks/user';
import { appLogger } from '../../../config/logger';

describe('/store/queries/get-user-by-username', () => {
  test('should be a Function', () => {
    expect(getUserByUsername).toEqual(expect.any(Function));
  });

  describe('when invoked', () => {
    const queryDatabaseMock: Mock = queryDatabase as Mock;
    const getUserFromUsersListMock: Mock = getUserFromUsersList as Mock;
    const initialInfoLogMessage: string = 'Attempting to get the User from the store using the provided "username" value.';
    let appLoggerDebug: SpyInstance;
    let appLoggerInfo: SpyInstance;
    let appLoggerWarn: SpyInstance;

    beforeEach(() => {
      appLoggerDebug = jest.spyOn(appLogger, 'debug').mockImplementation();
      appLoggerInfo = jest.spyOn(appLogger, 'info').mockImplementation();
      appLoggerWarn = jest.spyOn(appLogger, 'warn').mockImplementation();
    });

    afterEach(() => {
      jest.clearAllMocks();
    });

    test('should log data and resolve the Promise with a single user when the "queryDatabase" and "getUserFromUsersList" functions both resolve successfully', () => {
      // Create a set of users that will be used for this test.
      const mockedUsers = [getValidUserFromStore({}), getValidUserFromStore({})];

      // Set up the mocks / spies used in this test.
      queryDatabaseMock.mockResolvedValue(mockedUsers);
      getUserFromUsersListMock.mockResolvedValue(mockedUsers[0]);

      return new Promise((resolve, reject) => {
        // Execute the function to be tested.
        getUserByUsername(mockedUsers[0].username)
          .then((user) => {
            // Set the expectations.
            expect(appLoggerInfo).toHaveBeenCalledWith(initialInfoLogMessage);
            expect(appLoggerDebug).toHaveBeenCalledWith(`Username: ${mockedUsers[0].username}`);
            expect(queryDatabaseMock).toHaveBeenCalledWith(mockedUsers[0].username);
            expect(getUserFromUsersListMock).toHaveBeenCalledWith(mockedUsers);
            expect(appLoggerInfo).toHaveBeenCalledWith('Successfully retrieved the correct User from the database.');
            expect(appLoggerDebug).toHaveBeenCalledWith(`User: ${JSON.stringify(user)}`);
            expect(user).toEqual(mockedUsers[0]);

            // Ensure that the Promise is resolved if all of the expectations are successful.
            return resolve(null);
          })
          .catch(reject);
      });
    });

    test('should log the Error\'s stack and reject the Promise with the same Error when any Error is thrown', () => {
      // Create a set of users that will be used for this test.
      const mockedUsers: User.Core[] = [getValidUserFromStore({}), getValidUserFromStore({})];
      const thrownError: Error = new Error('Some random error thrown in the query.');

      // Set up the mocks / spies used in this test.
      queryDatabaseMock.mockRejectedValue(thrownError);
      getUserFromUsersListMock.mockResolvedValue(mockedUsers[0]);

      return new Promise((resolve, reject) => {
        // Execute the function to be tested.
        getUserByUsername(mockedUsers[0].username)
          .then(() => reject(new Error('The promise for this function should not have resolved.')))
          .catch((err) => {
            // Set the expectations.
            expect(appLoggerInfo).toHaveBeenCalledWith(initialInfoLogMessage);
            expect(appLoggerDebug).toHaveBeenCalledWith(`Username: ${mockedUsers[0].username}`);
            expect(appLoggerWarn).toHaveBeenCalledWith(err.stack);
            expect(err).toEqual(thrownError);

            // Ensure that the Promise is resolved if all of the expectations are successful.
            return resolve(null);
          });
      });
    });
  });
});
