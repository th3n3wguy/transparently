import { User } from '../../types/store-interfaces/users';

jest.mock('./query-database');

// Import 3rd-party libraries.
import Mock = jest.Mock;
import SpyInstance = jest.SpyInstance;

// Import local files.
import { default as getUsers, GetUsersQueryParams } from './';
import queryDatabase from './query-database';
import { appLogger } from '../../../config/logger';
import { getValidUserFromStore } from '../../../tests/mocks/user';
import DatabaseQueryError from '../../exceptions/DatabaseQueryError';

describe('/store/queries/get-users', () => {
  test('should be a Function', () => {
    expect(getUsers).toEqual(expect.any(Function));
  });

  describe('when invoked', () => {
    const queryDatabaseMock: Mock = queryDatabase as Mock;
    const queryParams: GetUsersQueryParams = {
      limit: 50,
      offset: 0,
      sortDirection: 'ASC',
      sortKey: 'username'
    };
    let appLoggerDebug: SpyInstance;
    let appLoggerInfo: SpyInstance;
    let appLoggerWarn: SpyInstance;

    beforeEach(() => {
      appLoggerDebug = jest.spyOn(appLogger, 'debug').mockImplementation();
      appLoggerInfo = jest.spyOn(appLogger, 'info').mockImplementation();
      appLoggerWarn = jest.spyOn(appLogger, 'warn').mockImplementation();
    });

    afterEach(() => {
      jest.clearAllMocks();
    });

    test('return the exact same list of Users that is provided by the database query when no errors occur', () => {
      // Define the fake data used within this test.
      const dbUsers: User.Core[] = [getValidUserFromStore({}), getValidUserFromStore({})];

      // Set up the spies and mocks used within this test.
      queryDatabaseMock.mockResolvedValue(dbUsers);

      return new Promise((resolve, reject) => {
        // Execute the function to be tested.
        getUsers(queryParams)
          .then((users: User.Core[]) => {
            // Set the expectations.
            expect(appLoggerInfo).toHaveBeenCalledWith('Attempting to retrieve the Users from the database.');
            expect(appLoggerDebug).toHaveBeenCalledWith(JSON.stringify(queryParams));
            expect(queryDatabaseMock).toHaveBeenCalledWith(queryParams);
            expect(appLoggerInfo).toHaveBeenCalledWith('Successfully retrieved the Users from the database.');
            expect(appLoggerDebug).toHaveBeenCalledWith(`Users: ${JSON.stringify(dbUsers)}`);
            expect(users).toEqual(dbUsers);

            // Ensure that the Promise is resolved if all of the expectations are successful.
            return resolve(null);
          })
          .catch(reject);
      });
    });

    test.each(
      [
        [new DatabaseQueryError('There was an error when querying the database.')],
        [new Error('Some random error that occurred when querying the database.')]
      ]
    )('return the same error in the rejected Promise that is caught within the function', (expectedError) => {
      // Set up the spies and mocks used within this test.
      queryDatabaseMock.mockRejectedValue(expectedError);

      return new Promise((resolve, reject) => {
        // Execute the function to be tested.
        getUsers(queryParams)
          .then(() => reject(new Error('This Promise should not have resolved.')))
          .catch((err) => {
            // Set the expectations.
            expect(appLoggerInfo).toHaveBeenCalledWith('Attempting to retrieve the Users from the database.');
            expect(appLoggerDebug).toHaveBeenCalledWith(JSON.stringify(queryParams));
            expect(queryDatabaseMock).toHaveBeenCalledWith(queryParams);
            expect(appLoggerWarn).toHaveBeenCalledWith(expectedError.stack);
            expect(err).toEqual(expectedError);

            // Ensure that the Promise is resolved if all of the expectations are successful.
            return resolve(null);
          });
      });
    });
  });
});
