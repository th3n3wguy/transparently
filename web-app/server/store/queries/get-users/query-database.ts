// Import types.
import { User } from '../../types/store-interfaces/users';
import { GetUsersQueryParams } from './';

// Import local files.
import { appLogger } from '../../../config/logger';
import db from '../../../config/database';
import DatabaseQueryError from '../../exceptions/DatabaseQueryError';

export default function queryDatabase({
  limit,
  offset,
  sortDirection,
  sortKey
}: GetUsersQueryParams): Promise<User.Core[]> {
  return new Promise((resolve, reject) => {
    db.from<User.Core, User.Core[]>('users')
      .select([
        'user_pk',
        'user_id',
        'created_on',
        'last_updated_on',
        'username',
        'email_address',
        'full_name'
      ])
      .limit(limit)
      .offset(offset)
      .orderBy(sortKey, sortDirection)
      .then((users) => {
        // Log the information to the application's log file for traceability.
        appLogger.info('Database Query: Success.');
        appLogger.debug(`Users: ${JSON.stringify(users)}`);

        // Return the list of Users.
        return resolve(users);
      })
      .catch((err: Error) => {
        appLogger.debug(JSON.stringify(err));
        appLogger.warn(err.stack);

        return reject(new DatabaseQueryError(err.message));
      });
  });
}
