// Import 3rd-party libraries.
import {
  first as _first,
  isEmpty as _isEmpty
} from 'lodash';

// Import local files.
import { User } from '../../types/store-interfaces/users';
import UserNotFoundError from '../../exceptions/UserNotFoundError';
import { appLogger } from '../../../config/logger';

/**
 * Retrieves the User object from the returned query or rejects with a UserNotFoundError error when none can be found.
 *
 * @param {User.Auth[]|undefined} users - An array (possibly empty) of Users that are returned from the database.
 * @returns {User.Auth} The User that should be returned. This does reject the Promise in certain cases. See the code for the business logic.
 */
export default function getUserFromUsersList(users: User.Auth[]|undefined): Promise<User.Auth|undefined> {
  return new Promise((resolve, reject) => {
    // If the list of Users that comes back from the database is empty...
    if (_isEmpty(users)) {
      appLogger.info('A User could not be found within the database.');

      return reject(new UserNotFoundError('A User could not be found within the database.'));
    }
    else {
      return resolve(_first(users));
    }
  });
}
