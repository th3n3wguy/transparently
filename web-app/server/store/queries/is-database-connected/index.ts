// Import local files.
import { appLogger } from '../../../config/logger';
import db from '../../../config/database';
import DatabaseQueryError from '../../exceptions/DatabaseQueryError';

export interface IsDatabaseConnected {
  (): Promise<void>;
}

/**
 * Returns whether the database is currently connected or not.
 *
 * @returns {Promise<void>} Returns a resolved Promise if the database is connected.
 */
export default function isDatabaseConnected(): Promise<void> {
  return new Promise((resolve, reject) => {
    appLogger.info('Checking if the database is connected.');

    db.raw('SELECT 1')
      .then(() => resolve())
      .catch((err: Error) => {
        appLogger.warn(err.stack);
        appLogger.debug(JSON.stringify(err));

        return reject(new DatabaseQueryError('The query to check whether the database is connected failed. The database might have disconnected.'));
      });
  });
}
