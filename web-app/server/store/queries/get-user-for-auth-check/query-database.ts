// Import local files.
import { User } from '../../types/store-interfaces/users';
import { appLogger } from '../../../config/logger';
import db from '../../../config/database';
import DatabaseQueryError from '../../exceptions/DatabaseQueryError';
import { PostgresError } from '../../types/postgres/database-errors';

/**
 * Queries the database and returns a list of users based upon the primary key (this should only return a single record or an empty record list).
 *
 * @param {User.ServerManagedProperties.username} username - The username by which to find the User within the store.
 * @returns {User.Auth[]|undefined} A list of Users.
 */
export default function queryDatabase(username: User.Auth['username']): Promise<User.Auth[]|undefined> {
  return new Promise((resolve, reject) => {
    db.from<User.Auth, User.Auth[]>('users')
      .select([
        'user_pk',
        'user_id',
        'username',
        'email_address',
        'full_name',
        'created_on',
        'last_updated_on',
        'password_hash'
      ])
      .where('username', username)
      .then((users) => {
        // Log the information to the application's log file for traceability.
        appLogger.info('Database Query: Success.');
        appLogger.debug(`Users: ${JSON.stringify(users)}`);

        // Return the list of Users.
        return resolve(users);
      })
      .catch((err: Error | PostgresError) => {
        appLogger.warn(err.stack);
        appLogger.debug(JSON.stringify(err));

        return reject(new DatabaseQueryError(err.message));
      });
  });
}
