

describe('/store/queries/get-work-item-types', () => {
  test.todo('should always return a Promise');

  describe('happy paths', () => {
    test.todo('the list of Work Item Types is successfully retrieved from the database');

    test.todo('an empty list of Work Item Types is successfully retrieved from the database');
  });

  describe('negative testing paths', () => {
    // This is going to require a lot of test.each() parameters to test all kinds of different invalid values.
    test.todo('the "limit" parameter is invalid');

    // This is going to require a lot of test.each() parameters to test all kinds of different invalid values.
    test.todo('the "offset" parameter is invalid');

    // This is going to require a lot of test.each() parameters to test all kinds of different invalid values.
    test.todo('the "sortDirection" parameter is invalid');

    // This is going to require a lot of test.each() parameters to test all kinds of different invalid values.
    test.todo('the "sortKey" parameter is invalid');

    test.todo('an unknown Error is thrown by the database');
  });
});
