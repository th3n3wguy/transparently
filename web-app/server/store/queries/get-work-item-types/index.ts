// Import local files.
import { appLogger } from '../../../config/logger';
import { WorkItemType } from '../../types/postgres/work-item-type';
import db from '../../../config/database';
import { PostgresError } from '../../types/postgres/database-errors';
import DatabaseQueryError from '../../exceptions/DatabaseQueryError';

export interface GetWorkItemsParams {
  limit: number;
  offset: number;
  sortDirection: 'ASC'|'asc'|'DESC'|'desc';
  sortKey: 'created_on'|'last_updated_on'|'slug'|'name';
}

export type GetWorkItemTypes = (queryParams: GetWorkItemsParams) => Promise<WorkItemType[]>;

export default function getWorkItemTypes({ limit, offset, sortDirection, sortKey }: GetWorkItemsParams): Promise<WorkItemType[]> {
  return new Promise((resolve, reject) => {
    appLogger.info('Attempting to retrieve the list of Work Item Types from the database.');
    appLogger.debug(`limit=${limit},offset=${offset},sortDirection=${sortDirection},sortKey=${sortKey}`);

    db.from<WorkItemType, (WorkItemType & { parent_slug: WorkItemType['slug'] })[]>('work_item_types')
      .select([
        'work_item_types.slug',
        'work_item_types.created_on',
        'work_item_types.last_updated_on',
        'work_item_types.name',
        'work_item_types.description',
        'parent_work_item_types.slug as parent_slug'
      ])
      .leftOuterJoin('work_item_types as parent_work_item_types', 'parent_work_item_types.work_item_type_pk', 'work_item_types.parent_type_fk')
      .limit(limit)
      .offset(offset)
      .orderBy(sortKey, sortDirection)
      .then((workItemTypes) => {
        appLogger.debug(`workItemTypes=${JSON.stringify(workItemTypes)}`);

        return resolve(workItemTypes);
      })
      .catch((err: Error|PostgresError) => {
        appLogger.warn(err.stack);
        appLogger.debug(JSON.stringify(err));

        return reject(new DatabaseQueryError(err.message));
      });
  });
}
