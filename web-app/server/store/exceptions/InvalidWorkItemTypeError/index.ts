/**
 * Error class that is specific for when the "work_items_work_item_type_fk_foreign" unique constraint is violated on the PostgreSQL database.
 *
 * @class InvalidWorkItemTypeError
 * @extends Error
 */
export default class InvalidWorkItemTypeError extends Error {
  constructor(message: string) {
    super(message);
    Error.captureStackTrace(this, InvalidWorkItemTypeError);
  }
}
