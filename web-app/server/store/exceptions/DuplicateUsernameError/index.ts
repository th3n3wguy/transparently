/**
 * Error class that is specific for when the "users_username_unique" unique constraint is violated on the PostgreSQL database within the store.
 *
 * @class DuplicateUsernameError
 * @extends Error
 */
export default class DuplicateUsernameError extends Error {
  constructor(message: string) {
    super(message);
    Error.captureStackTrace(this, DuplicateUsernameError);
  }
}
