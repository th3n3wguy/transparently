/**
 * Error class that is specific for when the "name" violates a length constraint on the database table.
 *
 * @class NameTooLongError
 * @extends Error
 */
export default class NameTooLongError extends Error {
  constructor(message: string) {
    super(message);
    Error.captureStackTrace(this, NameTooLongError);
  }
}
