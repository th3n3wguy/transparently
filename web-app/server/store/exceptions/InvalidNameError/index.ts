/**
 * Error class that is specific for when the "name" contains unsupported characters.
 *
 * @class InvalidNameError
 * @extends Error
 */
export default class InvalidNameError extends Error {
  constructor(message: string) {
    super(message);
    Error.captureStackTrace(this, InvalidNameError);
  }
}
