/**
 * Error class that describes when the username provided is blank and should not be so.
 *
 * @class UsernameIsBlankError
 * @extends Error
 */
export default class UsernameIsBlankError extends Error {
  constructor(message: string) {
    super(message);
    Error.captureStackTrace(this, UsernameIsBlankError);
  }
}
