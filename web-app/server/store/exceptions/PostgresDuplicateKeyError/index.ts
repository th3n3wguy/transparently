/**
 * Error class that is specific for when Postgres returns back that one of the values provided in the query violated a unique constraint. This is generally associated
 *   with code #23505 (https://www.postgresql.org/docs/current/errcodes-appendix.html).
 *
 * @class PostgresDuplicateKeyError
 * @extends Error
 */
export default class PostgresDuplicateKeyError extends Error {
  constructor(message: string) {
    super(message);
    Error.captureStackTrace(this, PostgresDuplicateKeyError);
  }
}
