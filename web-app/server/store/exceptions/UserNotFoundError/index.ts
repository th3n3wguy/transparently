/**
 * Error class that is used to denote when a User is not found within the store.
 *
 * @class UserNotFoundError
 * @extends Error
 */
export default class UserNotFoundError extends Error {
  constructor(message: string) {
    super(message);
    Error.captureStackTrace(this, UserNotFoundError);
  }
}
