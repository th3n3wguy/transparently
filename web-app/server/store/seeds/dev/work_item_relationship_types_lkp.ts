// Import 3rd-party libraries.
import { Knex } from 'knex';
import { WorkItemRelationshipType } from '../../types/postgres/work-item-relationship-type';

const workItemRelationshipTypesTableName: string = 'work_item_relationship_types_lkp';

exports.seed = function seedDatabase(knex: Knex) {
  const addDependsOn = () => knex.from(workItemRelationshipTypesTableName)
    .insert({
      slug: 'depends_on',
      name: 'depends on',
      description: 'This is used to state where the "primary" work item has a general dependency on the "secondary" work item.'
    })
    .onConflict('slug')
    .merge();
  const addStartDependsOnCompletionOf = () => knex.from<WorkItemRelationshipType, undefined>(workItemRelationshipTypesTableName)
    .insert({
      slug: 'unable_to_start_until_completion_of',
      name: 'unable to start until completion of',
      description: 'This is used to link where the "primary" work item requires that the "secondary" work item is "completed" / "done" before the "primary" can be started.'
    })
    .onConflict('slug')
    .merge();

  return addDependsOn()
    .then(addStartDependsOnCompletionOf);
};
