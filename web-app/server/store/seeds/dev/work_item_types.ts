// Import 3rd-party libraries.
import { Knex } from 'knex';
import {
  isUndefined as _isUndefined,
  merge as _merge
} from 'lodash';

// Import local files.
import { WorkItemType } from '../../types/postgres/work-item-type';

const workItemTypeTableName: string = 'work_item_types';

exports.seed = function seedDatabase(knex: Knex) {
  const addTheme = generateWorkItemTypeFunctionHandler({
    slug: 'theme',
    name: 'Theme',
    description: 'This is the highest-level Work Item available within the system. This is usually used for executive-level planning.'
  });
  const addEpic = generateWorkItemTypeFunctionHandler({
    slug: 'epic',
    name: 'Epic',
    description: 'This Work Item is usually reserved for Portfolio-level planning (borrowing the term from Scaled Agile Framework).'
  });
  const addCapability = generateWorkItemTypeFunctionHandler({
    slug: 'capability',
    name: 'Capability',
    description: 'This Work Item is usually reserved for Value-Stream-level planning (borrowing the term from the Scaled Agile Framework).'
  });
  const addFeature = generateWorkItemTypeFunctionHandler({
    slug: 'feature',
    name: 'Feature',
    description: 'This Work Item is usually reserved for Program-level planning (borrowing the term from the Scaled Agile Framework).'
  });
  const addUserStory = generateWorkItemTypeFunctionHandler({
    slug: 'user_story',
    name: 'User Story',
    description: 'The User Story is the Work Item Type usually used within a single team to identify and track specific work.'
  });
  const addDefect = generateWorkItemTypeFunctionHandler({
    slug: 'defect',
    name: 'Defect',
    description: 'A defect is a bug in the software and can be associated to work at any level in the hierarchy.'
  });
  const addTask = generateWorkItemTypeFunctionHandler({
    slug: 'task',
    name: 'Task',
    description: 'A task is the "lowest-grain" Work Item available. While it does not have a direct parent, these are usually reserved for team-level work.'
  });

  return addTheme([])
    .then(addEpic)
    .then(addCapability)
    .then(addFeature)
    .then(addUserStory)
    .then(() => addDefect([]))
    .then(() => addTask([]));

  function generateWorkItemTypeFunctionHandler(workItemType: Partial<WorkItemType>) {
    const now = new Date();

    return ([parentPk]: (WorkItemType['work_item_type_pk']|undefined)[]) => {
      return knex.from<Partial<WorkItemType>, (WorkItemType['work_item_type_pk']|undefined)[]>(workItemTypeTableName)
        .insert(_merge({}, workItemType, {
          created_on: now,
          last_updated_on: now,
          parent_type_fk: _isUndefined(parentPk) ? null : parentPk
        }))
        .returning('work_item_type_pk')
        .onConflict('slug')
        .merge();
    };
  }
};
