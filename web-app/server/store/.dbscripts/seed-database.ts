// Import 3rd-party libraries.
import { get as _get } from 'lodash';

// Import local files.
import { appLogger } from '../../config/logger';
import db from '../../config/database';
import knexConfig from '../../config/knexfile';

// Log a message that the seeds have begun.
appLogger.debug('Database Seeds: Starting');

db.seed.run(_get(knexConfig, `${process.env.NODE_ENV}.seeds`, knexConfig.production))
  .then(() => appLogger.info('Database Seeds: Completed Successfully'))
  .then(() => db.destroy())
  .catch((err: Error) => {
    // Log some information about the failure.
    appLogger.info('Database Seeds: Failed');
    appLogger.debug(JSON.stringify(err));
    appLogger.fatal(err.stack);

    return db.destroy();
  });
