// Import 3rd-party libraries.
import { get as _get } from 'lodash';

// Import local files.
import { appLogger } from '../../config/logger';
import db from '../../config/database';
import knexConfig from '../../config/knexfile';

// Log a message that the migrations have begun.
appLogger.debug(`Database Downgrade: Starting (for env ${process.env.NODE_ENV}`);

db.migrate.down(_get(knexConfig, `${process.env.NODE_ENV}.migrations`, knexConfig.production))
  .then(() => appLogger.info('Database Downgrade: Completed Successfully'))
  .then(() => db.destroy())
  .catch((err: Error) => {
    // Log a fatal message that the migrations failed.
    appLogger.info('Database Downgrade: Failed');
    appLogger.debug(JSON.stringify(err));
    appLogger.fatal(err.stack);

    return db.destroy();
  });
