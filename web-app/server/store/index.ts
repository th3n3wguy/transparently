// Import types.
import { Store } from './types/store';

// Import local files.
import createUser from './commands/create-user';
import createWorkItem from './commands/create-work-item';
import deleteUser from './commands/delete-user';
import getUserByPk from './queries/get-user-by-pk';
import getUserByUsername from './queries/get-user-by-username';
import getUserForAuthCheck from './queries/get-user-for-auth-check';
import getUsers from './queries/get-users';
import getWorkItems from './queries/get-work-items';
import getWorkItemTypes from './queries/get-work-item-types';
import isDatabaseConnected from './queries/is-database-connected';

export default <Store>{
  commands: {
    createUser,
    createWorkItem,
    deleteUser
  },
  queries: {
    getUserByPk,
    getUserByUsername,
    getUserForAuthCheck,
    getUsers,
    getWorkItems,
    getWorkItemTypes,
    isDatabaseConnected
  }
};
