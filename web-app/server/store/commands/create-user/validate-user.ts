// Import 3rd-party libraries.
import {
  isEmpty as _isEmpty,
  trim as _trim
} from 'lodash';

// Import local files.
import UsernameIsBlankError from '../../exceptions/UsernameIsBlankError';
import { User } from '../../types/store-interfaces/users';

/**
 * Used to validate that certain aspects of the user to be created can be caught that might not be caught using the database constraints.
 *
 * @param {User.UserManagedProperties} user The "user" to be created.
 * @returns {Promise<User.UserManagedProperties>} The "user" to be created.
 */
export default function validateUser(user: User.UserManagedProperties): Promise<User.UserManagedProperties> {
  return new Promise((resolve, reject) => {
    if (_isEmpty(_trim(user.username))) {
      return reject(new UsernameIsBlankError(`The provided username (${user.username} was an empty string.`));
    }

    return resolve(user);
  });
}
