// Import types.
import { User } from '../../types/store-interfaces/users';
import { PostgresError } from '../../types/postgres/database-errors';

// Import local files.
import { appLogger } from '../../../config/logger';
import createUserInDatabase from './create-user-in-database';
import DuplicateUsernameError from '../../exceptions/DuplicateUsernameError';
import validateUser from './validate-user';

/**
 * The type definition for the "createUser" command within the store.
 */
export interface CreateUser {
  (user: User.UserManagedProperties): Promise<void>
}

/**
 * Create a User within the store.
 *
 * @param {User.UserManagedProperties} user - The properties associated
 * @returns {Promise<void>} The User created within the store.
 */
export default function createUser(user: User.UserManagedProperties): Promise<void> {
  return new Promise((resolve, reject) => {
    appLogger.info('Attempting to create the User within the database.');
    appLogger.debug(`User: ${JSON.stringify(user)}`);

    validateUser(user)
      .then(createUserInDatabase)
      .then((user) => {
        appLogger.info('User created successfully within the database.');
        appLogger.debug(`User: ${JSON.stringify(user)}`);

        return resolve();
      })
      .catch((err: Error | PostgresError | DuplicateUsernameError) => {
        appLogger.info('Failed to create the User within the database.');

        return reject(err);
      });
  });
}
