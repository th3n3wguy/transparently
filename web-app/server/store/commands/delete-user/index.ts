// Import 3rd-party libraries.
import { isEqual as _isEqual } from 'lodash';

// Import local files.
import { appLogger } from '../../../config/logger';
import db from '../../../config/database';
import { User } from '../../types/store-interfaces/users';
import DatabaseQueryError from '../../exceptions/DatabaseQueryError';
import UserNotFoundError from '../../exceptions/UserNotFoundError';

/**
 * The type definition for the "createUser" command within the store.
 */
export interface DeleteUser {
  (userId: User.ServerManagedProperties['user_id']): Promise<void>
}

/**
 * Delete a User from the store.
 *
 * @param {User.ServerManagedProperties.user_id} user_id - The unique identifier associated to the User to be deleted.
 * @returns {Promise<void>} Returns a resolved Promise with a void value when successful.
 */
export default function deleteUser(user_id: User.ServerManagedProperties['user_id']): Promise<void> {
  appLogger.info('Attempting to delete the User with the provided userId value.');
  appLogger.debug(`userId: ${user_id}`);

  return new Promise((resolve, reject) => {
    db.from<User.Core, undefined>('users')
      .where('user_id', user_id)
      .del()
      .then((numberOfDeletedRecords) => {
        appLogger.info(`Deleted User from the database: ${_isEqual(numberOfDeletedRecords, 1)}`);

        return _isEqual(numberOfDeletedRecords, 1)
          ? resolve()
          : reject(new UserNotFoundError('A User with the provided "userId" was not found in the database.'));
      })
      .catch((err: Error) => {
        // TODO: Need to figure out what kind of PostgreSQL errors can / will be returned from this so we can handle them properly.
        appLogger.warn(err.stack);
        appLogger.debug(JSON.stringify(err));

        return reject(new DatabaseQueryError(err.message));
      });
  });
}
