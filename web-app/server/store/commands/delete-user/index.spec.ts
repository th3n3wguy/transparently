describe('/store/commands/delete-user', () => {
  test.todo('should return a resolved Promise with a "void" value when the user is successfully deleted from the database');

  test.todo('should return a rejected Promise with a DatabaseQueryError that contains the original error\'s message');
});
