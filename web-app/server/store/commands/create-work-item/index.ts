// Import local files.
import {
  UserManagedProperties,
  WorkItem
} from '../../types/postgres/work-item';
import insertIntoDatabase from './insert-into-database';
import { WorkItemType } from '../../types/postgres/work-item-type';


/**
 * The type definition for the "createWorkItem" command within the store.
 */
export interface CreateWorkItem {
  (workItem: UserManagedProperties, workItemTypeFk: WorkItem['work_item_type_fk']): Promise<WorkItem['work_item_id']>
}

/**
 * Create a Work Item within the store.
 *
 * @param {UserManagedProperties} workItem - The user-provided data points to create the Work Item.
 * @param {WorkItemType.slug} workItemTypeSlug - The foreign-key value that points to the Work Item Type to associate to this Work Item.
 * @returns {Promise<void>} Resolved Promise with the unique identifier associated to the Work Item created within the store.
 */
export default function createWorkItem(workItem: UserManagedProperties, workItemTypeSlug: WorkItemType['slug']): Promise<WorkItem['work_item_id']> {
  return new Promise((resolve, reject) => {
    // TODO: Validate the workItem properties that are being provided to ensure that they meet the guidelines above the database definitions.
    insertIntoDatabase(workItem, workItemTypeSlug)
      .then(resolve)
      .catch(reject);
  });
}
