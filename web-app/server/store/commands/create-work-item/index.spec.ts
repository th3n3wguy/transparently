

describe('/store/commands/create-work-item', () => {
  test.todo('should always return a Promise');

  describe('success paths', () => {
    test.todo('should return a ULID when the Work Item is created successfully in the store');
  });

  describe('negative testing paths', () => {
    test.todo('when the "name" provided is too long');

    test.todo('when the "name" provided contains unsupported characters');

    test.todo('when the "description" provided contains unsupported characters');

    test.todo('when the "work_item_type" "slug" is not found in the database');

    test.todo('when the "work_item_type" "slug" contains unsupported characters');

    test.todo('when there is an unknown error in the database');
  });
});
