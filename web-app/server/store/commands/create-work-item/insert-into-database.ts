// Import 3rd-party libraries.
import { Ulid } from 'id128';
import {
  get as _get,
  isEqual as _isEqual,
  isNil as _isNil,
  merge as _merge
} from 'lodash';

// Import local files.
import {
  UserManagedProperties,
  WorkItem
} from '../../types/postgres/work-item';
import { WorkItemType } from '../../types/postgres/work-item-type';
import { appLogger } from '../../../config/logger';
import db from '../../../config/database';
import { PostgresError } from '../../types/postgres/database-errors';
import InvalidWorkItemTypeError from '../../exceptions/InvalidWorkItemTypeError';
import { Knex } from 'knex';
import Transaction = Knex.Transaction;

export default function insertIntoDatabase(providedWorkItem: UserManagedProperties, workItemTypeSlug: WorkItemType['slug']): Promise<WorkItem['work_item_id']> {
  return new Promise((resolve, reject) => {
    const workItemToBeAdded: Partial<WorkItem> = _merge({}, providedWorkItem, getDefaultValues());

    appLogger.debug(`workItemToBeAdded=${JSON.stringify(workItemToBeAdded)},workItemTypeSlug=${workItemTypeSlug}`);

    db.transaction<WorkItem['work_item_id']>(handleTransaction(workItemTypeSlug, workItemToBeAdded))
      .then(resolve)
      .catch(reject);
  });
}

function handleTransaction(workItemTypeSlug: WorkItemType['slug'], workItemToBeAdded: Partial<WorkItem>): (trx: Transaction) => Promise<WorkItem['work_item_id']> {
  return (trx: Transaction) => getWorkItemTypePkFromDatabaseUsingWorkItemTypeSlug(trx, workItemTypeSlug)
    .then(insertWorkItemIntoDatabase(trx, workItemToBeAdded));
}

/**
 * Creates the constants that are used to create the proper data within the database when creating the User.
 *
 * @returns {{ created_on: WorkItem.created_on, last_updated_on: WorkItem.last_updated_on, work_item_id: WorkItem.work_item_id }} The constants that can be deconstructed for easier access / reasoning in the above function.
 */
function getDefaultValues(): Pick<WorkItem, 'created_on' | 'last_updated_on' | 'work_item_id'> {
  const now = new Date();

  return {
    created_on: now,
    last_updated_on: now,
    work_item_id: Ulid.generate({ time: now }).toRaw()
  };
}

/**
 * Transaction-alized function to retrieve the "work_item_type_pk" value based on the provided "slug".
 *
 * @param {Transaction} trx - The Knex.js Transaction object.
 * @param {WorkItemType.slug} workItemTypeSlug - The "slug" that is used to reference a specific Work Item Type outside of the database.
 * @returns {Promise<WorkItemType.work_item_type_pk>} - Returns a resolved Promise with the "work_item_type_pk" value when successful and an InvalidWorkItemTypeError when the Work Item Type slug is not found or is invalid.
 */
function getWorkItemTypePkFromDatabaseUsingWorkItemTypeSlug(trx: Transaction, workItemTypeSlug: WorkItemType['slug']): Promise<WorkItemType['work_item_type_pk']> {
  return new Promise((resolve, reject) => {
    trx.from<WorkItemType, WorkItemType[]>('work_item_types')
      .select(['work_item_type_pk'])
      .where('slug', workItemTypeSlug)
      .then(([workItemType]) => _isNil(workItemType)
        ? reject(new InvalidWorkItemTypeError(`The provided "slug" (${workItemTypeSlug}) is not a valid Work Item Type.`))
        : resolve(workItemType.work_item_type_pk))
      .catch(reject);
  });
}

/**
 * Transaction-alized function to add the Work Item into the database.
 *
 * @param {Transaction} trx - The Knex.js Transaction object.
 * @param {Partial<WorkItem>} workItem - The Work Item to be inserted into the database.
 * @returns {Function} - Returns a Function that, when executed, will return a Promise with ID of the Work Item that was created.
 */
function insertWorkItemIntoDatabase(trx: Transaction, workItem: Partial<WorkItem>): (workItemTypePk: WorkItemType['work_item_type_pk']) => Promise<WorkItem['work_item_id']> {
  return (workItemTypePk) => new Promise((resolve, reject) => {
    trx.from<WorkItem, WorkItem['work_item_id']>('work_items')
      .insert(_merge({}, workItem, { work_item_type_fk: workItemTypePk }))
      .returning('work_item_id')
      .then(([workItemId]) => resolve(workItemId))
      .catch((err: Error | PostgresError) => {
        appLogger.warn(err.stack);
        appLogger.debug(JSON.stringify(err));

        // TODO: Handle the case where the "name" provided is too long.
        return isWorkItemTypeForeignKeyConstraintViolated(err)
          ? new InvalidWorkItemTypeError(_get(err, 'detail', err.message))
          : reject(err);
      });
  });
}

/**
 * Determines whether the error was violating the foreign key constraint for the Work Item Type.
 *
 * @param {Error|PostgresError} err - The error returned back from the database.
 * @returns {boolean} Specifies whether the Work Item Type foreign key constraint was violated or not.
 */
function isWorkItemTypeForeignKeyConstraintViolated(err: Error | PostgresError): boolean {
  return _isEqual(_get(err, 'code'), '23503')
    && _isEqual(_get(err, 'constraint'), 'work_items_work_item_type_fk_foreign');
}
