/**
 * The properties that are associated to the Work Item Type on the store, but are managed by the server.
 */
export interface ServerManagedProperties {
  // This is actually defined as a BigInt in the database, but because JavaScript can't handle 64-bit
  //   integers at the moment, we want to make sure to represent it as a string to keep the precision.
  work_item_type_pk: string;
  created_on: NonNullable<Date>;
  last_updated_on: NonNullable<Date>;
  parent_type_fk: ServerManagedProperties['work_item_type_pk'] | null;
}

/**
 * The properties that are associated to the Work Item Type on the store and are NOT managed by the server.
 */
export interface UserManagedProperties {
  slug: NonNullable<string>;
  name: NonNullable<string>;
  description: NonNullable<string>;
  parent_slug: string | null;
}

export type WorkItemType = ServerManagedProperties & UserManagedProperties;
