/**
 * The properties that are associated to the Work Item Type on the store, but are managed by the server.
 */
export interface ServerManagedProperties {
  work_item_relationship_type_lkp_pk: number;
}

/**
 * The properties that are associated to the Work Item Type on the store and are NOT managed by the server.
 */
export interface UserManagedProperties {
  slug: string;
  name: string;
  description: string;
}

export type WorkItemRelationshipType = ServerManagedProperties & UserManagedProperties;
