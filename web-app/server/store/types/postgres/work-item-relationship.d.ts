// Import local files.
import { WorkItem } from './work-item';
import { WorkItemRelationshipType } from './work-item-relationship-type';

/**
 * The properties that are associated to the Work Item on the store, but are managed by the server.
 */
export interface ServerManagedProperties {
  work_item_relationship_pk: number;
  created_on: Date;
  last_updated_on: Date;
  // This value is stored as a string in the database, but can be easily converted to a ULID using the `id128` library.
  work_item_relationship_id: string;
}

/**
 * The properties that are associated to the Work Item on the store and are NOT managed by the server.
 */
export interface UserManagedProperties {
  primary_work_item_fk: WorkItem['work_item_pk'];
  secondary_work_item_fk: WorkItem['work_item_pk'];
  description: string;
  work_item_relationship_type_fk: WorkItemRelationshipType['work_item_relationship_type_lkp_pk'];
}

export type WorkItemRelationship = ServerManagedProperties & UserManagedProperties;
