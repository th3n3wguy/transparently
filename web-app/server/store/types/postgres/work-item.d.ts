// Import local files.
import { WorkItemType } from './work-item-type';

/**
 * The properties that are associated to the Work Item on the store, but are managed by the server.
 */
export interface ServerManagedProperties {
  work_item_pk: number;
  created_on: Date;
  last_updated_on: Date;
  // This value is stored as a string in the database, but can be easily converted to a ULID using the `id128` library.
  work_item_id: string;
  work_item_type_fk: WorkItemType['work_item_type_pk'];
}

/**
 * The properties that are associated to the Work Item on the store and are NOT managed by the server.
 */
export interface UserManagedProperties {
  name: string;
  description: string;
}

export type WorkItem = ServerManagedProperties & UserManagedProperties;
