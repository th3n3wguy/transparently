export interface PostgresError extends Error {
  length?: number;
  name?: string;
  severity?: string;
  // https://www.postgresql.org/docs/13/errcodes-appendix.html
  code?: string;
  detail?: string;
  schema?: string;
  table?: string;
  constraint?: string;
  file?: string;
  line?: string;
  routine?: string;
}
