// Import local files.
import { WorkItemType } from '../postgres/work-item-type';
import { WorkItem as WorkItemInDatabase } from '../postgres/work-item';

/**
 * The properties that are associated to the Work Item on the store, but are managed by the server.
 */
export interface ServerManagedProperties {
  // This value is stored as a string in the database, but can be easily converted to a ULID using the `id128` library.
  work_item_id: WorkItemInDatabase['work_item_id'];
  created_on: Date;
  last_updated_on: Date;
}

/**
 * The properties that are associated to the Work Item on the store and are NOT managed by the server.
 */
export interface UserManagedProperties {
  name: string;
  description: string;
  type_slug: WorkItemType['slug'];
}

export type WorkItem = ServerManagedProperties & UserManagedProperties;
