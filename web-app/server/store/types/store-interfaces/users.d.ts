export namespace User {
  /**
   * The properties that are associated to the User type on the store, but are managed by the server.
   */
  export interface ServerManagedProperties {
    user_pk: number;
    // This value is stored as a string in the database, but can be easily converted to a ULID using the `id128` library.
    user_id: string;
    created_on: Date;
    last_updated_on: Date;
  }

  /**
   * The properties that are associated to the User type on the store, but are NOT managed by the server.
   */
  export interface UserManagedProperties {
    username: string;
    email_address: string;
    full_name: string;
  }

  /**
   * The combination of all properties that are managed by the server / database, as well as the properties managed by the User. This type excludes the password details and is
   *   intended for use anywhere the User might be utilized outside the API.
   */
  export type Core = ServerManagedProperties & UserManagedProperties;

  /**
   * The combination of all properties that are managed by the server / database, as well as the properties managed by the User, including the "password_hash" value. This should
   *   NEVER be used when returning data back outside the API.
   */
  export type Auth = ServerManagedProperties & UserManagedProperties & { password_hash: string; };
}
