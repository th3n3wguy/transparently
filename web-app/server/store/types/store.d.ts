// Import local files.
import { CreateUser } from '../commands/create-user';
import { CreateWorkItem } from '../commands/create-work-item';
import { DeleteUser } from '../commands/delete-user';
import { GetUsers } from '../queries/get-users';
import { GetUserByPk } from '../queries/get-user-by-pk';
import { GetUserByUsername } from '../queries/get-user-by-username';
import { GetUserForAuthCheck } from '../queries/get-user-for-auth-check';
import { GetWorkItems } from '../queries/get-work-items';
import { GetWorkItemTypes } from '../queries/get-work-item-types';
import { IsDatabaseConnected } from '../queries/is-database-connected';

/**
 * The "Commands" to allow for data to be inserted, updated, or deleted from the store.
 */
export interface Commands {
  createUser: CreateUser;
  createWorkItem: CreateWorkItem;
  deleteUser: DeleteUser;
}

/**
 * The "Queries" (see CQRS) that allow for data to be retrieved from the store.
 */
export interface Queries {
  getUserByPk: GetUserByPk;
  getUserByUsername: GetUserByUsername;
  getUserForAuthCheck: GetUserForAuthCheck;
  getUsers: GetUsers;
  getWorkItems: GetWorkItems;
  getWorkItemTypes: GetWorkItemTypes;
  isDatabaseConnected: IsDatabaseConnected
}

export interface Store {
  commands: Commands;
  queries: Queries;
}


