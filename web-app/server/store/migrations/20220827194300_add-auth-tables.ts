// Import 3rd-party libraries.
import { Knex } from 'knex';

exports.up = function upgradeDatabase(knex: Knex) {
  const alterUsersTable = () => knex.schema.alterTable('users', (tb) => {
    tb.string('password_hash', 255)
      .notNullable()
      .defaultTo('')
      .comment('The password for the Authentication that is used for the sign-in functionality.');
  });

  return alterUsersTable();
};

exports.down = function downgradeDatabase(knex: Knex) {
  const dropAuthTable = () => knex.schema.alterTable('users', (tb) => {
    tb.dropColumn('password_hash');
  });

  return dropAuthTable();
};
