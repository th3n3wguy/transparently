// Import 3rd-party libraries.
import { Knex } from 'knex';

export const constraints20210929 = {
  workItems: {
    index_workItemId: 'idx___work_items___work_item_id',
    index_createdOn: 'idx___work_items___created_on',
    index_lastUpdatedOn: 'idx___work_items___last_updated_on'
  },
  workItemRelationships: {
    index_workItemRelationshipId: 'idx___work_item_relationships___work_item_relationship_id',
    index_primaryAndSecondaryFks: 'idx___work_item_relationships___primary_work_item_fk__secondary_work_item_fk'
  },
  workItemRelationshipTypes: {
    index_slug: 'idx___work_item_relationship_type___slug',
    index_name: 'idx___work_item_relationship_type___name'
  },
  workItemTypes: {
    index_slug: 'idx___work_item_type___slug',
    index_name: 'idx___work_item_type___name'
  }
};

exports.up = function upgradeDatabase(knex: Knex) {
  const createWorkItemTypesTable = () => knex.schema.createTable('work_item_types', (tb) => {
    tb.bigIncrements('work_item_type_pk', { primaryKey: true })
      .comment('A unique identifier for the Work Item Type that is used internally to perform joins within the database for better performance than joining on the "slug" column.');
    tb.timestamp('created_on', { useTz: true, precision: 6 })
      .notNullable()
      .comment('The date and time as to when the Work Item Type was created.');
    tb.timestamp('last_updated_on', { useTz: true, precision: 6 })
      .notNullable()
      .comment('The date and time as to when the Work Item Type was last updated.');
    tb.bigInteger('parent_type_fk')
      .nullable()
      .comment('The reference to the "parent" type, which allows for a hierarchy of work item types to be defined by the system.')
      .references('work_item_type_pk')
      .inTable('work_item_types');
    tb.string('slug', 50)
      .unique(constraints20210929.workItemTypes.index_slug)
      .notNullable()
      .comment('The unique identifier for the Work Item Type that is used within URLs or external systems to refer to the Work Item Type.');
    tb.string('name', 255)
      .unique(constraints20210929.workItemTypes.index_name)
      .notNullable()
      .comment('The name associated to the Work Item Type that is usually displayed within the UI of external systems.');
    tb.text('description')
      .notNullable()
      .comment('The detailed description of the Work Item Type, usually in Markdown-formatted text, to provide additional context and details.');
  });
  const createWorkItemsTable = () => knex.schema.createTable('work_items', (tb) => {
    tb.bigIncrements('work_item_pk', { primaryKey: true })
      .comment('A unique identifier for the Work Item that is used internally to perform joins within the database for better performance than joining on the "work_item_id" column.');
    tb.timestamp('created_on', { useTz: true, precision: 6 })
      .notNullable()
      .comment('The date and time as to when the Work Item was created.');
    tb.timestamp('last_updated_on', { useTz: true, precision: 6 })
      .notNullable()
      .comment('The date and time as to when the Work Item was last updated.');
    tb.string('work_item_id', 32)
      .unique(constraints20210929.workItems.index_workItemId)
      .notNullable()
      .comment('The unique identifier for the Work Item that is used for displaying an ID to other systems, specified using the ULID spec.');
    tb.string('name', 255)
      .notNullable()
      .comment('The name associated to the Work Item.');
    tb.text('description')
      .notNullable()
      .comment('The detailed description associated to the Work Item that will contain Markdown-formatted text.');
    tb.bigInteger('work_item_type_fk')
      .notNullable()
      .comment('The foreign-key reference to the "work_item_type_pk" column in the "work_item_types" table, which identifies the Work Item Type for this Work Item.')
      .references('work_item_type_pk')
      .inTable('work_item_types');
  });
  const createWorkItemRelationshipTypesTable = () => knex.schema.createTable('work_item_relationship_types_lkp', (tb) => {
    tb.bigIncrements('work_item_relationship_type_lkp_pk', { primaryKey: true })
      .comment('The unique identifier for the Work Item Relationship Type.');
    tb.string('slug', 50)
      .unique(constraints20210929.workItemRelationshipTypes.index_slug)
      .notNullable()
      .comment('This is a "slug" value that can be used in URLs to retrieve the specific Work Item Relationship Type from the system without exposing the auto-generated ID.');
    tb.string('name', 255)
      .unique(constraints20210929.workItemRelationshipTypes.index_name)
      .notNullable()
      .comment('The name of the Work Item Relationship Type that usually displays within the UI in drop-down / combo-boxes.');
    tb.text('description')
      .notNullable()
      .comment('The full detailed description of the Work Item Relationship Type.');
  });
  const createWorkItemRelationshipsTable = () => knex.schema.createTable('work_item_relationships', (tb) => {
    tb.bigIncrements('work_item_relationship_pk', { primaryKey: true })
      .comment('The unique identifier for the Work Item Relationship.');
    tb.timestamp('created_on', { useTz: true, precision: 6 })
      .notNullable()
      .comment('The date and time as to when the Work Item Relationship was created in the system.');
    tb.timestamp('last_updated_on', { useTz: true, precision: 6 })
      .notNullable()
      .comment('The date and time as to when the Work Item Relationship was last updated in the system.');
    tb.string('work_item_relationship_id', 32)
      .index(constraints20210929.workItemRelationships.index_workItemRelationshipId)
      .notNullable()
      .comment('The unique identifier for the Work Item Relationship, which is used for displaying an ID to other systems and specified using the ULID spec.');
    tb.bigInteger('primary_work_item_fk')
      .notNullable()
      .comment('The foreign-key reference to the "work_item_pk" column in the "work_items" table, which references the "primary" Work Item in the relationship.')
      .references('work_item_pk')
      .inTable('work_items');
    tb.bigInteger('secondary_work_item_fk')
      .notNullable()
      .comment('The foreign-key reference to the "work_item_pk" column in the "work_items" table, which references the "secondary" Work Item in the relationship.')
      .references('work_item_pk')
      .inTable('work_items');
    tb.bigInteger('work_item_relationship_type_fk')
      .notNullable()
      .comment('The foreign-key reference to the "work_item_relationship_type_id" column in the "work_item_relationship_types_lkp" table, which references the type of relationship between the two Work Items.')
      .references('work_item_relationship_type_lkp_pk')
      .inTable('work_item_relationship_types_lkp');
    tb.text('description')
      .nullable()
      .comment('An external-system-provided value that is used to define some custom context around the relationship between the two Work Items.');
    tb.index(['primary_work_item_fk', 'secondary_work_item_fk'], constraints20210929.workItemRelationships.index_primaryAndSecondaryFks);
  });

  return createWorkItemTypesTable()
    .then(createWorkItemsTable)
    .then(createWorkItemRelationshipTypesTable)
    .then(createWorkItemRelationshipsTable);
};

exports.down = function downgradeDatabase(knex: Knex) {
  const dropWorkItemTypesTable = () => knex.schema.dropTable('work_item_types');
  const dropWorkItemsTable = () => knex.schema.dropTable('work_items');
  const dropWorkItemRelationshipsTable = () => knex.schema.dropTable('work_item_relationships');
  const dropWorkItemRelationshipTypesTable = () => knex.schema.dropTable('work_item_relationship_types_lkp');

  return dropWorkItemRelationshipsTable()
    .then(dropWorkItemRelationshipTypesTable)
    .then(dropWorkItemsTable)
    .then(dropWorkItemTypesTable);
};
