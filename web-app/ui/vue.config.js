// Require 3rd-party libraries.
const { isEqual: _isEqual } = require('lodash');
const WorkerPlugin = require('worker-plugin');
const path = require('path');

module.exports = {
  publicPath: '/',
  productionSourceMap: _isEqual(process.env.NODE_ENV, 'production'),
  lintOnSave: false,
  devServer: {
    clientLogLevel: 'trace',
    disableHostCheck: true,
    overlay: {
      errors: true
    },
    proxy: {
      '/api': {
        target: process.env.VUE_APP_SVC_APP_URL,
        ws: false,
        changeOrigin: true,
        logLevel: 'debug',
        proxyTimeout: 100000,
        timeout: 1000000,
        bypass: function(req) {
          req.headers['origin'] = process.env.VUE_APP_SVC_APP_URL;
          req.headers['host'] = process.env.VUE_APP_SVC_APP_URL;
          req.headers['referrer'] = process.env.VUE_APP_SVC_APP_URL;
        },
        onError: function(err) {
          console.error('webpack dev server proxy error:', err);
        },
        onClose: function() {
          console.log('Client disconnected!');
        }
      }
    }
  },
  transpileDependencies: ['vuetify'],
  configureWebpack: {
    devtool: _isEqual(process.env.NODE_ENV, 'production') ? '#source-map' : '#eval-source-map',
    plugins: [
      new WorkerPlugin({
        globalObject: 'self'
      })
    ],
    resolve: {
      extensions: ['.ts', '.js', '.vue', '.json'],
      alias: {
        'vue$': 'vue/dist/vue.esm.js',
        '@config': path.resolve(__dirname, 'src/config'),
        '@components': path.resolve(__dirname, 'src/components'),
        '@helpers': path.resolve(__dirname, 'src/helpers'),
        '@mocks': path.resolve(__dirname, 'tests/mocks'),
        '@routes': path.resolve(__dirname, 'src/router/routes'),
        '@services': path.resolve(__dirname, 'src/services'),
        '@src': path.resolve(__dirname, 'src'),
        '@store': path.resolve(__dirname, 'src/store'),
        '@tests': path.resolve(__dirname, 'tests'),
        '@views': path.resolve(__dirname, 'src/views')
      }
    }
  },
  pwa: {
    workboxPluginMode: 'InjectManifest',
    workboxOptions: {
      swSrc: 'public/service-worker.js',
      importWorkboxFrom: 'local'
    },
    name: 'Transparently',
    manifestOptions: {
      name: 'Transparently',
      short_name: 'Transparently',
      start_url: './index.html',
      display: 'standalone',
      theme_color: '#0D47A1'
    },
    iconPaths: {
      appleTouchIcon: 'img/icons/apple-touch-icon-152x152.png',
      favicon16: 'img/icons/favicon-16x16.png',
      favicon32: 'img/icons/favicon-32x32.png',
      maskIcon: 'img/icons/safari-pinned-tab.svg',
      msTileImage: 'img/icons/msapplication-icon-144x144.png'
    }

  }
};
