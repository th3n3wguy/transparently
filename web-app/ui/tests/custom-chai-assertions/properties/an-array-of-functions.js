// Import 3rd party libraries.
import {
  isFunction as _isFunction,
  reduce as _reduce
} from 'lodash-es';
import {
  Assertion,
  util
} from 'chai';


/**
 * This function adds a custom validator that adds the ".to.be.anArrayOfFunctions" property that determines if the value is an array of functions.
 *
 * @typedef {Function} CustomChaiAssertions.AnArrayOfFunctions
 * @returns {void}
 */
export default function addChaiPropertyAnArrayOfFunctions() {
  const isArrayOfFunctions = _reduce(util.flag(this, 'object'), (current, next) => current && _isFunction(next), true);

  new Assertion(isArrayOfFunctions).to.be.true;
};
