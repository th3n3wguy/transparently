// Import 3rd party libraries (that won't be exported).
import Vue from 'vue';
import {
  createLocalVue,
  config,
  mount,
  shallowMount
} from '@vue/test-utils';
import Vuex from 'vuex';
import Vuetify from 'vuetify/lib';
import chai from 'chai';
import sinonChai from 'sinon-chai';

// Import local plugin setup files.
import vuetifyConfig from '@config/vuetify';
import { anArrayOfFunctions } from './custom-chai-assertions';

// Export the things we want to be available with every test.
export { expect } from 'chai';
export { default as sinon } from 'sinon';
export function createTestInstance(component, isShallowMount = true, vuexModules = {}, additionalOptions = {}) {
  // Create the local Vue.js instance for testing purposes.
  const localVue = createLocalVue();
  const vuetify = new Vuetify(vuetifyConfig);

  // Set up the test plugins.
  localVue.use(Vuex);
  Vue.use(Vuetify); // FIXME: https://github.com/vuetifyjs/vuetify/issues/4068#issuecomment-586829171

  // Set up any custom Chai assertions.
  chai.use(sinonChai);
  chai.util.addProperty(chai.Assertion.prototype, 'anArrayOfFunctions', anArrayOfFunctions);

  // Create the store using the modules that were provided.
  const store = new Vuex.Store({ modules: vuexModules });

  // Set the configurations that will be used across all tests.
  config.silent = true;

  // Return the Vue.js instance that was created so it can be used in the tests.
  return isShallowMount
    ? shallowMount(component, { localVue, vuetify, store, ...additionalOptions })
    : mount(component, { localVue, vuetify, store, ...additionalOptions });
}


