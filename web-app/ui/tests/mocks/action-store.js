import sinon from 'sinon';

export default {
  commit: sinon.fake(),
  dispatch: sinon.fake.returns(Promise.resolve()),
  getters: {}
};
