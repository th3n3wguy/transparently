// Import 3rd-party libraries.
import { RouteConfig } from 'vue-router';

// Import local files.
import adminRoute from './admin';
import home from './home';
import notFound from './not-found';
import workItems from './work-items';

export default <RouteConfig[]>[
  home,
  adminRoute,
  notFound,
  workItems,
  {
    path: '*',
    redirect: notFound.path
  }
];
