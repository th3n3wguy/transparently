// Import 3rd-party libraries.
import { RouteConfigSingleView } from 'vue-router/types/router';

// Import application components.
import notFound from '@views/not-found/not-found.vue';

export default <RouteConfigSingleView>{
  path: '/not-found',
  name: notFound.name,
  component: notFound,
  meta: {
    title: 'Page Not Found (404)'
  }
};
