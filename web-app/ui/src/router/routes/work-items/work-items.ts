// Import 3rd-party libraries.
import { RouteConfigSingleView } from 'vue-router/types/router';

export default <RouteConfigSingleView>{
  path: '',
  name: 'work-items',
  component: () => import('@views/work-items/work-items.vue'),
  meta: {
    title: 'Work Items',
    icon: 'mdi-format-list-bulleted'
  },
  children: []
};
