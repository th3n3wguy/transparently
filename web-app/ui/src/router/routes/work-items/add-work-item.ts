// Import 3rd-party libraries.
import { RouteConfigSingleView } from 'vue-router/types/router';

export default <RouteConfigSingleView>{
  path: 'add',
  name: 'add-work-item',
  component: () => import('@views/add-work-item/add-work-item.vue'),
  meta: {
    title: 'Add Work Item',
    icon: 'briefcase'
  },
  children: []
};
