// Import 3rd-party libraries.
import { RouteConfigSingleView } from 'vue-router/types/router';

// Import local files.
import addWorkItemRoute from './add-work-item';
import workItemsRoute from './work-items';

export default <RouteConfigSingleView>{
  path: '/work-items',
  name: 'work-items-base',
  // component: () => import('@views/work-items/work-items.vue'),
  component: {
    template: '<router-view :key="$route.fullPath" />'
  },
  children: [
    addWorkItemRoute,
    workItemsRoute
  ]
};
