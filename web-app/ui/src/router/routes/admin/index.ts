// Import 3rd-party libraries.
import { RouteConfigSingleView } from 'vue-router/types/router';

// Import local files
import addUser from './add-user';
import users from './users';

export default <RouteConfigSingleView>{
  path: '/admin',
  name: 'admin',
  component: {
    template: '<router-view :key="$route.fullPath" />'
  },
  children: [
    addUser,
    users
  ]
};
