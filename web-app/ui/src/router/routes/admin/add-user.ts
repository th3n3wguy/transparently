// Import 3rd-party libraries.
import { RouteConfigSingleView } from 'vue-router/types/router';

export default <RouteConfigSingleView>{
  path: 'users/add',
  name: 'add-user',
  component: () => import('@views/users/add-user/add-user.vue'),
  meta: {
    title: 'Add User',
    icon: 'people'
  },
  children: []
};
