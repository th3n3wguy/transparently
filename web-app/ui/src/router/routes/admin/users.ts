// Import 3rd-party libraries.
import { RouteConfigSingleView } from 'vue-router/types/router';

export default <RouteConfigSingleView>{
  path: 'users',
  name: 'user-administration',
  component: () => import('@views/admin-users/admin-users.vue'),
  meta: {
    title: 'User Administration',
    icon: 'mdi-account-multiple'
  },
  children: []
};
