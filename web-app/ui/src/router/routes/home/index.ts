// Import 3rd-party libraries.
import { RouteConfigSingleView } from 'vue-router/types/router';

// Import local files.
import { default as Home } from '@views/home/home.vue';

export default <RouteConfigSingleView>{
  path: '/',
  name: 'home',
  component: Home,
  meta: {
    title: 'Home',
    icon: 'mdi-home'
  }
};
