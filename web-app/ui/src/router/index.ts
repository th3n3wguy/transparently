// Import 3rd party libraries.
import { default as VueRouter, Route } from 'vue-router';
import { isNil as _isNil } from 'lodash-es';

// Import local files.
import { localStorage } from '../services';
import routes from './routes';

// TODO: https://router.vuejs.org/guide/advanced/lazy-loading.html
const router = new VueRouter({
  mode: 'history',
  linkActiveClass: 'active',
  scrollBehavior(to, from, savedPosition) {
    return !_isNil(savedPosition) ? savedPosition : { x: 0, y: 0 };
  },
  routes
});

router.beforeEach((to, from, next) => {
  // store.dispatch('Auth/authCheck')
  //   .then(() => next())
  //   .catch(() => {
  //     next(new Error('Failure to pass the authentication check on route change.'));
  //   });
  return next();
});

router.afterEach((to: Route) => {
  // Set the page's title attribute based on the "meta.title" for the route that is defined in each route.
  window.document.title = `Transparently | ${to.meta?.title}`;
  localStorage.set('last_page_visited', to.fullPath);
});

export default router;
