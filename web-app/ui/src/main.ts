// Import 3rd party libraries.
import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import VueRouter from 'vue-router';

// Import application plugins.
import vuetifyConfig from './config/vuetify';

// Import application files.
import App from './app.vue';
import router from './router';
import store from './store';
import './registerServiceWorker';

// Import library & application styles.
import 'roboto-fontface/css/roboto/roboto-fontface.css';
import '@mdi/font/css/materialdesignicons.css'; // Ensure you are using css-loader

Vue.config.productionTip = process.env.NODE_ENV !== 'production';

Vue.use(Vuetify);
Vue.use(VueRouter);

new Vue({
  router,
  store,
  vuetify: new Vuetify(vuetifyConfig),
  render: h => h(App)
}).$mount('#app');
