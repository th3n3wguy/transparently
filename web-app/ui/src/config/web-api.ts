// Import 3rd party libraries.
import { default as axios } from 'axios';

export default axios.create({
  baseURL: '/api',
  headers: {
    Accept: 'application/json'
  }
});
