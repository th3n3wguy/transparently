import { UserVuetifyPreset } from 'vuetify';

export default <UserVuetifyPreset>{
  customProperties: true,
  icons: {
    iconfont: 'mdi'
  },
  // Things to keep in mind: https://www.verywellmind.com/color-psychology-2795824
  theme: {
    dark: false,
    options: {
      customProperties: true,
      variations: false
    },
    themes: {
      light: {
        primary: '#0D47A1',
        secondary: '#7E93B6',
        accent: '#212121',
        error: '#F44336',
        info: '#0D47A1',
        success: '#4CAF50',
        warning: '#FF5722'
      },
      dark: {
        primary: '#0D47A1',
        secondary: '#7E93B6',
        accent: '#FF9800',
        error: '#F44336',
        info: '#0D47A1',
        success: '#4CAF50',
        warning: '#FF5722'
      }
    }
  }
};
