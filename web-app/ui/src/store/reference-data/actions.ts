// Import 3rd party libraries.
import {
  ActionContext,
  ActionTree
} from 'vuex';

// Import local files.
import { ReferenceDataStoreState } from './state';
import { SET_WORK_ITEM_TYPES } from './mutation-types';
import { webAppServer } from '@services';
import { INIT } from './action-types';

export default <ActionTree<ReferenceDataStoreState, unknown>>{
  [INIT]: init
};

function init({ commit }: ActionContext<ReferenceDataStoreState, unknown>): Promise<void> {
  return new Promise(function(resolve, reject) {
    commit(SET_WORK_ITEM_TYPES, undefined);

    webAppServer.referenceData.getWorkItemTypes()
      .then(({ data: workItemTypes }) => {
        commit(SET_WORK_ITEM_TYPES, workItemTypes);

        return resolve();
      })
      .catch((err) => {
        commit(SET_WORK_ITEM_TYPES, []);

        return reject(err);
      });
  });
}
