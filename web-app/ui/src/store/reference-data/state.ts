// Import local files.
import { WorkItemType } from '@services/web-app-server/types/work-item-type';

export interface ReferenceDataStoreState {
  workItemTypes?: WorkItemType[]
}

export default (): ReferenceDataStoreState => ({
  workItemTypes: undefined
});
