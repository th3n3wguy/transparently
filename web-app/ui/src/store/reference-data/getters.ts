// Import 3rd party libraries.
import { GetterTree } from 'vuex';
import { isUndefined as _isUndefined } from 'lodash-es';

// Import local files.
import { ReferenceDataStoreState } from './state';

export default <GetterTree<ReferenceDataStoreState, unknown>>{
  isLoadingWorkItemTypes
};

function isLoadingWorkItemTypes(state: ReferenceDataStoreState): boolean {
  return _isUndefined(state.workItemTypes);
}
