// Import 3rd-party libraries.
import { MutationTree } from 'vuex';

// Import local files.
import { ReferenceDataStoreState } from './state';
import { SET_WORK_ITEM_TYPES } from './mutation-types';

export default <MutationTree<ReferenceDataStoreState>>{
  [SET_WORK_ITEM_TYPES]: setWorkItemTypes
};

/**
 * Sets the state of the Work Item Types, which are referenced throughout the application.
 *
 * @param {ReferenceDataStoreState} state
 * @param {ReferenceDataStoreState.workItemTypes} workItemTypes
 * @returns {void}
 */
function setWorkItemTypes(state: ReferenceDataStoreState, workItemTypes: ReferenceDataStoreState['workItemTypes']): void {
  state.workItemTypes = workItemTypes;
}
