// Import 3rd-party libraries.
import { Module } from 'vuex';

// Import the store components.
import actions from './actions';
import getters from './getters';
import mutations from './mutations';
import { default as state, ReferenceDataStoreState } from './state';

export const REFERENCE_DATA_STORE_MODULE_NAME: string = 'referenceData';

export default <Module<ReferenceDataStoreState, unknown>>{
  // More information here => https://github.com/igeligel/vuex-namespaced-module-structure/blob/master/src/store/modules/chat/index.js
  namespaced: true,
  actions,
  getters,
  mutations,
  state
};
