// Import 3rd-party libraries.
import { noop as _noop } from 'lodash-es';
import { RouteConfigSingleView } from 'vue-router/types/router';

// Import local files.
import adminRoute from '@routes/admin/users';
import homeRoute from '@routes/home';
import workItemsRoute from '@routes/work-items/work-items';

export interface GlobalStoreState {
  dateDisplayFormat: string;
  inputColor: 'primary'|'secondary';
  isDarkTheme: boolean;
  isFullScreenLoadingIndicatorShown: boolean;
  mainNav: {
    isOpen: boolean;
    routes: {
      displayName: string;
      routes: RouteConfigSingleView[]
    }[]
  };
  notification: {
    actionCallback: () => void;
    actionText: string;
    isShown: boolean;
    message: string;
    timeout: number;
  }
}

export default (): GlobalStoreState => ({
  dateDisplayFormat: 'yyyy-MM-dd',
  inputColor: 'primary',
  isDarkTheme: false,
  isFullScreenLoadingIndicatorShown: false,
  mainNav: {
    isOpen: false,
    routes: [
      {
        displayName: 'MAIN',
        routes: [
          homeRoute,
          workItemsRoute
        ]
      },
      {
        displayName: 'ADMINISTRATION',
        routes: [adminRoute]
      }
    ]
  },
  notification: {
    actionCallback: _noop,
    actionText: 'OK',
    isShown: false,
    message: '',
    timeout: 4000
  }
});
