// Import 3rd-party libraries.
import {
  ActionContext,
  ActionTree
} from 'vuex';

// Import local files.
import {
  HIDE_NOTIFICATION,
  INIT,
  SET_DARK_THEME,
  SET_FULL_SCREEN_LOADING_INDICATOR,
  SET_SIDE_NAV,
  SHOW_NOTIFICATION
} from './action-types';
import {
  SET_DARK_THEME_STATE,
  SET_FULL_SCREEN_LOADING_INDICATOR_STATE,
  SET_NOTIFICATION_STATE,
  SET_SIDENAV_STATE
} from './mutation-types';
import { localStorage } from '../../services';
import { GlobalStoreState } from './state';

export default <ActionTree<GlobalStoreState, unknown>>{
  hideFullScreenLoadingIndicator,
  [HIDE_NOTIFICATION]: hideNotification,
  [INIT]: init,
  [SET_DARK_THEME]: setDarkTheme,
  [SET_SIDE_NAV]: setSideNav,
  [SET_FULL_SCREEN_LOADING_INDICATOR]: showFullScreenLoadingIndicator,
  [SHOW_NOTIFICATION]: showNotification
};

function hideFullScreenLoadingIndicator({ commit }: ActionContext<GlobalStoreState, unknown>): Promise<void> {
  return new Promise((resolve) => {
    // Commit the mutation to close the full-screen loading indicator.
    commit(SET_FULL_SCREEN_LOADING_INDICATOR_STATE, false);

    return resolve();
  });
}

function hideNotification({ commit }: ActionContext<GlobalStoreState, unknown>): Promise<void> {
  return new Promise((resolve) => {
    // Commit the mutation to close the dialog.
    commit(SET_NOTIFICATION_STATE, { isShown: false });

    return resolve();
  });
}

function init({ dispatch }: ActionContext<GlobalStoreState, unknown>): Promise<void> {
  return new Promise((resolve, reject) => {
    localStorage.get(localStorage.keys.isDarkMode, false)
      .then((isDark) => dispatch('setDarkTheme', isDark))
      // .then(() => {
      //   Configure Axios to handle various exceptions here.
      //   prosperitatemApi.interceptors.response.use(
      //     (response) => Promise.resolve(response),
      //     (err) => {
      //       // We only want to log the User out of the system if the HTTP Status Code is 401 AND the code that came back as part of the response body indicates their session isn't found.
      //       if (
      //         _isEqual(err.response.status, 401)
      //         && _isEqual(_get(err, 'response.data.code'), 'E_SESSION_NOT_FOUND')
      //       ) {
      //         // We need to "log out" the user.
      //         dispatch('Auth/logout', {}, { root: true });
      //       }
      //
      //       return Promise.reject(err);
      //     }
      //   );
      //
      //   return Promise.all([]);
      // })
      .then(() => resolve())
      .catch((err) => reject(err));
  });
}

function setDarkTheme({ commit }: ActionContext<GlobalStoreState, unknown>, isDarkTheme: boolean = false): Promise<void> {
  return new Promise((resolve, reject) => {
    // Commit the change to the state.
    commit(SET_DARK_THEME_STATE, isDarkTheme);

    // Set the value on LocalStorage.
    localStorage.set(localStorage.keys.isDarkMode, isDarkTheme)
      .then(() => resolve())
      .catch((err) => reject(err));
  });
}

function setSideNav({ commit }: ActionContext<GlobalStoreState, unknown>, isOpen: boolean = false): Promise<void> {
  return new Promise((resolve) => {
    // Commit the mutation to determine whether the side nav is open or not.
    commit(SET_SIDENAV_STATE, isOpen);

    return resolve();
  });
}

function showFullScreenLoadingIndicator({ commit }: ActionContext<GlobalStoreState, unknown>): Promise<void> {
  return new Promise((resolve) => {
    // Commit the mutation to show the full-screen loading indicator.
    commit(SET_FULL_SCREEN_LOADING_INDICATOR_STATE, true);

    return resolve();
  });
}

function showNotification({ commit, dispatch }: ActionContext<GlobalStoreState, unknown>, { actionCallback, actionText, message, timeout }: GlobalStoreState['notification']): Promise<void> {
  return new Promise((resolve, reject) => {
    // We want to make sure that any notifications that are shown right now are hidden first before the new one is shown, as we don't want overlaps.
    dispatch('hideNotification')
      .then(() => {
        // Commit the mutation to show the notification with the provided parameters.
        commit(SET_NOTIFICATION_STATE, { isShown: true, actionCallback, actionText, message, timeout });

        return resolve();
      })
      .catch((err) => reject(err));
  });
}
