// Import 3rd-party libraries.
import { GetterTree } from 'vuex';

// Import local files.
import { GlobalStoreState } from './state';

export default <GetterTree<GlobalStoreState, unknown>>{
  inputFieldColor
};

function inputFieldColor(state: GlobalStoreState): string {
  return state.isDarkTheme ? 'secondary' : 'primary';
}
