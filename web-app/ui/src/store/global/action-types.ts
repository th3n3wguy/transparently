export const HIDE_NOTIFICATION: string = 'hideNotification';
export const INIT: string = 'init';
export const SET_DARK_THEME: string = 'setDarkTheme';
export const SET_SIDE_NAV: string = 'setSideNav';
export const SET_FULL_SCREEN_LOADING_INDICATOR: string = 'showFullScreenLoadingIndicator';
export const SHOW_NOTIFICATION: string = 'showNotification';
