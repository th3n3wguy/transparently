// Import 3rd party libraries.
import {
  assign as _assign,
  isNil as _isNil
} from 'lodash-es';
import { MutationTree } from 'vuex';

// Import local files.
import {
  SET_DARK_THEME_STATE,
  SET_FULL_SCREEN_LOADING_INDICATOR_STATE,
  SET_NOTIFICATION_STATE,
  SET_SIDENAV_STATE
} from './mutation-types';
import { default as initialState, GlobalStoreState } from './state';

export default <MutationTree<GlobalStoreState>>{
  [SET_DARK_THEME_STATE]: setDarkThemeState,
  [SET_FULL_SCREEN_LOADING_INDICATOR_STATE]: setFullScreenLoadingIndicatorState,
  [SET_NOTIFICATION_STATE]: setNotificationState,
  [SET_SIDENAV_STATE]: setSidenavState
};

function setDarkThemeState(state: GlobalStoreState, isDarkTheme: boolean = false): void {
  state.isDarkTheme = isDarkTheme;
  state.inputColor = isDarkTheme ? 'secondary' : 'primary';
}

function setFullScreenLoadingIndicatorState(state: GlobalStoreState, isOpen: boolean = false): void {
  state.isFullScreenLoadingIndicatorShown = isOpen;
}

function setNotificationState(state: GlobalStoreState, { actionCallback, actionText, isShown, message, timeout }: GlobalStoreState['notification']): void {
  const initState = initialState();

  state.notification = {
    actionCallback: _isNil(actionCallback) ? initState.notification.actionCallback : actionCallback,
    actionText: _isNil(actionText) ? initState.notification.actionText : actionText,
    isShown: _isNil(isShown) ? initState.notification.isShown : isShown,
    message: _isNil(message) ? initState.notification.message : message,
    timeout: _isNil(timeout) ? initState.notification.timeout : timeout
  };
}

function setSidenavState(state: GlobalStoreState, isOpen: boolean = false): void {
  state.mainNav = _assign({}, state.mainNav, { isOpen });
}
