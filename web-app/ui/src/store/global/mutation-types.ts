export const SET_DARK_THEME_STATE: string = 'SET_DARK_THEME_STATE';
export const SET_FULL_SCREEN_LOADING_INDICATOR_STATE: string = 'SET_FULL_SCREEN_LOADING_INDICATOR_STATE';
export const SET_NOTIFICATION_STATE: string = 'SET_NOTIFICATION_STATE';
export const SET_SIDENAV_STATE: string = 'SET_SIDENAV_STATE';
