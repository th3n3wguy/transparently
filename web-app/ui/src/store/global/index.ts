// Import 3rd-party libraries.
import { Module } from 'vuex';

// Import local files.
import actions from './actions';
import getters from './getters';
import mutations from './mutations';
import { default as state, GlobalStoreState } from './state';

// Set up the name of the module.
export const GLOBAL_STORE_MODULE_NAME: string = 'global';

export default <Module<GlobalStoreState, unknown>>{
  // More information here => https://github.com/igeligel/vuex-namespaced-module-structure/blob/master/src/store/modules/chat/index.js
  namespaced: true,
  actions,
  getters,
  mutations,
  state
};
