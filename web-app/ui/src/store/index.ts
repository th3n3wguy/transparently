// Import 3rd party libraries.
import Vue from 'vue';
import { default as Vuex } from 'vuex';

// Import application stores.
import { default as global, GLOBAL_STORE_MODULE_NAME as GLOBAL_STORE_MODULE_NAME } from './global';
import { default as referenceData, REFERENCE_DATA_STORE_MODULE_NAME } from './reference-data';

// Tell Vue that we want to use the Vuex store engine for this application.
Vue.use(Vuex);

export default new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',
  modules: {
    [GLOBAL_STORE_MODULE_NAME]: global,
    [REFERENCE_DATA_STORE_MODULE_NAME]: referenceData
  }
});
