// // Import 3rd party libraries.
// import Vuex from 'vuex';
// import {
//   shallowMount,
//   createLocalVue
// } from '@vue/test-utils';
//
// // Import application components.
// import SideNavComponent from './side-nav';
// import sinon from 'sinon';
//
// xdescribe(SideNavComponent.name, () => {
//   describe('.name property', () => {
//     test.skip('should be of type String', () => {
//       expect(SideNavComponent.name).to.be.a('string');
//     });
//   });
//
//   describe('when the component is mounted', () => {
//     let localVue;
//     let component;
//
//     before(() => {
//       // Create a local Vue instance for the test environment.
//       localVue = createLocalVue();
//       localVue.use(Vuex);
//     });
//
//     beforeEach(() => {
//       component = shallowMount(SideNavComponent, {
//         localVue,
//         store: new Vuex.Store({
//           modules: {
//             sideNav: {
//               namespaced: true,
//               state: {},
//               actions: {
//                 show: sinon.fake(),
//                 hide: sinon.fake()
//               },
//               mutations: {},
//               getters: {
//                 isShown: sinon.fake.returns(true),
//                 navRoutes: sinon.fake.returns([]),
//                 userRoute: sinon.fake.returns({})
//               }
//             },
//             currentUser: {
//               namespaced: true,
//               state: {},
//               actions: {},
//               mutations: {},
//               getters: {
//                 profile: sinon.fake.returns({})
//               }
//             }
//           }
//         }),
//         stubs: [
//           'v-divider',
//           'v-list',
//           'v-list-tile',
//           'v-list-tile-avatar',
//           'v-list-tile-content',
//           'v-list-tile-title',
//           'v-navigation-drawer',
//           'v-toolbar'
//         ]
//       });
//     });
//
//     describe('.toggle', () => {
//       test.skip('should be of type Function', () => {
//         expect(component.vm.toggle).to.be.a('function');
//       });
//
//       describe('when invoked', () => {
//         beforeEach(() => {
//           component.vm.hide = sinon.fake();
//         });
//
//         afterEach(() => sinon.restore());
//
//         test.skip('should not hide the sidebar if, and only if, the parameter provided is a Boolean value of "true"', () => {
//           component.vm.toggle(true);
//
//           expect(component.vm.hide.calledOnce).to.be.false;
//         });
//
//         test.skip('should hide the sidebar if the parameter provided is a value of "null"', () => {
//           component.vm.toggle(null);
//
//           expect(component.vm.hide.calledOnce).to.be.true;
//         });
//
//         test.skip('should hide the sidebar if the parameter provided is a value of "undefined", or no parameter is provided', () => {
//           component.vm.toggle();
//
//           expect(component.vm.hide.calledOnce).to.be.true;
//         });
//
//         test.skip('should hide the sidebar if the parameter provided is a String value of ""', () => {
//           component.vm.toggle('');
//
//           expect(component.vm.hide.calledOnce).to.be.true;
//         });
//
//         test.skip('should hide the sidebar if the parameter provided is a Boolean value of "false"', () => {
//           component.vm.toggle(false);
//
//           expect(component.vm.hide.calledOnce).to.be.true;
//         });
//       });
//     });
//   });
// });
