// // Import 3rd party libraries.
// import Vuex from 'vuex';
// import {
//   mount,
//   createLocalVue
// } from '@vue/test-utils';
//
// // Import application components.
// import Notifications from './notifications';
//
// // Create a local Vue instance for the test environment.
// const localVue = createLocalVue();
//
// // Tell the local Vue instance to use Vuex.
// localVue.use(Vuex);
//
// xdescribe(Notifications.name, () => {
//   describe('.name', () => {
//     test.skip('should be defined and of type String', () => {
//       expect(Notifications.name).to.be.a('string');
//     });
//   });
//
//   describe('when the component is mounted', () => {
//     const isShownDefaultValue = false;
//     const actionCallbackSpy = sinon.fake.returns(null);
//     let component;
//
//     beforeEach(() => {
//       component = mount(Notifications, {
//         localVue,
//         store: new Vuex.Store({
//           modules: {
//             Notifications: {
//               namespaced: true,
//               state: {},
//               actions: {},
//               mutations: {},
//               getters: {
//                 text: sinon.fake.returns(null),
//                 timeout: sinon.fake.returns(4000),
//                 showAction: sinon.fake.returns(null),
//                 actionText: sinon.fake.returns(null),
//                 actionCallback: actionCallbackSpy,
//                 isShown: sinon.fake.returns(isShownDefaultValue)
//               }
//             }
//           }
//         }),
//         stubs: [
//           'v-snackbar',
//           'v-btn'
//         ]
//       });
//     });
//
//     afterEach(() => {
//       sinon.restore();
//     });
//
//     describe('.isShown', () => {
//       test.skip('should be of type Boolean', () => {
//         expect(component.vm.isShown).to.be.a('boolean');
//       });
//
//       test.skip('should return the "state.getters.isShown" value', () => {
//         expect(component.vm.isShown).to.equal(isShownDefaultValue);
//       });
//
//       test.skip('should close the dialog when the value is changed', () => {
//         component.vm.hide = sinon.fake.returns(Promise.resolve());
//
//         component.vm.isShown = false;
//
//         expect(component.vm.hide.calledOnce).to.be.true;
//       });
//     });
//
//     describe('.activateActionButton', () => {
//       test.skip('should be of type Function', () => {
//         expect(component.vm.activateActionButton).to.be.a('function');
//       });
//
//       describe('when invoked', () => {
//         test.skip('should invoke the ".hide" and ".actionCallback" functions', () => {
//           const fakeEvent = { a: 'b' };
//           component.vm.hide = sinon.fake.returns(Promise.resolve());
//
//           component.vm.activateActionButton(fakeEvent);
//
//           expect(component.vm.hide.calledOnce).to.be.true;
//           expect(actionCallbackSpy.calledOnce).to.be.true;
//         });
//       });
//     });
//   });
// });
