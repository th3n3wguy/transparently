// // Import 3rd party libraries.
// import {
//   mount,
//   createLocalVue
// } from '@vue/test-utils';
// import Vuex from 'vuex';
//
// // Import application components.
// import LoadingIndicator from './loading-indicator';
//
// // Create a local Vue instance for the test environment.
// const localVue = createLocalVue();
//
// // Tell the local Vue instance to use Vuex.
// localVue.use(Vuex);
//
// describe(LoadingIndicator.name, () => {
//   describe('.name', () => {
//     test.skip('should be of type String', () => {
//       expect(LoadingIndicator.name).to.be.a('string');
//     });
//   });
//
//   describe('when the component is mounted', () => {
//     let component;
//     let store;
//
//     beforeEach(() => {
//       // TODO: Need to create a mocked version of the store so I can import it into other tests that might use it in the future.
//       store = new Vuex.Store({
//         modules: {
//           LoadingIndicator: {
//             namespaced: true,
//             state: {},
//             actions: {},
//             mutations: {},
//             getters: {
//               isShown: sinon.fake.returns(false)
//             }
//           }
//         }
//       });
//       component = mount(LoadingIndicator, {
//         localVue,
//         store,
//         stubs: [
//           'v-dialog',
//           'v-container',
//           'v-progress-circular'
//         ]
//       });
//     });
//
//     afterEach(() => {
//       sinon.restore();
//     });
//   });
// });
