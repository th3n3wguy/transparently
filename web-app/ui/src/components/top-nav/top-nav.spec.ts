// // Import 3rd party libraries.
// import { createTestInstance } from '@tests/setup';
// import { merge as _merge } from 'lodash-es';
//
// // Import application components.
// import TopNavComponent from './top-nav';
// import globalStore from '@store/global';
//
// describe(TopNavComponent.name, () => {
//   describe('.name property', () => {
//     test.skip('should be of type String', () => {
//       expect(TopNavComponent.name).to.be.a('string');
//     });
//   });
//
//   describe('when the component is mounted', () => {
//     let component;
//
//     beforeEach(() => {
//       // Create a local Vue instance for the test environment.
//       component = createTestInstance(TopNavComponent, true, {
//         global: globalStore,
//         CreateWorkItemDialog: _merge({}, CreateWorkItemDialogStore, {
//           actions: {
//             open: sinon.fake.returns(Promise.resolve())
//           }
//         })
//       });
//     });
//
//     describe('.setSideNav', () => {
//       test.skip('should be a Function', () => {
//         expect(component.vm.setSideNav).to.be.a('function');
//       });
//     });
//
//     describe('.openCreateWorkItemDialog', () => {
//       test.skip('should be a Function', () => {
//         expect(component.vm.openCreateWorkItemDialog).to.be.a('function');
//       });
//     });
//
//     describe('.logout', () => {
//       test.skip('should be a Function', () => {
//         expect(component.vm.logout).to.be.a('function');
//       });
//     });
//   });
// });
