// Import 3rd-party libraries.
import {
  DataOptions,
  DataTableHeader
} from 'vuetify';
import { RouteConfigSingleView } from 'vue-router/types/router';

// Import local files.
import { WorkItemType } from '@services/web-app-server/types/work-item-type';
import { WorkItem } from '@services/web-app-server/types/work-item';

export interface Data {
  createWorkItemRoute: RouteConfigSingleView;
  tableHeaders: DataTableHeader[];
  tableOptions: DataOptions;
  workItems?: (WorkItem & { createdOnDisplayValue: string; lastUpdatedOnDisplayValue: string; type?: WorkItemType; })[];
}

export interface Computed {
  isLoadingWorkItems: boolean;
  isLoadingWorkItemTypes: boolean;
  workItemTypes?: WorkItemType[];
}

export interface Methods {
  loadWorkItems(): Promise<void>;
}

export interface Props {}
