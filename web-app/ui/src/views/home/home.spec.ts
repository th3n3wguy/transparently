// // Import application components.
// import home from './home';
//
// describe(home.name, () => {
//   describe('.name property', () => {
//     test.skip('should be defined and of type String', () => {
//       expect(home.name).to.not.be.undefined;
//       expect(home.name).to.not.be.null;
//       expect(home.name).to.be.a('string');
//     });
//   });
//
//   describe('.data() property', () => {
//     describe('.msg property', () => {
//       let data;
//
//       beforeEach(() => {
//         data = home.data();
//       });
//
//       test.skip('should be defined and of type String', () => {
//         expect(data.msg).to.not.be.undefined;
//         expect(data.msg).to.not.be.null;
//         expect(data.msg).to.be.a('string');
//       });
//
//       test.skip('should not be an empty String', () => {
//         expect(data.msg).to.not.be.empty;
//       });
//     });
//   });
// });
