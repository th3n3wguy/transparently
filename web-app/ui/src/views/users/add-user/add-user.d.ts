// Import 3rd-party libraries.
import { InputValidationRules } from 'vuetify';

// Import local files.
import { GlobalStoreState } from '@store/global/state';

export interface Computed {
  inputColor: GlobalStoreState['inputColor'];
}

export interface Data {
  bottomNavValue: string;
  emailAddress: string;
  emailAddressHint: string;
  emailAddressRules: InputValidationRules;
  fullName: string;
  fullNameHint: string;
  fullNameRules: InputValidationRules;
  isFormValid: boolean;
  possibleServerErrors: Record<string, string>;
  serverErrors: string[];
  username: string;
  usernameRules: InputValidationRules;
}

export interface Methods {
  cancel(): void;
  clearServerErrors(): void;
  goBack(): void;
  resetForm(): void;
  submit(): void;
  validateForm(): void;
}

export interface Props {}
