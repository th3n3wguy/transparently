// // Import 3rd party libraries.
// import {
//   createLocalVue,
//   shallowMount
// } from '@vue/test-utils';
// import Vuex from 'vuex';
//
// // Import the component to be tested.
// import UserList from './admin-users';
// import mockNamespacedStore from '@mocks/namespaced-store';
//
// xdescribe(UserList.name, () => {
//   describe('.name property', () => {
//     test.skip('should be of type String', () => {
//       expect(UserList.name).to.be.a('string');
//     });
//   });
//
//   describe('when the component is mounted', () => {
//     let component;
//     let localVue;
//
//     before(() => {
//       localVue = createLocalVue();
//       localVue.use(Vuex);
//     });
//
//     beforeEach(() => {
//       component = shallowMount(UserList, {
//         localVue,
//         store: new Vuex.Store({
//           modules: {
//             adminCreateUserDialog: _.merge({}, mockNamespacedStore, {
//               mutations: {
//                 open: sinon.fake()
//               }
//             }),
//             Notifications: _.merge({}, mockNamespacedStore, {
//               actions: {
//                 open: sinon.fake.returns(Promise.resolve())
//               }
//             })
//           }
//         }),
//         stubs: [
//           'v-alert',
//           'v-avatar',
//           'v-btn',
//           'v-container',
//           'v-data-table',
//           'v-divider',
//           'v-flex',
//           'v-icon',
//           'v-layout',
//           'v-progress-linear',
//           'v-subheader',
//           'v-tooltip'
//         ]
//       });
//     });
//
//     describe('.isLoading', () => {
//       test.skip('should be of type Boolean', () => {
//         expect(component.vm.isLoading).to.be.a('boolean');
//       });
//
//       test.skip('should default to a value of "false"', () => {
//         expect(component.vm.isLoading).to.be.false;
//       });
//     });
//
//     describe('.headers', () => {
//       test.skip('should be of type Array', () => {
//         expect(component.vm.headers).to.be.an('array');
//       });
//
//       test.skip('each object in the Array should contain the expected properties', () => {
//         _.map(component.vm.headers, (header) => {
//           expect(header.text).to.be.a('string');
//           expect(header.sortable).to.be.a('boolean');
//           expect(header.value).to.be.a('string');
//         });
//       });
//     });
//
//     describe('.users', () => {
//       test.skip('should be of type Array', () => {
//         expect(component.vm.users).to.be.an('array');
//       });
//
//       test.skip('should be initialized to an empty array', () => {
//         expect(component.vm.users).to.be.empty;
//       });
//     });
//
//     describe('.getAllUsers', () => {});
//
//     describe('.deleteUser', () => {});
//   });
// });
