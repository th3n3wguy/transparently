// Import 3rd-party libraries.
import {
  DataItemsPerPageOption,
  DataTableHeader
} from 'vuetify';

// Import local files.
import { User } from '@services/web-app-server/types/user';

export interface Data {
  createUserRoute: {
    name: string;
  };
  // This is the VDataFooter.props data type, but that is not exported from Vuetify as an interface / type.
  footer: {
    itemsPerPageOptions: DataItemsPerPageOption[];
    itemsPerPageText: string;
    showFirstLastPage: boolean;
  };
  headers: DataTableHeader[];
  isLoading: boolean;
  users: User[];
}

export interface Methods {
  deleteUser(userId: User['userId']): void;
  getAllUsers(): void;
  viewUser(userId: User['userId']): void;
}

export interface Computed {}

export interface Props {}
