// Import 3rd-party libraries.
import { InputValidationRules } from 'vuetify';

// Import local files.
import { WorkItemType } from '@services/web-app-server/types/work-item-type';
import { GlobalStoreState } from '@store/global/state';

export interface Computed {
  inputColor: GlobalStoreState['inputColor'];
  isInitialFormState: boolean;
}

export interface Data {
  description: InitialFormData['description'];
  descriptionRules: InputValidationRules;
  isFormValid: boolean;
  name: InitialFormData['name'];
  nameRules: InputValidationRules;
  serverErrors: string[];
  workItemType: InitialFormData['workItemType'];
  workItemTypeRules: InputValidationRules;
}

export interface Methods {
  cancel(): void;
  clearServerErrors(): void;
  submit(): void;
}

export interface Props {}

export interface InitialFormData {
  description: string;
  name: string;
  workItemType: WorkItemType | null;
}
