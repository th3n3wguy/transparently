// Import local files.
import { InitialFormData } from '@views/add-work-item/add-work-item';

export function generateInitialFormData(): InitialFormData {
  return {
    description: '',
    name: '',
    workItemType: null
  };
}
