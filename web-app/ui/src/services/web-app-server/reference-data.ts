// Import 3rd-party libraries.
import { AxiosResponse } from 'axios';

// Import local files.
import webApi from '../../config/web-api';
import { WorkItemType } from './types/work-item-type';

export default {
  getWorkItemTypes
};

function getWorkItemTypes(): Promise<AxiosResponse<WorkItemType[]>> {
  return new Promise((resolve, reject) => {
    webApi.get<WorkItemType[]>('/v0/reference-data/work-item-types', { params: { limit: 50, offset: 0, sortKey: 'createdOn', sortDirection: 'desc' }})
      .then(resolve)
      .catch(reject);
  });
}
