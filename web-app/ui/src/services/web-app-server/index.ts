// Import local files.
import referenceData from './reference-data';
import users from './users';
import workItems from './work-items';

/**
 * @typedef {Object} Services.WebAppServer
 * @property {typeof referenceData} referenceData
 * @property {typeof users} users
 * @property {typeof workItems} workItems
 */
export default {
  referenceData,
  users,
  workItems
};
