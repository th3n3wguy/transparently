export type User = ServerManagedProperties & EditableProperties;

export interface ServerManagedProperties {
  userId: string;
  createdOn: string;
  lastUpdatedOn: string;
}

export interface EditableProperties {
  username: string;
  fullName: string;
  emailAddress: string;
}
