export type WorkItem = WorkItemServerManagedData & WorkItemUserManagedData;

export interface WorkItemServerManagedData {
  id: string;
  // This is a string-representation of the ISO6801 format: YYYY-MM-DDTHH:mm:ss.sssZ
  createdOn: string;
  // This is a string-representation of the ISO6801 format: YYYY-MM-DDTHH:mm:ss.sssZ
  lastUpdatedOn: string;
}

export interface WorkItemUserManagedData {
  name: NonNullable<string>;
  description: NonNullable<string>;
  typeSlug: NonNullable<string>;
}
