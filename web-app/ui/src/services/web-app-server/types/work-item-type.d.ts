export interface WorkItemType {
  slug: string;
  // This is a string-representation of the ISO6801 format: YYYY-MM-DDTHH:mm:ss.sssZ
  createdOn?: string;
  // This is a string-representation of the ISO6801 format: YYYY-MM-DDTHH:mm:ss.sssZ
  lastUpdatedOn?: string;
  name: NotNullable<string>;
  description: NotNullable<string>;
  parentSlug: string | null;
}
