export interface GetRequestQueryParams {
  limit?: number;
  offset?: number;
  sortKey?: string;
  sortDirection?: string;
}
