// Import 3rd-party libraries.
import { AxiosResponse } from 'axios';

// Import local files.
import {
  User,
  EditableProperties,
  ServerManagedProperties
} from './types/user';
import webApi from '../../config/web-api';

export default {
  createUser,
  deleteUserById,
  getAll
};

function createUser(user: EditableProperties): Promise<AxiosResponse<User>> {
  return new Promise((resolve, reject) => {
    webApi.post<User>('/v0/users', user)
      .then(resolve)
      .catch(reject);
  });
}

function deleteUserById(userId: User['userId']): Promise<AxiosResponse<void>> {
  return new Promise((resolve, reject) => {
    webApi.delete<void>(`/v0/users/${userId}`)
      .then(resolve)
      .catch(reject);
  });
}

function getAll(): Promise<AxiosResponse<User[]>> {
  return new Promise((resolve, reject) => {
    webApi.get<User[]>('/v0/users')
      .then(resolve)
      .catch(reject);
  });
}
