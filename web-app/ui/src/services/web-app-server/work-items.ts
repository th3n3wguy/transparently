// Import 3rd-party libraries.
import { AxiosResponse } from 'axios';

// Import local files.
import webApi from '../../config/web-api';
import { WorkItem } from '@services/web-app-server/types/work-item';
import { GetRequestQueryParams } from '@services/web-app-server/types/get-request-query-params';

export default {
  getWorkItems
};

function getWorkItems(params: GetRequestQueryParams): Promise<AxiosResponse<WorkItem[]>> {
  return new Promise((resolve, reject) => {
    webApi.get<WorkItem[]>('/v0/work-items', { params })
      .then(resolve)
      .catch(reject);
  });
}
