// Import 3rd-party libraries.
import {
  invokeMap as _invokeMap,
  isEmpty as _isEmpty
} from 'lodash-es';

export interface LogService {
  exception: typeof exception;
  error: typeof error;
  warn: typeof warn;
  info: typeof info;
  debug: typeof debug;
  trace: typeof trace;
}

export default <LogService>{
  exception,
  error,
  warn,
  info,
  debug,
  trace
};

function exception(e: Error): Promise<void> {
  // Returning a Promise here because we will likely log this information to the API server or other places in the future.
  return new Promise((resolve) => {
    // eslint-disable-next-line no-console
    console.error(e.stack);

    return resolve();
  });
}

function error<T>(msg: string, additionalDetails: T[] = []): Promise<void> {
  return new Promise((resolve) => {
    // eslint-disable-next-line no-console
    console.error(msg);
    logAdditionalDetailsToConsole(msg, additionalDetails);

    return resolve();
  });
}

function warn<T>(msg: string, additionalDetails: T[] = []): Promise<void> {
  return new Promise((resolve) => {
    // eslint-disable-next-line no-console
    console.warn(msg);
    logAdditionalDetailsToConsole(msg, additionalDetails);

    return resolve();
  });
}

function info<T>(msg: string, additionalDetails: T[] = []): Promise<void> {
  return new Promise((resolve) => {
    // eslint-disable-next-line no-console
    console.info(msg);
    logAdditionalDetailsToConsole(msg, additionalDetails);

    return resolve();
  });
}

function debug<T>(msg: string, additionalDetails: T[] = []): Promise<void> {
  return new Promise((resolve) => {
    // eslint-disable-next-line no-console
    console.debug(msg);
    logAdditionalDetailsToConsole(msg, additionalDetails);

    return resolve();
  });
}

function trace<T>(msg: string, additionalDetails: T[] = []): Promise<void> {
  return new Promise((resolve) => {
    // eslint-disable-next-line no-console
    console.trace(msg);
    logAdditionalDetailsToConsole(msg, additionalDetails);

    return resolve();
  });
}

function logAdditionalDetailsToConsole<T>(msg: string, additionalDetails: T[]) {
  if (!_isEmpty(additionalDetails)) {
    // eslint-disable-next-line no-console
    console.groupCollapsed(`${msg} (additional details / information)`);
    _invokeMap(additionalDetails, function(this: string|boolean|number|Record<string, unknown>|null|undefined) {
      // eslint-disable-next-line no-console
      console.debug(this);
    });
    // eslint-disable-next-line no-console
    console.groupEnd();
  }
}
