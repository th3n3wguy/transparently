// Import 3rd party libraries.
import {
  add as _add,
  concat as _concat,
  curryRight as _curryRight,
  flow as _flow,
  includes as _includes,
  map as _map,
  max as _max,
  toFinite as _toFinite,
  values as _values
} from 'lodash-es';

interface LocalStorageKeys {
  isDarkMode: string;
  lastPageVisited: string;
}

export const namespace: string = 'transparently_';
export const keys: LocalStorageKeys = {
  isDarkMode: 'is_dark_mode',
  lastPageVisited: 'last_page_visited'
};

export function generateNextId(key: string): number {
  return _flow(
    (current) => _concat(current, { [key]: 0 }),
    // @ts-ignore: Unsure of how to map this curried function as the types aren't matching, even though the code works perfectly.
    _curryRight(_map)((val: Record<string, unknown>) => _toFinite(val[key])),
    _curryRight(_max),
    _curryRight(_add)(1)
  )(get(key, []));
}

export function get(key: string = '', defaultValue: boolean|string|number|Record<string, unknown>|[] = ''): Promise<string|boolean|number|Record<string, unknown>|[]> {
  return new Promise((resolve) => {
    try {
      return resolve(JSON.parse(<string>window.localStorage.getItem(`${namespace}${key}`)));
    }
    catch (ex) {
      return resolve(defaultValue);
    }
  });
}

export function remove(key: string = ''): Promise<void> {
  return new Promise((resolve, reject) => {
    try {
      return resolve(window.localStorage.removeItem(`${namespace}${key}`));
    }
    catch (ex) {
      return reject(ex);
    }
  });
}

export function set(key: string, value: boolean|string|number|Record<string, unknown>|[] = 'missing_value'): Promise<void> {
  return new Promise((resolve, reject) => {
    if (!_includes(_values(keys), key)) {
      return reject(new Error('The provided "key" is not one of the expected values.'));
    }

    try {
      return resolve(window.localStorage.setItem(`${namespace}${key}`, JSON.stringify(value)));
    }
    catch (ex) {
      return reject(ex);
    }
  });
}

export interface LocalStorageService {
  generateNextId: typeof generateNextId;
  get: typeof get;
  keys: LocalStorageKeys;
  remove: typeof remove;
  set: typeof set;
}

export default <LocalStorageService>{
  generateNextId,
  get,
  keys,
  remove,
  set
};
