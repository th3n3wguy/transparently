// Import 3rd-party libraries.
import {
  parse as dfParse,
  format as dfFormat
} from 'date-fns';

export const serverDateFnsFormat: NonNullable<string> = 'yyyy-MM-dd\'T\'HH:mm:ss.SSSSSSxxx';
export const humanReadableFormatWithoutMilliseconds: NonNullable<string> = 'd MMMM, yyyy h:m:s a xxx';
export const humanReadableFormatWithMilliseconds: NonNullable<string> = 'd MMMM, yyyy h:m:s.SSSSSS a xxx'

export default function serverTimestampStringToHumanReadable(date: NonNullable<string>, includeMilliseconds: boolean = false): NonNullable<string> {
  return dfFormat(
    dfParse(date, serverDateFnsFormat, new Date()),
    includeMilliseconds ? humanReadableFormatWithMilliseconds : humanReadableFormatWithoutMilliseconds
  );
}
