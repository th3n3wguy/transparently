// Configuration definition.
module.exports = {
  collectCoverage: true,
  collectCoverageFrom: [
    '<rootDir>/src/**/*.vue',
    '!<rootDir>/src/{assets,config,router}/**/*.*',
    '!<rootDir>/src/**/*.spec.js'
  ],
  coverageThreshold: {
    global: {
      branches: 1,
      functions: 1,
      lines: 1,
      statements: 1
    }
  },
  coverageReporters: [
    'json',
    'lcov',
    'text'
  ],
  maxWorkers: 2,
  moduleFileExtensions: [
    'js',
    'json',
    'vue'
  ],
  moduleNameMapper: {
    '^@/(.*)$': '<rootDir>/src/$1',
    '@config': '<rootDir>/src/config',
    '@components': '<rootDir>/src/components',
    '@mocks': '<rootDir>/tests/mocks',
    '@routes': '<rootDir>/src/router/routes',
    '@services': '<rootDir>/src/services',
    '@store': '<rootDir>/src/store',
    '@tests': '<rootDir>/tests',
    '@views': '<rootDir>/src/views'
  },
  moduleDirectories: [
    'node_modules',
    'src'
  ],
  preset: '@vue/cli-plugin-unit-jest/presets/typescript-and-babel',
  rootDir: './',
  roots: [
    '<rootDir>/src',
    '<rootDir>/tests'
  ],
  setupFiles: [
    'dotenv/config'
  ],
  setupFilesAfterEnv: [
    'jest-extended'
  ],
  testEnvironment: 'jsdom',
  testMatch: [
    'src/**/*.(spec|test).(js|ts)',
    'tests/**/*.(spec|test).(js|ts)'
  ],
  transform: {
    '^.+\\.js$': 'babel-jest',
    '^.+\\.tsx?$': 'ts-jest',
    '^.+\\.vue$': 'vue-jest'
  }
};
