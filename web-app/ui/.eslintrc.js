// https://eslint.org/docs/user-guide/configuring

module.exports = {
  root: true,
  parserOptions: {
    ecmaVersion: 2020
  },
  env: {
    browser: true,
    es6: true,
    node: true,
    jest: true
  },
  globals: {
    '_': true,
    expect: true,
    jest: true
  },
  extends: [
    'plugin:vue/recommended',
    '@vue/standard',
    '@vue/typescript/recommended'
  ],
  plugins: [
    'jest',
    'vue',
    'vuetify'
  ],
  // add your custom rules here
  rules: {
    // Vue-specific rules.
    'vue/component-definition-name-casing': ['error', 'kebab-case'],
    'vue/v-slot-style': ['error', {
      'atComponent': 'longform',
      'default': 'longform',
      'named': 'longform'
    }],
    'vue/valid-v-slot': ['error', {
      'allowModifiers': true
    }],
    // Import rules.
    'import/no-named-default': 'off',
    // TypeScript rules.
    '@typescript-eslint/ban-ts-comment': ['error', {
      'ts-ignore': 'allow-with-description'
    }],
    '@typescript-eslint/explicit-module-boundary-types': ['warn', {
      'allowedNames': ['data', 'mounted']
    }],
    '@typescript-eslint/no-inferrable-types': ['off'],
    '@typescript-eslint/no-namespace': 'off',
    // General JavaScript rules.
    'brace-style': ['error', 'stroustrup'],
    'generator-star-spacing': 'off',
    'indent': ['error', 2],
    'no-console': ['error', { 'allow': ['warn', 'error'] }],
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'warn',
    'semi': ['error', 'always'],
    'space-before-function-paren': ['error', 'never'],
    // Vuetify.js-specific rules.
    'vuetify/grid-unknown-attributes': 'warn',
    'vuetify/no-deprecated-classes': 'error',
    'vuetify/no-legacy-grid': 'error',
    // Vue.js-specific rules.
    'vue/array-bracket-spacing': 'error',
    'vue/arrow-spacing': 'error',
    'vue/attributes-order': 'error',
    'vue/brace-style': 'error',
    'vue/eqeqeq': 'error',
    'vue/html-self-closing': ['error', {
      'html': {
        'void': 'always',
        'normal': 'always',
        'component': 'always'
      },
      'svg': 'always',
      'math': 'always'
    }],
    'vue/key-spacing': 'error',
    'vue/match-component-file-name': 'error',
    'vue/max-attributes-per-line': ['error', {
      'singleline': 3,
      'multiline': {
        'max': 1,
        'allowFirstLine': false
      }
    }],
    'vue/name-property-casing': ['error', 'kebab-case'],
    'vue/no-restricted-syntax': [
      'error',
      'FunctionExpression',
      'WithStatement',
      'VElement > VExpressionContainer CallExpression'
    ],
    'vue/order-in-components': 'error',
    'vue/script-indent': ['error', 2, { 'baseIndent': 1 }],
    'vue/singleline-html-element-content-newline': ['error', {
      'ignoreWhenNoAttributes': true,
      'ignores': [
        'p',
        'span',
        'v-btn',
        'v-card-title',
        'v-icon',
        'v-toolbar-title'
      ]
    }]
  },
  overrides: [
    {
      files: [
        '*.vue'
      ],
      rules: {
        indent: 'off'
      }
    },
    {
      files: [
        '**/__tests__/*.{j,t}s?(x)',
        '**/tests/unit/**/*.spec.{j,t}s?(x)'
      ],
      env: {
        jest: true
      }
    }
  ]
};
