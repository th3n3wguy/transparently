require('dotenv').config();
const sonarqubeScanner = require('sonarqube-scanner');
const { isNil: _isNil } = require('lodash');

sonarqubeScanner({
  serverUrl: 'https://sonarcloud.io',
  options: {
    'sonar.javascript.lcov.reportPaths': './coverage/lcov.info',
    'sonar.login': process.env.SONAR_LOGIN,
    'sonar.language': 'js',
    'sonar.modules': 'webUi,webServer,userService,workItemService',
    'sonar.organization': 'th3n3wguy',
    'sonar.projectKey': 'th3n3wguy_transparently',
    'sonar.sources': 'src',
    'webUi.sonar.projectBaseDir': 'web-app/ui',
    'webUi.sonar.sources': 'src,tests',
    'webServer.sonar.projectBaseDir': 'web-app/server',
    'webServer.sonar.sources': 'api,store,tests',
    'userService.sonar.projectBaseDir': 'services/microservice-user',
    'userService.sonar.sources': 'errors,rest,sockets,store',
    'workItemService.sonar.projectBaseDir': 'services/microservice-work-item',
    'workItemService.sonar.sources': 'errors,rest,sockets,store'
  }
}, (err) => {
  return _isNil(err) ? console.log('Sonar scan successful.')
    : console.error('Sonar scan error:', err);
});
